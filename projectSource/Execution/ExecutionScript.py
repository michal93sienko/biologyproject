import os
from zipfile import ZipFile

from GUI.Utils import Config
from Logic.runningProgrammes import header


def execute_programs():
    config = Config.load_config()
    apps_names = Config.get_external_apps_names()
    app_executer = Config.get_external_apps_execution_functions()
    app_output_files = []
    for app_name in apps_names:
        if app_name not in config or app_name not in app_executer:
            print("<font color=\"red\"> Config corrupted! no "+str(app_name)+" defined.</font>")
        else:
            app_details = config[app_name].split(";")
            if app_details[1] == "0":
                print("<font color=\"blue\"> "+str(app_name)+" skipped.</font>")
            else:
                print("\n\n<font color=\"green\"> Executing " + str(app_name) + "...</font>")
                print(header(app_name))
                app_output_files.append(app_executer[app_name](app_details[0], config["DataFilePath"]))
        print("APP_PROCESSING_FINISHEDsdsdmdfdkflsdkdsl")

    with ZipFile('GENERAL_OUTPUT.zip', 'w') as myzip:

        for output in app_output_files:
            if output and os.path.isfile(output):
                myzip.write(output)
    return

if __name__ == '__main__':
    execute_programs()