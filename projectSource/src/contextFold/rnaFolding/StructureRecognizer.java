/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.rnaFolding;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import contextFold.common.Env;

// recognize specific sub-structures of an RNA structure, 
// such as a loop, a stem, and so on.
//
// requirements:
//    - the recognized types should be mutual exclusive.
//    - every contextFold.rna structure should be covered by a type.
//  this means:
//       getType(fold,i,j) can return just one value (duh..) and should never fail.
public interface StructureRecognizer extends Serializable {

	public static final int UNPAIRED_BASE = -1;
	public int MIN_HAIRPIN_LENGTH = Env.MIN_HAIRPIN_LENGTH;
	
	int numTypes();
	
	boolean isBaseType(int type);
	boolean isBasepairType(int type);
	boolean isLengthType(int type);

	int getFirstBaseType();
	int getLastBaseType();
	int getFirstBasepairType();
	int getLastBasepairType();
	int getFirstLengthType();
	int getLastLengthType();

	Collection<int[]> getCombinedElements(int fold[]);

	String getElementName(int elementType);
	
	String elementToString(int[] element);


	void sortElements(List<int[]> elements);


}
