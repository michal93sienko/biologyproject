/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.rnaFolding;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import contextFold.common.Env;
import contextFold.common.Utils;
import contextFold.features.FeatureManager;
import contextFold.rna.OneLineReader;
import contextFold.rna.RNA;
import contextFold.rna.RNAListReader;
import contextFold.scoring.StructreElementsScorer;
import contextFold.scoring.StructreElementsScorerImpl;



/**
 * An object that implements the (dynamic
 * programming Zuker-like) RNA folding prediction algorithm.
 * 
 * The implementation utilizes sparsification techniques, which
 * were presented in:
 *  <a href="http://dx.doi.org/10.1016/j.jda.2010.09.001">
 *  Rolf Backofen, Dekel Tsur, Shay Zakov, and Michal Ziv-Ukelson.
 *  Sparse RNA Folding: Time and Space Efficient Algorithms. 
 *  Journal of Discrete Algorithms, Volume 9, Issue 1, March 2011, Pages 12-31,
 *  20th Anniversary Edition of the Annual Symposium on Combinatorial Pattern 
 *  Matching (CPM 2009)</a>.
 * 
 */
@SuppressWarnings("static-access")
public final class Folder implements Fold{


	private StructureRecognizerImpl csr;
	private static final int PAIRED = Traceback.PAIRED;
	private static final int UNPAIRED = Traceback.UNPAIRED;
	private static final int BEST = 2;

	private static final int INF = Integer.MIN_VALUE/3;

	private int MIN_HAIRPIN_LENGTH;
	private int SHORT_SIDE_FACTOR; // used for deciding indices in the il array.


	//	 Dynamic programming score tables
	//	---------------------------------

	/**
	 *  The score of the substring s[i,n], for the paired, unpaired, and best 
	 *  cases. Format: scores_3[i][type], where i in 0..n-1 and type in 
	 *  {PAIRED, UNPAIRED, BEST}.
	 */
	private int[][] scores_3 = null;

	/**
	 * The score of the substring s[i,j], given that i and j are paired 
	 * (co-terminus score). Format: cot[ij], where ij = i*n+j, i,j in 0..n-1. 
	 * See also "cot invariant".
	 */
	private int[] cot = null;

	/**
	 * The score of the substring s[i,j], given that the interval [i,j] is 
	 * enclosed within a multiloop, and it contains at least one branch. 
	 * Format as for cot.
	 */
	private int[] ml1 = null;  

	/**
	 * The score of the substring s[i,j], given that the interval [i,j] is 
	 * enclosed within a multiloop, and it contains at least two branches. 
	 * Format as for cot.
	 */
	private int[] ml2 = null;

	/**
	 * The score of the substring s[i,j], given that the interval [i,j] 
	 * contains the opening branch of an internal-loop. Format: il[ix], where 
	 * ix encodes the indices i and j, the shorter side of the loop (left/right),
	 * and how many unpaired bases are in the shorter side. The encoding is as 
	 * follows: ix = (shortLength + shortSide*SHORT_SIDE_FACTOR)*n^2 + i*n+j, 
	 * where SHORT_SIDE_FACTOR = csr.IL_MAX_IX+1, shortLength in 0..csr.IL_MAX_IX, 
	 * shortSide in {LEFT, RIGHT}, i,j in 0..n-1. See also "il invariant".
	 */
	private int[] il = null;  

	//	Auxiliary arrays for score computations
	//  ---------------------------------------

	/**
	 * The end indices of OCT substrings. Format: j = rowOctIxs[i][k], where
	 * s[i,j] is the k-th OCT that starts an index i.  
	 */
	private int[][] rowOctIxs = null;

	/**
	 * Used for computing rowOctIxs[i] for the current row i. 
	 */
	private int[] currRowOctIxs = null;

	/**
	 * Used for computing scores of unpaired bases within hairpins.
	 * hpAccumBases[i] is the sum of scores of hairpin unpaired
	 * bases at positions 0..i.
	 */
	private int[] hpAccumBases = null;

	/**
	 * Scores of unpaired bases within internal loops. Format: ilBaseScores[ix], 
	 * where ix encodes the index i, the shorter side of the loop (left/right),
	 * and how many unpaired bases are in the shorter side. The encoding is as 
	 * follows: ix = (shortLength + shortSide*SHORT_SIDE_FACTOR)*n + i, 
	 * where SHORT_SIDE_FACTOR = csr.IL_MAX_IX+1, shortLength in 0..csr.IL_MAX_IX, 
	 * shortSide in {LEFT, RIGHT}, i in 0..n-1. 
	 */
	private int[] ilBaseScores = null; 

	/**
	 * Used for computing scores of unpaired bases within internal loops.
	 * Format is as for ilBaseScores, and the summation is as in hpAccumBases.
	 */
	private int[] ilAccumBases = null; 

	/**
	 * Scores for unpaired bases within multiloops.
	 */
	private int[] mlBaseScores = null;


	// auxiliary arrays for il computations, for loops enclosed by the 
	// base-pairs (i, j) and (p, q), where i < p < q < j.
	private static final int[] step = {-1, 1}; // advancing i by decreasing its value by 1, advancing j by increasing its value by 1.
	private final int[] ilIxs = {0,0}; // {i,j}.
	private final int[] ilLengths = {0,0}; // {p-i-1, j-q-1}.
	private final int[] ilLengthIxs = {0,0}; // {p-i-1, j-q-1}.
	private final int[] maxLengths = {0,0}; // induce i = 0 and j = n-1.
	private int maxLength = 0;

	private static final int SHORT = 0, LONG = 1;
	private static final int LEFT = 0, RIGHT = 1;
	private static final int[] LENGTHS = {SHORT, LONG};

	private Traceback traceback = new Traceback();

	//scoring model arrays:

	/**
	 * Contains feature scores. Format: scores[elementType][template][localFeature],
	 * where 'elementType' denotes the structural element type, 'template' denotes
	 * a feature template (i.e. one of the examined sequential contexts), and
	 * localFeature is the feature index within the corresponding template.
	 */
	private int[][][] scores; 

	/**
	 * The sequential context local features of the input string. Format:
	 * features[elementType][template][index], where 'elementType' and 'template'
	 * are as for 'scores', and index is either in the range 0..n-1 (for single
	 * position features) or in the range 0..n^2-1 (for double position features,
	 * encoding pairs (i, j) by index = i*n+j).  
	 */
	private int[][][] features;

	/**
	 * Auxiliary array, holding arrays of the form scores[elementType]. 
	 */
	private int[][] typeScores;

	/**
	 * Auxiliary array, holding arrays of the form features[elementType]. 
	 */
	private int[][] typeFeatures;

	/**
	 * Holds hairpin length feature scores, where hpLengthScores[l] is the 
	 * score of a hairpin of length 'l'. 
	 */
	int[] hpLengthScores;

	/**
	 * Holds internal loop length feature scores, where ilLengthScores[k][l] is
	 * the score of a short/medium internal loop which short side is of length
	 * 'k', and long side is of length 'l'. 
	 */
	int[][] ilLengthScores;

	public FeatureManager fm;

	// DEBUG and statistics
	public long[] octPerLength = new long[0];
	public long[] splitsPerLength = new long[0];
	public long[] seqPerLength = new long[0];
	public long concatanations = 0;
	public long naivSplits = 0;
	public boolean sparseByOcts = true;
	public boolean sparseBySteps = true;
	private boolean echo = false;
	private boolean countOcts = false;


	/**
	 * Constructs a Folder object. Note that this folder will throw
	 * exceptions if used before setting a feature manager.
	 */
	public Folder(){}

	/**
	 * Construct a folder which uses the scoring model of the given
	 * feature manager.
	 *  
	 * @param featureManager a FeatureManager object that defines a
	 * scoring model.
	 */
	public Folder(FeatureManager featureManager){
		this();
		setFeatureManager(featureManager);
	}

	/**
	 * Construct a folder which uses the scoring model of the given
	 * feature manager.
	 * 
	 * @param modelPath a path to a serialized (trained) FeatureManager object.
	 * @throws IOException - an I/O error has occurred. 
	 * @throws ClassNotFoundException - Class of a serialized object cannot be found. 
	 */
	public Folder(String modelPath) throws IOException, ClassNotFoundException{
		this(Utils.load(FeatureManager.class, modelPath));
	}

	/**
	 * A setter to the feature manager of the folder (the scoring model).
	 * @param featureManager a FeatureManager object.
	 */
	public void setFeatureManager(FeatureManager featureManager){
		if (fm != featureManager){
			fm = featureManager;
			fm.getFeatureWeights().uncompress();
			csr = (StructureRecognizerImpl) fm.sr;
			MIN_HAIRPIN_LENGTH = csr.MIN_HAIRPIN_LENGTH;
			SHORT_SIDE_FACTOR = csr.IL_MAX_IX+1;
			initializeTables(-1);
		}
	}

	/**
	 * A setter to the feature manager of the folder (the scoring model).
	 * 
	 * @param modelPath a path to a serialized (trained) FeatureManager object.
	 * @throws IOException - an I/O error has occurred. 
	 * @throws ClassNotFoundException - Class of a serialized object cannot be found. 
	 */
	public void setFeatureManager(String modelPath) throws IOException, ClassNotFoundException{
		setFeatureManager(Utils.load(FeatureManager.class, modelPath));
	}

	//for non-inline computation:
	/**
	 * Computes the score of a structural element, by summing up all of its
	 * feature scores.
	 * 
	 * @param elementType the type of the element.
	 * @param val the index of the element for an unpaired base element, the
	 * pair encoding for a base-pair element (val = i*n+j), or the length for
	 * a length element.
	 * 
	 * @return the element score.
	 */
	private final int elementScore(int elementType, int val){
		int elementScore = 0;
		typeScores = scores[elementType];
		typeFeatures = features[elementType];
		for (int template = typeFeatures.length-1; template >= 0; --template){
			elementScore += typeScores[template][typeFeatures[template][val]];
		}
		return elementScore;
	}


	/**
	 * Fills the dynamic programming scoring tables.
	 * 
	 * @param es a {@code StructreElementsScorer} object that defines 
	 * structural element scores (this object wraps the input RNA string).
	 * @param optFoldings a handle in which computed optimal solutions are 
	 * returned. Currently only one optimal solution is computed (might change
	 * in the future).
	 * @param numOfOutputFoldings CURRENTLY NOT WORKING. In the future: allows 
	 * to specify the number of returned (sub) optimal foldings.
	 * 
	 * @return the score of the optimal folding.
	 */
	public float computeDPTables(StructreElementsScorer es, List<int[]> optFoldings, int numOfOutputFoldings){
		int n = es.sequenceLength(); // the input sequence length.
		int nSqr = n*n;
		initializeTables(n); // allocates scoring tables and initializes them.
		initializeScores(es); // initializes the auxiliary scoring arrays.

		echo = contextFold.common.Env.ECHO;

		long currConcatanations = 0;

		int currOctNum;
		//		int octCount = 0;
		//		int stepCount = 0;
		int longSide;

		int tmpScore, currScore, longIlScore;
		int external3BasesScore = 0, externalBasesScore;

		int ij, in, inn; // ij = i*n+j, in = i*n, inn = (i+1)*n

		scores_3[n][BEST] = 0;

		// iterating over all substring start position i:
		for (int i = n-1; i>=0; --i){

			currOctNum = 0; // the number of OCTs starting at i.
			in = i*n;
			inn = in+n;

			// the score of the base at position i, given that it is unpaired,
			// within a multiloop.
			int iUnpairedScore = mlBaseScores[i]; 

			// initialization of ml2 row scores:
			for (int j = i+1; j<n; ++j) {
				ml2[in+j] = ml2[inn+j] + iUnpairedScore;
			}

			// iterating over all substring end positions j, and computing the
			// corresponding score entries:
			for (int j = i+MIN_HAIRPIN_LENGTH+1; j<n; ++j){
				ij = in+j;

				//********************* computing co-terminus scores *************************************

				// 'cot' invariant: cot[ij] contains the best short il score. 
				// 'il' invariant: 
				//		(1) il[(shortLength+shortSide*())*n^2+ij] contains the best shortLength-shortSide-medium 
				//			il score, without the score of the closing base-pair (i,j).
				//		(2) il[(shortLength+shortSide*())*n^2+i'j'] contains the best shortLength-shortSide-long 
				//			il score, without the score of the closing base-pair (i',j'), for every 
				//			i <= i' < j' <= j s.t. (i',j') != (i,j).

				currScore = cot[ij]; // the best 'short' il score.

				//hairpin score:
				int hpTypeIx = j-i-1;
				if (hpTypeIx > csr.HP_MAX_IX) hpTypeIx = csr.HP_MAX_IX;

				tmpScore = (
						elementScore(csr.HP_CLOSE[hpTypeIx], ij) + 
						hpLengthScores[j-i-1] +
						hpAccumBases[j-1]-hpAccumBases[i]); 
				if (currScore < tmpScore) currScore = tmpScore;

				longIlScore = INF;

				// medium-long il score:
				for (int shortLength = 0; shortLength <= csr.IL_MAX_IX; ++shortLength){ //iterating on all il tables
					int longLength = csr.IL_LONG[shortLength];
					for (int shortSide=0; shortSide <=1; ++shortSide){ //iterating on left/right short sides
						if (shortLength == longLength && shortSide == RIGHT){
							++longLength;
						}
						int shortShift = shortSide*SHORT_SIDE_FACTOR;
						if (j-i > shortLength+longLength + MIN_HAIRPIN_LENGTH + 2){
							longSide = 1-shortSide;
							int longShift = longSide*SHORT_SIDE_FACTOR;

							ilIxs[0] = i;
							ilIxs[1] = j;
							ilIxs[longSide] -= step[longSide]; 

							// computing shortSide-long il score:
							longIlScore = (
									il[(shortLength+shortShift)*nSqr+ilIxs[0]*n+ilIxs[1]] +
									ilBaseScores[(shortLength + SHORT_SIDE_FACTOR)*n+ilIxs[longSide]]
							);
							ilIxs[longSide] += step[longSide]; 

							if (shortLength == csr.IL_MAX_IX){
								// computing shortSide-long il score:
								tmpScore = (
										il[(csr.IL_MAX_IX+shortShift)*nSqr+ij+n-1] +
										ilBaseScores[(shortLength+shortShift)*n+i+1] +
										ilBaseScores[(shortLength+longShift)*n+j-1]
								);
								if (longIlScore < tmpScore){
									longIlScore = tmpScore;
								}
							}


							// setting 'closeIxs' to actually contain extreme open Ixs for shortSide-long il: 
							ilIxs[shortSide] -= step[shortSide]*(shortLength+1);
							ilIxs[longSide] -= step[longSide]*(longLength+1);
							ilLengthIxs[shortSide] = shortLength;
							ilLengthIxs[longSide] = csr.IL_MEDIUM[shortLength];

							ilLengths[0] = ilIxs[0]-i-1;
							ilLengths[1] = j-ilIxs[1]-1;
							tmpScore = (
									cot[ilIxs[0]*n+ilIxs[1]] + 
									elementScore(csr.IL_OPEN[ilLengthIxs[1]][ilLengthIxs[0]], ilIxs[1]*n+ilIxs[0]) +
									ilAccumBases[(shortLength+shortShift)*n+ilIxs[0]-1] - //p-1
									ilAccumBases[(shortLength+shortShift)*n+i] +
									ilAccumBases[(shortLength+longShift)*n+j-1] -
									ilAccumBases[(shortLength+longShift)*n+ilIxs[1]] //q
							);

							if (longIlScore < tmpScore){
								longIlScore = tmpScore;
							}

							tmpScore = elementScore(csr.IL_CLOSE[ilLengthIxs[0]][ilLengthIxs[1]], ij);
							if (longIlScore > il[(shortLength+shortShift)*nSqr+ij]){
								tmpScore += longIlScore;
							}
							else tmpScore += il[(shortLength+shortShift)*nSqr+ij];
							if (currScore < tmpScore) currScore = tmpScore;

							il[(shortLength+shortShift)*nSqr+ij] = longIlScore; 
						}
						else {
							il[(shortLength+shortShift)*nSqr+ij] = INF;
						}
					}

				}

				//multiloop score:
				tmpScore = elementScore(csr.ML_CLOSE, ij) + ml2[ij+n-1];
				if (currScore < tmpScore) currScore = tmpScore;

				cot[ij] = currScore;

				//forward updating internal loop scores:
				if (cot[ij] != INF){
					maxLengths[0] = i-1; // induce i = 0.
					maxLengths[1] = n-j-2; // induce j = n-1.
					updateIlInvariant(i,j,n);
				}

				//********************* computing ml2 score: *************************************

				int jUnpairedScore = mlBaseScores[j]; 

				if (ml2[ij] < ml2[ij-1] + jUnpairedScore){ 
					ml2[ij] = ml2[ij-1] + jUnpairedScore;
				}

				//********************* computing ml1 score: *************************************
				currScore = ml1[ij+n]+iUnpairedScore;

				if (currScore < ml1[ij-1] + jUnpairedScore){
					currScore = ml1[ij-1] + jUnpairedScore;
				}

				if (currScore < ml2[ij]){
					currScore = ml2[ij];
				}

				int branchelementScore = elementScore(csr.ML_OPEN, j*n+i);
				if (currScore < cot[ij] + branchelementScore){
					//an OCT is found
					currScore = cot[ij] + branchelementScore;
					currRowOctIxs[currOctNum] = j;
					++currOctNum;
					//					++octCount;
				}

				if (!sparseByOcts && currRowOctIxs[currOctNum-1] != j){
					currRowOctIxs[currOctNum] = j;
					++currOctNum;
					//					++octCount;
				}

				ml1[ij] = currScore;

				if ((!sparseBySteps || currScore > ml1[ij+n]+iUnpairedScore) && j+1 < n){ 
					// a step is found
					// ++stepCount;
					currConcatanations += rowOctIxs[j+1].length;
					int jnn = j*n+n;
					for (int k=rowOctIxs[j+1].length-1; k>=0; --k){
						int j2 = rowOctIxs[j+1][k];
						++splitsPerLength[j2-i+1];
						if (ml2[in+j2] < ml1[ij] + ml1[jnn+j2]){
							ml2[in+j2] = ml1[ij] + ml1[jnn+j2];
						}
					}
				}
			}

			//maintaining ml1 OCTs:
			rowOctIxs[i] = Arrays.copyOf(currRowOctIxs, currOctNum);

			// computing scores_3:
			scores_3[i][PAIRED] = INF;
			scores_3[i][UNPAIRED] = INF;

			currScore = INF;
			for (int j = i+MIN_HAIRPIN_LENGTH+1; j<n; ++j){
				tmpScore = cot[in+j] + 
				elementScore(csr.XL_OPEN, j*n+i) + scores_3[j+1][BEST]; 
				if (currScore < tmpScore) {
					currScore = tmpScore;
				}
			}
			scores_3[i][PAIRED] = currScore;

			external3BasesScore += elementScore(csr.XI_BASE_3, i);
			currScore = external3BasesScore + elementScore(csr.XI_LENGTH_3, n-i);

			externalBasesScore = 0;
			int externalIntervalScore;
			for (int j=i; n-1-j > MIN_HAIRPIN_LENGTH; ++j){
				if (i>0) {
					externalBasesScore += elementScore(csr.XI_BASE_MID, j);
					externalIntervalScore = elementScore(csr.XI_LENGTH_MID, j-i+1);
				}
				else {
					externalBasesScore += elementScore(csr.XI_BASE_5, j);
					externalIntervalScore = elementScore(csr.XI_LENGTH_5, j+1);
				}

				tmpScore = externalIntervalScore + externalBasesScore
				+ scores_3[j+1][PAIRED]; 
				if (currScore < tmpScore) currScore = tmpScore;
			}
			scores_3[i][UNPAIRED] = currScore;

			scores_3[i][BEST] = Math.max(scores_3[i][PAIRED], scores_3[i][UNPAIRED]);
		}

		if (echo) {
			//		System.out.println("OCT proportion: " + ((float) octCount)/subseqNum); 
			//		System.out.println("Step proportion: " + ((float) stepCount)/subseqNum); 
			System.out.println("Score: " + scores_3[0][BEST]); 
			//		System.out.println("***********************************************\n"); 
		}

		concatanations += currConcatanations;

		if (optFoldings != null) {
			int[] optFold = traceback.traceback(scores_3, cot, il, ml1, ml2, 
					rowOctIxs, es, n, hpAccumBases, hpLengthScores, 
					ilBaseScores, mlBaseScores, ilLengthScores, ilAccumBases, csr);

			optFoldings.add(optFold);

			//DEBUG
			if (Env.DEBUG_MODE){
				Collection<int[]> elements = csr.getCombinedElements(optFold);
				int score=0;
				for (int[] element : elements) {
					score += elementScore(element[0], element[1]); 
				}
				if (score != scores_3[0][BEST]){ 
					System.err.println("Un equal algorithm and folding scores: algorithm score is " +scores_3[0][BEST] +
							", where folding score is " + score);
					System.err.println(csr.elementsString(optFold));
				}
			}
		}

		// Accumulating the number of splits examined by the standard algorithm,
		// for assessing sparsification statistics:
		naivSplits += naivSplits(n);
		if(countOcts){
			for (int i=0; i<n; ++i){
				seqPerLength[i+1] += n-i;
				for (int j:rowOctIxs[i]){
					++octPerLength[j-i+1];
				}
			}
		}

		return scores_3[0][BEST];
	}

	// updating forwards short-medium il scores:
	private final void updateIlInvariant(int p, int q, int n) {

		int tmpScore;
		int maxShortLength, maxLongLength;
		int initScore;
		int nonShortOpenScore;
		//		int shortLength, longLength;
		int elementType;
		int qp = q*n+p;
		int pq = p*n+q;


		for (int shortSide = LEFT; shortSide <= RIGHT; ++shortSide){
			int longSide = 1-shortSide;
			int shortShift = shortSide*SHORT_SIDE_FACTOR;
			int longShift = longSide*SHORT_SIDE_FACTOR;

			// short ils update cot scores:
			maxShortLength = csr.IL_SHORT_MAX_IX;
			if (maxShortLength > maxLengths[shortSide]){
				maxShortLength = maxLengths[shortSide];
			}

			for (ilLengths[shortSide] = 0; ilLengths[shortSide] <= maxShortLength; ++ilLengths[shortSide]){
				ilLengths[longSide] = ilLengths[shortSide];
				if (shortSide == RIGHT) ++ilLengths[longSide];

				maxLongLength = csr.IL_MEDIUM[ilLengths[shortSide]]-1;
				if (maxLongLength > maxLengths[longSide]){
					maxLongLength = maxLengths[longSide];
				}
				if (maxLongLength < ilLengths[longSide]) break;

				ilIxs[0] = p - ilLengths[0] - 1; //i
				ilIxs[1] = q + ilLengths[1] + 1; // j

				initScore = cot[pq] + (
						ilAccumBases[(ilLengths[shortSide]+shortShift)*n+p-1] - 
						ilAccumBases[(ilLengths[shortSide]+shortShift)*n+ilIxs[0]] + //i
						ilAccumBases[(ilLengths[shortSide]+longShift)*n+ilIxs[1]-1] - //j-1
						ilAccumBases[(ilLengths[shortSide]+longShift)*n+q]
				);


				for (; ilLengths[longSide] <= maxLongLength; ++ilLengths[longSide], ilIxs[longSide]+=step[longSide]){
					tmpScore = (
							initScore + ilLengthScores[ilLengths[shortSide]][ilLengths[longSide]]+ 
							elementScore(csr.IL_OPEN[ilLengths[1]][ilLengths[0]], qp) +
							elementScore(csr.IL_CLOSE[ilLengths[0]][ilLengths[1]], ilIxs[0]*n+ilIxs[1])
					); 	

					if (cot[ilIxs[0]*n+ilIxs[1]] < tmpScore) {
						cot[ilIxs[0]*n+ilIxs[1]] = tmpScore;
					}
					initScore += ilBaseScores[(ilLengths[shortSide]+csr.IL_MAX_IX+1)*n+ilIxs[longSide]];
				}
			}


			// medium ils update il scores:
			maxShortLength = csr.IL_MAX_IX;
			if (maxShortLength > maxLengths[shortSide]){
				maxShortLength = maxLengths[shortSide];
			}

			for (ilLengths[shortSide] = 0; ilLengths[shortSide] <= maxShortLength; ++ilLengths[shortSide]){
				ilLengths[longSide] = csr.IL_MEDIUM[ilLengths[shortSide]];
				if (ilLengths[longSide] == ilLengths[shortSide] && shortSide == RIGHT) {
					++ilLengths[longSide];
				}

				maxLongLength = csr.IL_LONG[ilLengths[shortSide]]-1;
				if (maxLongLength > maxLengths[longSide]){
					maxLongLength = maxLengths[longSide];
				}
				if (maxLongLength < ilLengths[longSide]) break;


				ilIxs[0] = p - ilLengths[0] - 1; //i
				ilIxs[1] = q + ilLengths[1] + 1; //j

				if (shortSide == LEFT){
					elementType = csr.IL_OPEN[csr.IL_MEDIUM[ilLengths[LEFT]]][ilLengths[LEFT]];
				}
				else{
					elementType = csr.IL_OPEN[ilLengths[RIGHT]][csr.IL_MEDIUM[ilLengths[RIGHT]]];
				}
				nonShortOpenScore = elementScore(elementType, qp);

				initScore = cot[pq] + nonShortOpenScore + (
						ilAccumBases[(ilLengths[shortSide]+shortShift)*n+p-1] - 
						ilAccumBases[(ilLengths[shortSide]+shortShift)*n+ilIxs[0]] + //i
						ilAccumBases[(ilLengths[shortSide]+longShift)*n+ilIxs[1]-1] - //j-1
						ilAccumBases[(ilLengths[shortSide]+longShift)*n+q]
				);


				for (; ilLengths[longSide] <= maxLongLength; ++ilLengths[longSide], ilIxs[longSide]+=step[longSide]){
					tmpScore = initScore + ilLengthScores[ilLengths[shortSide]][ilLengths[longSide]];	
					if (il[(ilLengths[shortSide]+shortShift)*n*n+ilIxs[0]*n+ilIxs[1]] < tmpScore){
						il[(ilLengths[shortSide]+shortShift)*n*n+ilIxs[0]*n+ilIxs[1]] = tmpScore;
					}
					initScore += ilBaseScores[(ilLengths[shortSide]+csr.IL_MAX_IX+1)*n+ilIxs[longSide]];
				}
			}

		}
	}



	public void initializeTables(int n) {
		if (n==-1 || n > maxLength){
			//			System.gc();
			if (n > maxLength) maxLength = n;

			cot = new int[maxLength*maxLength]; // contains the best score for the substring [i..j] for some j, given that i and j are paired (optimal co-terminus folding score).
			scores_3 = new int[maxLength+1][3]; // contains the best score for the substring [i..n] (optimal folding score), for the paired, unpaired, and best cases.
			ml1 = new int[maxLength*maxLength]; // contains the best score for the substring [i..j], given that the interval [i...j] is contained within a multiloop. 
			ml2 = new int[maxLength*maxLength]; // contains the best score for the substring [i..j], given that the interval [i...j] is contained within a multiloop.
			il = new int[2*(csr.IL_MAX_IX+1)*n*n+maxLength*maxLength];

			rowOctIxs = new int[maxLength][];
			currRowOctIxs = new int[maxLength];

			hpLengthScores = new int[maxLength];
			hpAccumBases = new int[maxLength];
			ilBaseScores = new int[(csr.IL_MAX_IX+1)*2*maxLength];
			ilAccumBases = new int[(csr.IL_MAX_IX+1)*2*maxLength];
			mlBaseScores = new int[maxLength];

			if (n == -1 || ilLengthScores == null){
				ilLengthScores = new int[csr.IL_LONG.length][];
				for (int i=0; i<csr.IL_LONG.length; ++i){
					ilLengthScores[i] = new int[csr.IL_LONG[i]];
				}
			}

			octPerLength = Arrays.copyOf(octPerLength, maxLength+1);
			splitsPerLength = Arrays.copyOf(splitsPerLength, maxLength+1);
			seqPerLength = Arrays.copyOf(seqPerLength, maxLength+1);
		}

		Arrays.fill(cot, INF);
		Arrays.fill(ml1, INF);
		Arrays.fill(ml2, INF);
		Arrays.fill(il, INF);
		for (int[] arr : scores_3) Arrays.fill(arr, INF);
	}

	private void initializeScores(StructreElementsScorer es) {
		scores = ((StructreElementsScorerImpl) es).getScores();
		features = ((StructreElementsScorerImpl) es).getFeatures();

		int n = es.sequenceLength();
		for (int i=1; i<n-1; ++i){
			hpLengthScores[i] = elementScore(csr.HP_LENGTH, i);;
			hpAccumBases[i] = hpAccumBases[i-1] + elementScore(csr.HP_BASE, i);
			mlBaseScores[i] = elementScore(csr.ML_BASE, i); 

			for (int shortSide = 0; shortSide <= csr.IL_MAX_IX; ++shortSide){
				for (int length : LENGTHS){
					ilBaseScores[(shortSide+length*(csr.IL_MAX_IX+1))*n+i] = elementScore(csr.IL_BASE[shortSide][length], i);
					ilAccumBases[(shortSide+length*(csr.IL_MAX_IX+1))*n+i] = 
						ilAccumBases[(shortSide+length*(csr.IL_MAX_IX+1))*n+i-1] + 
						ilBaseScores[(shortSide+length*(csr.IL_MAX_IX+1))*n+i];  
				}

			}
		}

		for (int shortLength = 0; shortLength <= csr.IL_MAX_IX; ++shortLength){
			for (int longLength = shortLength; longLength < csr.IL_LONG[shortLength]; ++longLength){
				ilLengthScores[shortLength][longLength] = 
					elementScore(csr.IL_MEDIUM_LENGTH[shortLength], longLength);
			}
		}
	} 

	public static void printStructure(int[] S, int[] matchIxs){
		for (int i=0; i<S.length; ++i) {
			System.out.print(getLetter(S[i]) + " ");
		}
		System.out.println();

		for (int i=0; i<S.length; ++i) {
			System.out.print(getStructure(i, matchIxs[i]) + " ");
		}
	}


	private static String getStructure(int i, int j) {
		if (j==-1) return ".";
		else if (j>i) return "(";
		else return ")";
	}


	private static String getLetter(int letterEncoding) {
		switch (letterEncoding){
		case 0: return "A";
		case 1: return "C";
		case 2: return "G";
		case 3: return "U";
		default: return "N";
		}
	}


	/**
	 * Computes the number of split-points needed to be examined in order
	 * to find optimal branching structure, in which both subsequence 
	 * endpoints i and j are paired (not to each other). 
	 * 
	 * <p>
	 * The minimum 
	 * split-point index is i+MIN_HAIRPIN_LENGTH+2, and the maximum index
	 * is j-MIN_HAIRPIN_LENGTH-1, and thus there are j-i-2*MIN_HAIRPIN_LENGTH-2
	 * split-points to examine (or 0, if j-i < 2*MIN_HAIRPIN_LENGTH+2).
	 * The return value of this function is the result of summing the above
	 * term for all pairs of indices i,j in the sequence.	 
	 * 
	 * @param n sequence length.
	 * @return the number of split-points which are examined by
	 * the standard algorithm.
	 */
	public long naivSplits(long n){
		long x = MIN_HAIRPIN_LENGTH*2+3;
		return (n*n*n + 3*(1-x)*n*n + (3*x*x-6*x +2)*n - x*(x-1)*(x-2))/6;
	}

	@Override
	public List<RNA> tagMany(String listInput) {
		List<int[]> optFoldings = new ArrayList<int[]>(1);
		RNAListReader reader = new OneLineReader();
		List<RNA> rnas = reader.readList(listInput);
		for (RNA rna : rnas){
			if (rna.length > Env.MAX_SERVER_SEQUENCE_LENGTH) {
				System.err.println("Server inputs are limited to 1000 bases or " +
						"less. Ignoring the following FASTA item:\n" + rna); 
			}
			else{
				StructreElementsScorer es = fm.getElementScorer(rna);
				computeDPTables(es, optFoldings , 1);
				rna.addFold(optFoldings.get(0)); 
				optFoldings.clear();
			}
		}
		return rnas;
	}

	@Override
	@Deprecated
	public RNA tagOne(String sequence) {
		RNA folded = null;
		if (sequence.length()>1000) { return null; }
		try{
			List<int[]> optFoldings = new ArrayList<int[]>();
			folded = new RNA(sequence.trim());
			StructreElementsScorer es = fm.getElementScorer(folded);
			computeDPTables(es, optFoldings , 1);
			folded.addFold(optFoldings.get(0)); 
		}
		catch (Exception e){
			System.err.println("Could not fold the string:\n" + sequence);
			return null;
		}
		return folded;
	}

	public String predictFolding(String sequence) throws IOException, ClassNotFoundException {
		RNA folded = null;
		try{
			List<int[]> optFoldings = new ArrayList<int[]>();
			folded = new RNA(sequence.trim());
			StructreElementsScorer es = fm.getElementScorer(folded);
			computeDPTables(es, optFoldings , 1);
			folded.addFold(optFoldings.get(0)); 
		}
		catch (Exception e){
			System.err.println("Could not fold the string:\n" + sequence);
		}
		return RNA.foldToBracets(folded.lastPrediction());
	}

	/**
	 * 
	 */
	public void resetCounts() {
		Arrays.fill(octPerLength, 0);
		Arrays.fill(seqPerLength, 0);
		Arrays.fill(splitsPerLength, 0);
		concatanations = 0;
		naivSplits = 0;
	}

	public static void main(String[] args) throws IOException, ClassNotFoundException{
		Fold folder = new Folder(Env.defaultModel);
		System.out.println(folder.tagOne(args[0]));
	}


} //public class Folder 
