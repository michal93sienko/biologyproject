/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.rnaFolding;

import java.util.Arrays;


import contextFold.common.Env;
import contextFold.common.Utils;
import contextFold.scoring.StructreElementsScorer;


/**
 * Contains methods for tracing-back score tables,
 * in order to produce optimal foldings. 
 * 
 *
 */
public class Traceback {

	private static final int[] step = {1,-1};
	public static final int JUST_ONE_BRANCH = 0;  
	public static final int MORE_THAN_ONE_BRANCH = 1; 

	public static final int PAIRED = 0; // for external traceBack. 
	public static final int UNPAIRED = 1; // for external traceBack. 
	public static final int BEST = 2; // for external traceBack. 
	private static int LEFT = 0;
	private static int RIGHT = 1;


	private StructureRecognizerImpl csr;

	private int[] ilIxs = new int[2];
	private int[] ilLengths = new int[2];
	private int[] ilLengthIxs = new int[2];


	private int[][] scores_3;
	int[] cot, ml1, ml2;
	private int[] il;
	private int[][] rowOctIxs;
	private StructreElementsScorer es;
	private int n;
	private int nSqr;
	private int[] fold;

	private int[] hpBaseAccumScores, hpLengthScores, mlBaseScores;
	private int[] ilBaseScores;
	private int[] ilAccumBases;
	private int[][] ilLengthScores;

	@SuppressWarnings("static-access")
	protected int[] traceback(int[][] _scores_3, int[] _cot,
			int[] _il, int[] _ml1, int[] _ml2, int[][] _rowOctIxs,
			StructreElementsScorer _es, int _n, int[] _hpBaseAccumScores,
			int[] _hpLengthScores, int[] _ilBaseScores,
			int[] _mlBaseScores, int[][] _ilLengthScores, int[] _ilAccumBases, 
			StructureRecognizerImpl _csr) {

		scores_3 = _scores_3;
		cot = _cot;
		ml1 = _ml1;
		ml2 = _ml2;
		il = _il;
		rowOctIxs = _rowOctIxs;
		es = _es;
		n = _n;
		nSqr = n*n;
		hpBaseAccumScores = _hpBaseAccumScores;
		hpLengthScores =_hpLengthScores;
		ilBaseScores = _ilBaseScores;
		ilAccumBases = _ilAccumBases;
		mlBaseScores = _mlBaseScores;
		ilLengthScores = _ilLengthScores;
		csr = _csr;


		fold = new int[n];
		Arrays.fill(fold, csr.UNPAIRED_BASE);
		traceback3(csr.XI_BASE_5, csr.XI_LENGTH_5, 0);
		return fold;
	}


	/**
	 * Resolving the next external branch, from left (5') to right (3').
	 * @param leftXIBaseType
	 * @param leftXILengthType
	 * @param i the starting index of the string's 3' suffix.
	 */
	@SuppressWarnings("static-access")
	public void traceback3(int leftXIBaseType, int leftXILengthType,
			int i) {

		if (i == n) return;

		int xiBasesScore = 0;
		int xiLengthScore = 0;

		int j = i;

		// finding unpaired left region (j is the first paired base from left):
		while (j < n && scores_3[j][PAIRED] + xiBasesScore + xiLengthScore < scores_3[i][BEST]){// Utils.floatSmaller(scores_3[j][PAIRED] + 
			//				xiBasesScore + xiLengthScore, scores_3[i][BEST])){
			xiBasesScore += es.elementScore(leftXIBaseType, j);
			++j;
			xiLengthScore = es.elementScore(leftXILengthType, j-i);
		}

		// If no unpaired left region with corresponding score was found,
		// the optimal folding is a 3' end unpaired interval 
		if (j == n){
			if (Env.DEBUG_MODE){
				xiLengthScore = es.elementScore(csr.XI_LENGTH_3, j-i);
				xiBasesScore = 0;
				for(j=i; j<n; ++j) xiBasesScore += es.elementScore(csr.XI_BASE_3, j);
				assert xiBasesScore + xiLengthScore == scores_3[i][BEST]; //Utils.floatEquals(xiBasesScore + xiLengthScore, scores_3[i][BEST]);
			}
		}
		// Else, j is the first index of the next external branch.
		// We next find the index to which j is paired: 
		else{
			int k = j+csr.MIN_HAIRPIN_LENGTH+1;
			int kj = k*n+j;
			for (; Utils.floatSmaller(cot[j*n+k] + es.elementScore(csr.XL_OPEN, kj) 
					+ scores_3[k+1][BEST], scores_3[j][PAIRED]); ++k, kj = k*n+j);
			if (Env.DEBUG_MODE){
				assert //Utils.floatEquals(cot[j][k] + es.elementScore(csr.XL_OPEN, kj) 
				//						+ scores_3[k+1][BEST], scores_3[j][PAIRED]);
				cot[j*n+k] + es.elementScore(csr.XL_OPEN, kj) 
				+ scores_3[k+1][BEST] == scores_3[j][PAIRED];
			}
			tracebackCot(j, k);
			traceback3(csr.XI_BASE_MID, csr.XI_LENGTH_MID, k+1);
		}
	}

	@SuppressWarnings("static-access")
	private void tracebackCot(int i, int j) {
		fold[i] = j;
		fold[j] = i;
		int ij = i*n+j;

		// handling stems (done first, since that should be the case for most base-pairs):
		if (//Utils.floatEquals(cot[i][j],
				cot[ij] ==
					cot[ij+n-1] + ilLengthScores[0][0] + 
					es.elementScore(csr.IL_CLOSE[0][0], ij) +
					es.elementScore(csr.IL_OPEN[0][0], (j-1)*n+(i+1))){
			tracebackCot(i+1, j-1);
			return;
		}

		// handling hairpins:
		int hpTypeIx = j-i-1;
		if (hpTypeIx > csr.HP_MAX_IX) hpTypeIx = csr.HP_MAX_IX;
		if (cot[ij] == //Utils.floatEquals(cot[i][j],  
				es.elementScore(csr.HP_CLOSE[hpTypeIx], ij) + 
				hpLengthScores[j-i-1] +
				hpBaseAccumScores[j-1]-hpBaseAccumScores[i]){
			return; // no more edges to reconstruct.
		}

		// handling multiloops:
		if (cot[ij] == /*Utils.floatEquals(cot[i][j],*/ ml2[ij+n-1] + 
				es.elementScore(csr.ML_CLOSE, ij)){
			tracebackMl(i+1, j-1, ml2);
			return;
		}

		// handling ils:

		// long ils:
		int maxLengthSum = j-i-3-csr.MIN_HAIRPIN_LENGTH;
		int longSide;
		for (int shortLength = 0; shortLength <= csr.IL_MAX_IX; ++shortLength){
			if (maxLengthSum-shortLength-csr.IL_LONG[shortLength] < 0) continue;
			for (int shortSide = LEFT; shortSide <= RIGHT; ++shortSide){
				longSide = 1-shortSide;
				int shortShift = shortSide*(csr.IL_MAX_IX+1);
				int longShift = longSide*(csr.IL_MAX_IX+1);
				ilLengthIxs[shortSide] = shortLength;
				ilLengthIxs[longSide] = csr.IL_MEDIUM[shortLength]; 
				if (cot[ij] == /*Utils.floatEquals(cot[i][j], */ il[(shortLength+shortShift)*nSqr+ij] + 
						es.elementScore(csr.IL_CLOSE[ilLengthIxs[LEFT]][ilLengthIxs[RIGHT]], ij)){ 
					// it is possible to trace-back a long il.
					if (shortLength == csr.IL_MAX_IX){
						while (il[(shortLength+shortShift)*nSqr+i*n+j] == //Utils.floatEquals(il[shortLength][shortSide][i][j], 
								il[(shortLength+shortShift)*nSqr+i*n+j+n-1]+
								ilBaseScores[(shortLength+shortShift)*n+i+1]+
								ilBaseScores[(shortLength+longShift)*n+j-1]){
							++i;
							--j;
						}
					}						

					ilIxs[LEFT] = i;
					ilIxs[RIGHT] = j;
					do {
						i = ilIxs[LEFT];
						j = ilIxs[RIGHT];
						assert j-i-3-csr.MIN_HAIRPIN_LENGTH-shortLength-csr.IL_LONG[shortLength] >= 0;
						ilIxs[longSide] += step[longSide];
					} while (/*Utils.floatEquals(*/il[(shortLength+shortShift)*nSqr+i*n+j] == 
							il[(shortLength+shortShift)*nSqr+ilIxs[LEFT]*n+ilIxs[RIGHT]]+
							ilBaseScores[(shortLength+csr.IL_MAX_IX+1)*n+ilIxs[longSide]]);
					ilIxs[longSide] -= step[longSide]; // taking back the last step.
					// computing the opening pair (p,q):

					int longLength = csr.IL_LONG[shortLength];
					if (longLength == shortLength && shortSide == RIGHT){
						++longLength;
					}

					if (Env.DEBUG_MODE){
						int[] openIxs = {0,0};
						openIxs[shortSide] = ilIxs[shortSide] + step[shortSide]*(shortLength+1); 
						openIxs[longSide] = ilIxs[longSide] + step[longSide]*(longLength+1); 
						int[] lengthIxs = {0, 0};
						lengthIxs[shortSide] = shortLength;
						lengthIxs[longSide] = csr.IL_MEDIUM[shortLength];

						int first = il[(shortLength+shortShift)*nSqr+ilIxs[LEFT]*n+ilIxs[RIGHT]];
						int second = cot[openIxs[0]*n+openIxs[1]] + 
						es.elementScore(csr.IL_OPEN[lengthIxs[1]][lengthIxs[0]], openIxs[1]*n+openIxs[0]) + 
						ilAccumBases[(shortLength+shortShift)*n+openIxs[0]-1] -
						ilAccumBases[(shortLength+shortShift)*n+ilIxs[0]] +
						ilAccumBases[(shortLength+longShift)*n+ilIxs[1]-1] -
						ilAccumBases[(shortLength+longShift)*n+openIxs[1]];
						assert /*Utils.floatEquals(*/first == second : 
									"first: " + first + ", second: " + second;
					}

					ilIxs[shortSide] += step[shortSide]*(shortLength+1); 
					ilIxs[longSide] += step[longSide]*(longLength+1);

					tracebackCot(ilIxs[LEFT], ilIxs[RIGHT]);
					return;
				}
			}
		}

		//handling short and medium length ils:
		int longLength, shortLength, longLengthIx;
		for (int totalLength = 1; totalLength <= csr.IL_MEDIUM_MAX_TOTAL_LENGTH ; ++totalLength){
			for (shortLength = 0; shortLength <= Math.min(totalLength/2, csr.IL_MAX_IX); ++shortLength){
				longLength = totalLength-shortLength;
				if (longLength >= csr.IL_LONG[shortLength]) continue;

				longLengthIx = longLength;
				if (longLength >= csr.IL_MEDIUM[shortLength]){
					longLengthIx = csr.IL_MEDIUM[shortLength];
				}

				for (int shortSide = LEFT; shortSide <= RIGHT; ++shortSide){
					longSide = 1 - shortSide;
					int shortShift = shortSide*(csr.IL_MAX_IX+1);
					int longShift = longSide*(csr.IL_MAX_IX+1);

					ilLengths[shortSide] = shortLength;
					ilLengths[longSide] = longLength;
					ilLengthIxs[shortSide] = shortLength;
					ilLengthIxs[longSide] = longLengthIx;
					ilIxs[LEFT] = i + ilLengths[LEFT] + 1;
					ilIxs[RIGHT] = j - ilLengths[RIGHT] - 1;

					if (cot[i*n+j] == cot[ilIxs[LEFT]*n+ilIxs[RIGHT]] +
							ilLengthScores[shortLength][longLength] +
							ilAccumBases[(shortLength+shortShift)*n+ilIxs[LEFT]-1]-
							ilAccumBases[(shortLength+shortShift)*n+i] +
							ilAccumBases[(shortLength+longShift)*n+j-1]-
							ilAccumBases[(shortLength+longShift)*n+ilIxs[RIGHT]] +
							es.elementScore(csr.IL_CLOSE[ilLengthIxs[LEFT]][ilLengthIxs[RIGHT]],ij)+
							es.elementScore(csr.IL_OPEN[ilLengthIxs[RIGHT]][ilLengthIxs[LEFT]], 
									ilIxs[RIGHT]*n+ilIxs[LEFT])){
						tracebackCot(ilIxs[LEFT], ilIxs[RIGHT]);
						return;
					}
				}
			}
		}
		assert false;
	}

	private void tracebackMl(int i, int j, int[] mlScores) {
		while (i <= j && mlScores[i*n+j] == mlScores[i*n+n+j] + mlBaseScores[i]){// Utils.floatEquals(mlScores[i][j], mlScores[i+1][j] + mlBaseScores[i])){
			++i;
		}
		while (i <= j && /*Utils.floatEquals(*/ mlScores[i*n+j] == mlScores[i*n+j-1] + mlBaseScores[j]){
			--j;
		}
		if (i>j) return;
		for (int k : rowOctIxs[i]){
			if (k >= j) break;
			if (/*Utils.floatEquals(*/mlScores[i*n+j] == cot[i*n+k] + 
					es.elementScore(csr.ML_OPEN, k*n+i) + ml1[k*n+n+j]){
				tracebackCot(i, k);
				tracebackMl(k+1, j, ml1);
				return;
			}
		}
		// if arrived here, [i...j] is an OCT
		if (Env.DEBUG_MODE){
			assert /*Utils.floatEquals(*/mlScores[i*n+j] == cot[i*n+j] + 
					es.elementScore(csr.ML_OPEN, j*n+i);
		}
		tracebackCot(i, j);
		return;
		//		assert false;
	}

	/*	private static void tracebackLongBlg(int blgSide, int step,
			short openType, float[][] blgScores) {

		float currScore;
		do {
			currScore = blgScores[ilIxs[LEFT]][ilIxs[RIGHT]];
			ilIxs[blgSide] += step;
		} while (Utils.floatEquals(currScore, blgScores[ilIxs[LEFT]][ilIxs[RIGHT]] + 
				blgBaseScores[ilIxs[blgSide]]));


		while (Utils.floatSmaller(cot[ilIxs[LEFT+1]][ilIxs[RIGHT-1]] + 
				es.elementScore(openType, ilIxs[RIGHT-1], ilIxs[LEFT+1]), currScore)) {
			currScore -= blgBaseScores[ilIxs[blgSide]];
			ilIxs[blgSide] += step;
		} 

		tracebackCot(ilIxs[LEFT+1], ilIxs[RIGHT-1]);
	}
	 */	
	/*
	private static void tracebackMediumShortBlg(int blgSide, int step,
			short openType, float[][] blgScores) {

		float currScore;
		do {
			currScore = blgScores[ilIxs[LEFT]][ilIxs[RIGHT]];
			ilIxs[blgSide] += step;
		} while (Utils.floatEquals(currScore, blgScores[ilIxs[LEFT]][ilIxs[RIGHT]] + 
				blgBaseScores[ilIxs[blgSide]]));


		while (Utils.floatSmaller(cot[ilIxs[LEFT+1]][ilIxs[RIGHT-1]] + 
				es.elementScore(openType, ilIxs[RIGHT-1], ilIxs[LEFT+1]), currScore)) {
			currScore -= blgBaseScores[ilIxs[blgSide]];
			ilIxs[blgSide] += step;
		} 

		tracebackCot(ilIxs[LEFT+1], ilIxs[RIGHT-1]);
	}
	 */
	/*
	@SuppressWarnings("static-access")
	private static void tracebackLongIl(int longSide, int step,
			float[][] ilScores) {

		while (Utils.floatEquals(ilScores[ilIxs[LEFT]][ilIxs[RIGHT]], ilScores[ilIxs[LEFT]+1][ilIxs[RIGHT]-1] + 
				ilBaseScores[ilIxs[LEFT]+1] + ilBaseScores[ilIxs[RIGHT]-1])) {
			++ilIxs[LEFT];
			--ilIxs[RIGHT];
		}

		float currScore;
		do {
			currScore = ilScores[ilIxs[LEFT]][ilIxs[RIGHT]];
			ilIxs[longSide] += step;
		} while (Utils.floatEquals(currScore, ilScores[ilIxs[LEFT]][ilIxs[RIGHT]] + 
				ilBaseScores[ilIxs[longSide]] + 
				linearAsymScore));

		ilIxs[longSide] -= step;

		int shortLength = 0;
		for (; csr.IL_MEDIUM[shortLength+1] >= shortLength+1; ++shortLength){
			++ilIxs[LEFT];
			--ilIxs[RIGHT];
			currScore -= ilBaseScores[ilIxs[LEFT]];
			currScore -= ilBaseScores[ilIxs[RIGHT]];
		}

		int longLength = shortLength;
		for (; longLength < csr.IL_MEDIUM[shortLength]; ++longLength){
			currScore -= linearAsymScore;
			ilIxs[longSide] += step;
			currScore -= ilBaseScores[ilIxs[longSide]];
		}

		while (Utils.floatSmaller(cot[ilIxs[LEFT+1]][ilIxs[RIGHT-1]] + 
				es.elementScore(csr.ML_OPEN, ilIxs[RIGHT-1], ilIxs[LEFT+1]), currScore)){
			ilIxs[1-longSide] += step;
			--shortLength;
			currScore += ilBaseScores[ilIxs[1-longSide]];
			currScore -= linearAsymScore;
			for (; longLength < csr.IL_MEDIUM[shortLength]; ++longLength){
				currScore -= linearAsymScore;
				ilIxs[longSide] += step;
				currScore -= ilBaseScores[ilIxs[longSide]];
			}
		}
		tracebackCot(ilIxs[LEFT+1], ilIxs[RIGHT-1]);
	}
	 */

}
