/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.rnaFolding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;



/**
 * This class defines the structural elements which are considered by the
 * Folder class.</p> 
 * </p>
 * Base-pairs elements:</p>
 * --------------------</p>
 * </p>
 * Base-pair elements are oriented and are considered with respect to the
 * loop they terminate. Each base-pair (i,j) induces two "loop termination"
 * elements: one for the loop it "closes" (including i+1 and j-1), and one for
 * the loop it "opens" (including i-1 and j+1). In addition, there are specific
 * base-pair elements for short hairpin, internal, and bulge loops (each short 
 * loop length induces its unique element). </p>
 * 
 * Loop types: hairpin (HP), stem, internal loop (IL), left bulge (LB), right
 * bulge (RB), multi-loop (ML), external loop (XL).</p>
 * 
 * </p>
 * Unpaired base elements:</p>
 * -----------------------</p>
 * </p>
 * Every unpaired base induces an element which depends in the loop within it
 * is enclosed. These elements allow linear score expressions with respect to
 * loop lengths.</p>
 * 
 * Base types: HP_BASE, IL_BASE, ML_BASE, BLG_BASE, XI_BASE_5 (a base in an 
 * external interval at the 5` end of the string), XI_BASE_3 (the same, at the
 * 3` end of the string), and XI_BASE_MID (the same, but neither at the 5` nor
 * 3` end).</p>
 * 
 * </p>
 * Length elements:</p>
 * ----------------</p>
 * </p>
 * Loops and external intervals induce length elements. The score of such elements
 * is usually a correction to the linear score which is obtained by the unpaired
 * bases. For bulges and internal loops, length elements are only induced by medium
 * length loops, where for longer loops the score is linear with respect to the length
 * (for multi-loop, the score is always linear).
 * </p>
 * Length element types: HP_LENGTH, IL_LENGTH (the summation of lengths of both unpaired 
 * regions of the loop), IL_ASYM (the absolute difference between the lengths of the two
 * unpaired regions), BLG_LENGTH, XI_LENGTH_5 (XI = external interval), XI_LENGTH_3, 
 * XI_LENGTH_MID.
 * 
 */
public class StructureRecognizerImpl implements StructureRecognizer {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3466539402528122226L;
	private int numTypes;
	private Map<Integer, String> typeNames; 

	//************************************* unpaired base elements *************************************

	public final int FIRST_BASE_TYPE;
	public final int LAST_BASE_TYPE;
	public final int BASE_TYPES_NUM;

	public final int HP_BASE;
	public final int[][] IL_BASE;
	public final int ML_BASE;
	public final int XI_BASE_5;
	public final int XI_BASE_3;
	public final int XI_BASE_MID;

	//************************************* loop termination elements *************************************


	//	calculating maximum length of right il side, given the left side length.                  [0,1,2,3,4,5,6,7,8]
	public final int[] IL_MEDIUM; // = new short[IL_MEDIUM_MAX_LONG_SIDE + 2]; //{0,7,5,4,3,2,1,1,0};
	public final int[] IL_LONG; //  = new short[IL_MEDIUM_MAX_LONG_SIDE + 2]; //{0,3,2,1,0,0,0,0,0};
	public final int IL_MEDIUM_MAX_TOTAL_LENGTH;


	public final int FIRST_LOOP_TERMINATION_TYPE;
	public final int LAST_LOOP_TERMINATION_TYPE;
	public final int LOOP_TERMINATION_TYPES_NUM;

	public final int[]   HP_CLOSE;
	public final int[][] IL_CLOSE; 
	public final int[][] IL_OPEN; 
	public final int 	   ML_CLOSE;
	public final int	   ML_OPEN;
	public final int 	   XL_OPEN;

	public final int		HP_MAX_IX;
	public final int		IL_SHORT_MAX_IX;
	public final int		IL_MAX_IX;

	//************************************* length elements *************************************

	public final int FIRST_LENGTH_TYPE;
	public final int LAST_LENGTH_TYPE;
	public final int LENGTH_TYPES_NUM;

	public final int   HP_LENGTH;
	public final int[] IL_MEDIUM_LENGTH;
	public final int   XI_LENGTH_5;
	public final int   XI_LENGTH_3;
	public final int   XI_LENGTH_MID;




	public StructureRecognizerImpl(
			int hpShortMaxLength,
			int[] ilLong,
			int[] ilMedium
	){

		HP_MAX_IX = hpShortMaxLength+1;

		IL_MAX_IX = Math.max(ilMedium.length, ilLong.length); // if the length of the shorter side is at least this number, this is a long-long il.
		IL_SHORT_MAX_IX = ilMedium.length-1; // if the length of the shorter side is bigger than this number, this is a non-short il.
		IL_LONG   = Arrays.copyOf(ilLong,   IL_MAX_IX+1); 
		IL_MEDIUM = Arrays.copyOf(ilMedium, IL_MAX_IX+1);

		// asserting input validity:
		for (int i = 0; i <= IL_MAX_IX; ++i){
			IL_MEDIUM[i] = Math.max(i, IL_MEDIUM[i]);
			IL_LONG[i] = Math.max(IL_MEDIUM[i], IL_LONG[i]);
		}


		typeNames = new HashMap<Integer, String>();
		numTypes = 0;

		// unpaired base elements:
		FIRST_BASE_TYPE = numTypes;

		HP_BASE 	= addType("HP_BASE");
		ML_BASE 	= addType("ML_BASE");
		XI_BASE_5 	= addType("XI_BASE_5");
		XI_BASE_3 	= addType("XI_BASE_3");
		XI_BASE_MID	= addType("XI_BASE_MID");

		IL_BASE = new int[IL_MAX_IX+1][];
		for (int i=0; i<IL_MAX_IX; ++i){
			IL_BASE[i] = addTypes("IL_"+i, "BASE", 0, -1, "SHORT", "LONG");
		}
		IL_BASE[IL_MAX_IX] = addTypes("IL_LONG", "BASE", 0, -1, "SHORT", "LONG");

		LAST_BASE_TYPE = numTypes-1;
		BASE_TYPES_NUM = LAST_BASE_TYPE - FIRST_BASE_TYPE +1;

		// loop termination elements:
		FIRST_LOOP_TERMINATION_TYPE = numTypes;

		HP_CLOSE = addTypes("HP","CLOSE", MIN_HAIRPIN_LENGTH, hpShortMaxLength, "LONG"); 
		IL_CLOSE = addIlTypes("CLOSE"); 
		IL_OPEN  = addIlTypes("OPEN");
		ML_CLOSE = addType("ML_CLOSE");
		ML_OPEN  = addType("ML_OPEN");
		XL_OPEN	 = addType("XL_OPEN");

		int maxTotalLength = 0;
		for (int shortLength = 0; shortLength<= IL_MAX_IX; ++shortLength){
			maxTotalLength = Math.max(maxTotalLength, shortLength+IL_LONG[shortLength]-1);
		}
		IL_MEDIUM_MAX_TOTAL_LENGTH = maxTotalLength;
		LAST_LOOP_TERMINATION_TYPE = numTypes-1;
		LOOP_TERMINATION_TYPES_NUM = LAST_LOOP_TERMINATION_TYPE - FIRST_LOOP_TERMINATION_TYPE + 1;	

		// length elements:
		FIRST_LENGTH_TYPE = numTypes;

		HP_LENGTH 			= addType("HP_LENGTH");
		IL_MEDIUM_LENGTH	= addTypes("IL", "MEDIUM_LENGTH", 0, IL_MAX_IX);
		XI_LENGTH_5 		= addType("XI_LENGTH_5");
		XI_LENGTH_3 		= addType("XI_LENGTH_3");
		XI_LENGTH_MID		= addType("XI_LENGTH_MID");

		LAST_LENGTH_TYPE = numTypes-1;
		LENGTH_TYPES_NUM = LAST_LENGTH_TYPE - FIRST_LENGTH_TYPE + 1;	
	}


	public StructureRecognizerImpl(){
		this(
				DEFAULT_HP_SHORT_MAX_LENGTH,
				DEFAULT_IL_LONG,
				DEFAULT_IL_MEDIUM
		);
	}

	private static final int DEFAULT_HP_SHORT_MAX_LENGTH = 4;
	private static final int[] DEFAULT_IL_LONG = {11, 8, 6, 5};
	private static final int[] DEFAULT_IL_MEDIUM = {5, 4, 3};
	private static final int INVALID_TYPE = -1;

	public int numTypes() {return numTypes;}


	//************************************* Auxiliary methods *************************************

	private int addType(String typeName) {
		typeNames.put((int)numTypes, typeName);
		++numTypes;
		return numTypes-1;
	}

	private int[] addTypes(String prefix, String suffix,
			int minLength, int maxLength, String... additionalTypes) {
		int typesNum = maxLength-minLength+1+additionalTypes.length;
		int[] types = new int[typesNum+minLength];
		Arrays.fill(types, 0, minLength, INVALID_TYPE);
		for (int i=minLength; i<=maxLength; ++i){
			types[i] = addType(prefix + "_" + i + "_" + suffix);
		}
		for (int i=0; i<additionalTypes.length; ++i){
			types[i+maxLength+1] = addType(prefix + "_" + additionalTypes[i] + "_" + suffix);
		}
		return types;
	}

	private int[][] addIlTypes(String suffix) {

		int maxSideLength = 0;
		for (int i = 0; i <= IL_MAX_IX; ++i){
			maxSideLength = Math.max(maxSideLength, IL_MEDIUM[i]);
		}

		int[][] ilTypes = new int[maxSideLength+1][];

		int[] maxRightLengths = new int[maxSideLength+1];
		Arrays.fill(maxRightLengths, -1);


		for (int leftLength=0; leftLength<=IL_MAX_IX; ++leftLength){
			maxRightLengths[leftLength] = IL_MEDIUM[leftLength]; 
			maxRightLengths[IL_MEDIUM[leftLength]] = leftLength; 
		}


		for (int leftLength=maxSideLength; leftLength>=0; --leftLength){
			if (maxRightLengths[leftLength] == -1){
				maxRightLengths[leftLength] = maxRightLengths[leftLength+1];
			}

			ilTypes[leftLength] = new int[maxRightLengths[leftLength]+1];
			Arrays.fill(ilTypes[leftLength], -1);

		}

		for (int shortLength=0; shortLength < IL_MAX_IX; ++shortLength){
			for (int longLength = shortLength; longLength < IL_MEDIUM[shortLength]; ++longLength){
				ilTypes[shortLength][longLength] = addType("IL" + "_" + shortLength + 
						"_" + longLength + "_" + suffix);
				if (shortLength < longLength){
					ilTypes[longLength][shortLength] = addType("IL" + "_" + longLength + 
							"_" + shortLength + "_" + suffix);
				}
			}
			ilTypes[shortLength][IL_MEDIUM[shortLength]] = addType("IL" + "_" + shortLength + 
					"_LONG_" + suffix);
			ilTypes[IL_MEDIUM[shortLength]][shortLength] = addType("IL_LONG_" 
					+ shortLength + "_" + suffix);
		}
		ilTypes[IL_MAX_IX][IL_MEDIUM[IL_MAX_IX]] = addType("IL_LONG_LONG_" + suffix);
		return ilTypes;
	}


	public Collection<int[]> getCombinedElements(int[] fold) {
		int n = fold.length;

		//		TODO: use pooling instead of new int[]

		Collection<int[]> elements = new ArrayList<int[]>(n);


		int i,ij,p,q,shortSide, longSide;
		int branchNum = 0;
		int length, lengthIx;
		int[] ilLengths = {0,0};
		int[] ilLengthIxs = {0,0};

		Stack<Integer> branchNumStack = new Stack<Integer>();
		branchNumStack.push(branchNum);

		// Resolving (partial) element set:
		for (int j=0; j<n; ++j){
			if (fold[j] != UNPAIRED_BASE){
				// index j is an end-point of an edge
				if (fold[j] > j){ // opening a new edge
					branchNumStack.push(branchNum);
					branchNum = 0;
				}

				else{ // closing an edge
					i = fold[j];
					ij = i*n+j;
					if (branchNum == 0) { // hairpin
						for (int k = i+1; k<j; ++k){
							addElement(elements, HP_BASE, k);
						}
						length = j-i-1;
						addElement(elements, HP_LENGTH, length);
						lengthIx = Math.min(length, HP_MAX_IX);
						addElement(elements, HP_CLOSE[lengthIx], ij);
					}

					else if (branchNum == 1){ //il
						for (p=i+1; fold[p] == UNPAIRED_BASE; ++p);
						for (q=j-1; fold[q] == UNPAIRED_BASE; --q);
						assert fold[p] == q;
						ilLengths[0] = p-i-1;
						ilLengths[1] = j-q-1;
						if (ilLengths[1] < ilLengths[0]) shortSide = 1;
						else shortSide = 0;

						longSide = 1-shortSide;

						ilLengthIxs[shortSide] = Math.min(ilLengths[shortSide], IL_MAX_IX);
						ilLengthIxs[longSide] = Math.min(ilLengths[longSide], IL_MEDIUM[ilLengthIxs[shortSide]]);
						if (ilLengths[longSide] < IL_LONG[ilLengthIxs[shortSide]]){
							addElement(elements, IL_MEDIUM_LENGTH[ilLengths[shortSide]], ilLengths[longSide]);
						}

						for (p=i+1; fold[p] == UNPAIRED_BASE; ++p){
							addElement(elements, IL_BASE[ilLengthIxs[shortSide]][shortSide], p);
						}
						for (q=j-1; fold[q] == UNPAIRED_BASE; --q){
							addElement(elements, IL_BASE[ilLengthIxs[shortSide]][longSide], q);
						}
						addElement(elements, IL_OPEN[ilLengthIxs[1]][ilLengthIxs[0]], q*n+p);
						addElement(elements, IL_CLOSE[ilLengthIxs[0]][ilLengthIxs[1]], ij);

					}

					else {// branchNum >= 2 => multiloop
						for (int k= i+1; k<j; ++k) {
							if (fold[k] == UNPAIRED_BASE){
								addElement(elements, ML_BASE, k);
							}
							else{
								// there is a branch starting at index k
								addElement(elements, ML_OPEN, fold[k]*n+k);
								k = fold[k]; // jumping to the end of the branch.
							}
						}
						addElement(elements, ML_CLOSE, ij);
					}

					branchNum = branchNumStack.pop();
					++branchNum;
				}
			}
		}

		assert branchNumStack.size() == 1 : "Ilegal folding!";

		// Resolving external element types:

		for (i=n-1; i>=0 && fold[i] == UNPAIRED_BASE; --i){
			addElement(elements, XI_BASE_3, i);
		}
		int seqStructureEnd = i;
		if (seqStructureEnd < n-1) addElement(elements, XI_LENGTH_3, n-1-seqStructureEnd);

		for (i=0; i<seqStructureEnd && fold[i] == UNPAIRED_BASE; ++i){
			addElement(elements, XI_BASE_5, i);
		}
		if (i>0) addElement(elements, XI_LENGTH_5, i);


		if (i<n){
			int midIntervalStart = i;
			for (; i<seqStructureEnd; ++i){
				if (fold[i] == UNPAIRED_BASE){
					addElement(elements, XI_BASE_MID, i);
				}
				else {
					// there is a external edge starting at index i
					if (midIntervalStart < i ){
						addElement(elements, XI_LENGTH_MID, i-midIntervalStart);
					}
					addElement(elements, XL_OPEN, fold[i]*n+i);
					i = fold[i]; // jumping to the end of the branch.
					midIntervalStart = i+1;
				}
			}
		}
		return elements;
	}

	private void addElement(Collection<int[]> elements, int elementType, int val) {
		//		addElement(elements, elementType, 0, val1);
		elements.add(new int[]{elementType, val});
	}

	//	private void addElement(Collection<int[]> elements, int... elementArgs) {
	//		elements.add(elementArgs);
	//	}

	public boolean isBaseType(int type) {
		return (type >= FIRST_BASE_TYPE &&
				type <= LAST_BASE_TYPE);
	}

	public boolean isBasepairType(int type) {
		return (type >= FIRST_LOOP_TERMINATION_TYPE &&
				type <= LAST_LOOP_TERMINATION_TYPE);
	}

	public boolean isLengthType(int type) {
		return (type >= FIRST_LENGTH_TYPE &&
				type <= LAST_LENGTH_TYPE);
	}

	public int getFirstBaseType() {
		return FIRST_BASE_TYPE;
	}

	public int getLastBaseType() {
		return LAST_BASE_TYPE;
	}

	public int getFirstBasepairType() {
		return FIRST_LOOP_TERMINATION_TYPE;
	}

	public int getLastBasepairType() {
		return LAST_LOOP_TERMINATION_TYPE;
	}

	public int getFirstLengthType() {
		return FIRST_LENGTH_TYPE;
	}

	public int getLastLengthType() {
		return LAST_LENGTH_TYPE;
	}

	public String getElementName(int elementType) {
		return typeNames.get(elementType);
	}

	@Override
	public void sortElements(List<int[]> elements) {
		Collections.sort(elements, new Comparator<int[]>() {

			@Override
			public int compare(int[] o1, int[] o2) {
				if (isLengthType(o1[0]) || isLengthType(o2[0])){
					if (!isLengthType(o1[0])) return -1;
					else if (!isLengthType(o2[0])) return 1;
					else if (o1[0] != o2[0]) return o1[0] - o2[0];
					else return o1[1] - o2[1];
				}
				else return o1[1] - o2[1];
			}
		});
	}


	public String elementToString(int[] element) {
		String str = "[" + typeNames.get(element[0]);
		for (int i=1; i<element.length; ++i){
			str += ", " + element[i];
		}
		return str + "]";
	}

	public String elementsString(int[] fold) {
		String res = "";
		Collection<int[]> elements = getCombinedElements(fold);
		sortElements((List<int[]>) elements);

		for (int[] element : elements) {
			res += elementToString(element)+"\n";
		}

		return res;
	}

}