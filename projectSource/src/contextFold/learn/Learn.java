/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.learn;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.io.PrintWriter;

import contextFold.common.Env;
import contextFold.common.Utils;
import contextFold.features.AbstractFeatureVector;
import contextFold.features.DenseFeatureVector;
import contextFold.features.FeatureManager;
import contextFold.features.SparseFeatureVector;
import contextFold.rna.RNA;
import contextFold.rnaFolding.Folder;
import contextFold.scoring.FoldEvaluator;
import contextFold.scoring.LossCalculator;
import contextFold.scoring.StructreElementsScorer;


/**
 * Implements the machine-learning algorithm for inferring feature weight
 * parameters. The learning algorithm is based on the algorithm which is described
 * in Sections 8 and 9 of the paper:<br />
 * 
 *  <a href="http://portal.acm.org/citation.cfm?id=1248566">
 *  Koby Crammer, Ofer Dekel, Joseph Keshet, Shai Shalev-Shwartz, and Yoram Singer.
 *  Online Passive-Aggressive Algorithms. Journal of Machine Learning Research 7 (2006) 551�585</a>.
 *  
 */
public class Learn  {

	static final int UPDATE_MIRA = 0;
	static final int UPDATE_PERCEP = 1;
	static final int UPDATE_CS_PA = 2; // cost-sensitive PA

	//	static final int UPDATE=UPDATE_MIRA;  
	static int UPDATE = UPDATE_CS_PA;
	private static final String modelFileExtension = ".model";

	//	DenseFeatureVector w;
	public PrintWriter output = null;
	FoldEvaluator foldEvaluator = null;

	int kbest;
	private final Folder folder;
	private FeatureManager featureManager;
	private LossCalculator lossCalculator;

	private AbstractFeatureVector distFV;

	public Learn(FeatureManager featureManager, FoldEvaluator foldEvaluator, int kbest, LossCalculator lossCalculator) {
		this.featureManager = featureManager;
		this.folder = new Folder(featureManager);
		this.foldEvaluator = foldEvaluator;
		this.lossCalculator = lossCalculator;
		this.kbest = kbest;
		//		featureManager.getFeatureWeights() = new DenseFeatureVector(featureManager); //new DenseFeatureVector(fvs.dimensionality());
		distFV = new SparseFeatureVector();
	}


	protected int[][] getFolds(StructreElementsScorer Gs) {
		int[][] folds;
		List<int[]> kbestFolds = new ArrayList<int[]>();
		folder.computeDPTables(Gs, kbestFolds, this.kbest);
		folds = new int[kbestFolds.size()][];
		for (int i=0;i<folds.length;i++) {
			folds[i] = kbestFolds.get(i); 
		}
		return folds;
	} 

	public float doIter(int iter, List<RNA> rnas, 
			int averageEvery, int niters) {
		int counter = 0;
		int step = 5;
		int stepCounter = 1;
		int nextPrint = (int) (stepCounter*step*rnas.size()/100.0);

		float averageLoss = 0;
		for (RNA rna : rnas) {
			//			if (iter >= niters) break;
			++iter;
			boolean skeep = false;
			++counter;
			if (counter >= nextPrint){
				System.out.print(stepCounter*step +"%, ");
				++stepCounter;
				nextPrint = (int) (stepCounter*step*rnas.size()/100.0);
			}


			// calc current weights
			StructreElementsScorer elementScorer = featureManager.getElementScorer(rna);

			// fold using weights
			int[][] foldeds = this.getFolds(elementScorer);
			int[] guessFold = null;

			float[] lam_dists = new float[foldeds.length];
			float[] losses    = new float[foldeds.length];
			float[] margins   = new float[foldeds.length]; 
			float[] lts       = new float[foldeds.length]; 
			float loss;
			float lam_dist;

			float goldScore, predictionScore;

//			featureManager.getFeatureWeights().tick(); // count the number of sequences this
//			// feature vector was used on.  These 
//			// numbers later play a role in the update.
//			// must be called on every sequence.

			for (int i=0;i<foldeds.length;i++) {
				guessFold = foldeds[i];
				rna.addFold(guessFold);
				loss = this.lossCalculator.loss(guessFold, rna.goldFold()); // @@ loss is actually "cost"
				averageLoss += loss;
				if (Env.ECHO) System.out.print("loss:" + loss);

				//				goldScore = ((FoldEvaluatorImpl) this.foldEvaluator).scoreFoldDebug(contextFold.rna,contextFold.rna.fold);
				goldScore = this.foldEvaluator.scoreFold(rna,rna.goldFold());
				if (Float.isInfinite(goldScore) || Float.isNaN(goldScore)){
					System.err.println("Ilegal gold folding. Skeeping this sequence.");
					skeep = true;
					break;
				}

				//				predictionScore = ((FoldEvaluatorImpl) this.foldEvaluator).scoreFoldDebug(contextFold.rna,folded);
				predictionScore = this.foldEvaluator.scoreFold(rna,guessFold);
				if (Float.isInfinite(predictionScore) || Float.isNaN(predictionScore)){
					System.err.println("Ilegal prediction folding - we have a bug!!! Skeeping this sequence.");
					skeep = true;
					continue;
				}

				lam_dist = goldScore-predictionScore;
				if (Env.ECHO) System.out.println(", lam dist " + i +":" + lam_dist);

				losses[i]    = loss;
				lam_dists[i] = lam_dist;
				margins[i]   = loss - lam_dist;
				lts[i]       = (float) (-lam_dist + Math.sqrt(loss));

				if (Env.ECHO) System.out.println("--" + goldScore + 
						"/" + predictionScore);
				if (Env.ECHO) {
					System.out.println(rna);
					//					System.out.println(FoldPrinter.format(foldeds[0]));
					System.out.print("Before update. ");
					System.out.print("Gold score: " + goldScore);
					System.out.println(", prediction score: " + predictionScore);
				}
			}

			// do update

			featureManager.getFeatureWeights().tick(); // count the number of sequences this
			// feature vector was used on.  These 
			// numbers later play a role in the update.
			// must be called on every sequence.


			if (skeep) continue;

			DenseFeatureVector oldWeights = null;
			if (Env.DEBUG_MODE){
				oldWeights = new DenseFeatureVector(featureManager.getFeatureWeights());
			}

			if (UPDATE == UPDATE_CS_PA) {
				this.costSensitivePAUpdate(rna, guessFold, rna.goldFold(), lts[0]);
			}
			else { // MIRA or PERCEP
				this.updateKbest(foldeds, rna.goldFold(), margins, iter, counter, rnas.size(), rna);
			}

			if (Env.DEBUG_MODE) {
				
				goldScore = this.foldEvaluator.scoreFold(rna,rna.goldFold());
				predictionScore = this.foldEvaluator.scoreFold(rna,guessFold);

				if (goldScore < predictionScore){//Utils.floatSmaller(goldScore, predictionScore)){
					System.err.println("Update didn't cause gold folding to get better score than the prediction folding." +
							"\nUpdated scores: gold: "+ goldScore + ", prediction: "+ predictionScore +
							", diff: " + (predictionScore-goldScore));
					printPairwiseInfo(rna, guessFold, rna.goldFold(), featureManager.getFeatureWeights(), oldWeights);
				}
			}


			//foldeds = this.getFolds(contextFold.rna,G);
			if (Env.ECHO) {
				System.out.print("After update. ");
				System.out.print("Gold score: " + this.foldEvaluator.scoreFold(rna,rna.goldFold()));
				System.out.println(", prediction score: " + this.foldEvaluator.scoreFold(rna, foldeds[0]));
				System.out.println();
			}
			//			System.out.println("done");

			//			System.out.println(iter + ")============================");
			if (averageEvery > 0 && iter % averageEvery == 0){
				featureManager.getFeatureWeights().average();
			}
		}
		averageLoss /= counter;
		return averageLoss;
	}



	// MIRA/PA kBest update
	public void updateKbest(int[][] guessFolds, int[] goldFold, float[] margins, int iter, int seqnum, int seqlen, RNA rna) {

		DenseFeatureVector[] distFVs = new DenseFeatureVector[guessFolds.length];

		for (int i=0;i<distFVs.length;i++) { distFVs[i] = featureManager.getFeatureWeights().newEmpty(); }
		for (int n=0;n<distFVs.length;n++) { 
			this.foldEvaluator.differenceFeatureVector(guessFolds[n],goldFold, distFVs[n], rna);
		}

		float alpha[];
		if (UPDATE == UPDATE_MIRA) {
			alpha = Hildreth.hildreth(distFVs,  margins ); //!! @@TODO
		} else { // assuming UPDATE == UPDATE_PERCEP
			alpha = new float[margins.length]; //!! @@TODO
			for (int n=0;n<alpha.length;n++) { alpha[n]=1;}
		}

		System.out.println("mar:" + margins[0]);
		System.out.println("alpha:" + alpha[0]);

		for (int n=0;n<alpha.length;n++) {
			featureManager.getFeatureWeights().update(distFVs[n], alpha[n]);
		}
	}
	// cost sensitive passive-aggressive update
	public void costSensitivePAUpdate(RNA rna, int[] guessFold, int[] goldFold, float lt) {
		if (contextFold.common.Env.VERBOSE) printPairwiseInfo(rna, guessFold, goldFold, featureManager.getFeatureWeights(), null); //DEBUG!!!!

//		DenseFeatureVector distFV = featureManager.getFeatureWeights().newEmpty();
		distFV.clear();
		

		this.foldEvaluator.differenceFeatureVector(guessFold,goldFold, distFV, rna);

		float norm = distFV.norm();
		if (norm == 0) return; // no update - the contextFold.features are identical.
		float tau = lt / norm;

		featureManager.getFeatureWeights().update(distFV, tau);

		if (contextFold.common.Env.VERBOSE) printPairwiseInfo(rna, guessFold, goldFold, featureManager.getFeatureWeights(), null); //DEBUG!!!!
	}


	private void printPairwiseInfo(RNA rna, int[] guessFold, int[] goldFold, AbstractFeatureVector weights, DenseFeatureVector oldWeights) {
		System.err.println("Gold fold contextFold.features:");
		printInfo(rna, goldFold, weights, oldWeights); 
		System.err.println("\nPredicted fold contextFold.features:");
		printInfo(rna, guessFold, weights, oldWeights);
		SparseFeatureVector diffVector = (SparseFeatureVector) foldEvaluator.differenceFeatureVector(guessFold,goldFold, rna);
		System.err.println("\nFeatures diff vector (new weights):\n" + diffVector.toString(weights, true));
		if (oldWeights != null){
			System.err.println("\nFeatures diff vector (old weights):\n" + diffVector.toString(oldWeights, true));
		}
	}

	private void printInfo(RNA rna, int[] fold, AbstractFeatureVector weights, DenseFeatureVector oldWeights) {
		Collection<int[]> elements = featureManager.sr.getCombinedElements(fold);
		featureManager.sr.sortElements((List<int[]>) elements);

		SparseFeatureVector featureVector = new  SparseFeatureVector();

		float accumeScore = 0;

		for (int[] element : elements) {
			AbstractFeatureVector elementFeaturs = featureManager.getElementFeaturs(rna, element[0], element[1]);
			float score = elementFeaturs.sum();
			if (score != 0) {
				accumeScore += weights.dot(elementFeaturs);
				System.err.print(featureManager.sr.elementToString(element) + ", " + ((SparseFeatureVector) elementFeaturs).toString(weights));
				System.err.println(", accumulated score: " + accumeScore); // accumulated
				//				System.out.println(Arrays.toString(element) + ", score: " + score1);
			}
			featureVector.add(elementFeaturs);
		}
		System.err.println("Complete feature vector: " + featureVector.toString(weights));

		if (oldWeights != null){
			System.err.println("\nOld weights:");
			featureVector.clear();

			accumeScore = 0;

			for (int[] element : elements) {
				AbstractFeatureVector elementFeaturs = featureManager.getElementFeaturs(rna, element[0], element[1]);
				float score = elementFeaturs.sum();
				if (score != 0) {
					accumeScore += oldWeights.dot(elementFeaturs);
					System.err.print(featureManager.sr.elementToString(element) + ", " + ((SparseFeatureVector) elementFeaturs).toString(oldWeights));
					System.err.println(", accumulated score: " + accumeScore); // accumulated
					//					System.out.println(Arrays.toString(element) + ", score: " + score1);
				}
				featureVector.add(elementFeaturs);
			}
			System.err.println("Complete feature vector: " + featureVector.toString(oldWeights));
		}
	}

	public void learn(int iters, List<RNA> rnas, String weightsOutputFile, 
			int averageEvery, boolean shuffle, float requiredImproveFactor,
			int maxNoImprovements, boolean keepIterationWeights) {

		float timeFactor = (float) (1d/(Math.pow(10, 9)*60));
		int maxLength = 0;
		for (RNA rna: rnas){
			maxLength = Math.max(maxLength, rna.length);
		}
		folder.initializeTables(maxLength);

		int processedSequences = 0;
		int iter = 0;
		float bestLoss = Float.POSITIVE_INFINITY;
		float currLoss;
		int notImporvedCount = 0;

		while (iter < iters) {
			++iter;
			folder.concatanations = 0;
			folder.naivSplits = 0;
			System.out.println("Iteration number: "+iter);
			if (shuffle) Collections.shuffle(rnas, Env.random);
			long startTime = System.nanoTime();
			currLoss = doIter(processedSequences, rnas, averageEvery, iters);
			processedSequences += rnas.size();

			if (currLoss/bestLoss <= 1-requiredImproveFactor) notImporvedCount = 0;
			else ++notImporvedCount;

			System.out.print("\nLoss estimation: "+ currLoss + ", loss improvement: " + bestLoss/currLoss);
			//			System.out.print("\nShort OCT length: "+ contextFold.rnaFolding.ComplexFolder.SHORT_OCT_LENGTH);
			System.out.println(", Iter time (min): " + (System.nanoTime() - startTime)*timeFactor +
					",\nconcatenations: " + folder.concatanations + ", naive splits: " + folder.naivSplits + 
					", sparsification factor: " + ((float) folder.concatanations) / folder.naivSplits);
			System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
			if (keepIterationWeights && weightsOutputFile!=null) {
				Utils.save(featureManager, weightsOutputFile+ "Iter" +iter+ modelFileExtension);
			}

			if (notImporvedCount >= maxNoImprovements || currLoss == 0) break;
			bestLoss = Math.min(bestLoss, currLoss);
		}

		featureManager.getFeatureWeights().average();
		if (weightsOutputFile!=null) {
			featureManager.compress(); //for smaller file size;
			Utils.save(featureManager, weightsOutputFile+ modelFileExtension);
		}
	}
}

