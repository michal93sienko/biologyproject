/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.learn;

import contextFold.common.Dotable;

/**
 * adapted from mstparser
 */
public class Hildreth {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static float[] hildreth(Dotable[] a, float[] b) {

		int i;
		int max_iter = 10000;
		float eps = (float) 0.00000001;
		float zero = (float) 0.000000000001;

		float[] alpha = new float[b.length];

		float[] F = new float[b.length];
		float[] kkt = new float[b.length];
		float max_kkt = Float.NEGATIVE_INFINITY;

		int K = a.length;

		float[][] A = new float[K][K];
		boolean[] is_computed = new boolean[K];
		for(i = 0; i < K; i++) {
			A[i][i] = a[i].dot(a[i]);
			is_computed[i] = false;
		}

		int max_kkt_i = -1;


		for(i = 0; i < F.length; i++) {
			F[i] = b[i];
			kkt[i] = F[i];
			if(kkt[i] > max_kkt) { max_kkt = kkt[i]; max_kkt_i = i; }
		}

		int iter = 0;
		float diff_alpha;
		float try_alpha;
		float add_alpha;

		while(max_kkt >= eps && iter < max_iter) {

			diff_alpha = (float) (A[max_kkt_i][max_kkt_i] <= zero ? 0.0 : F[max_kkt_i]/A[max_kkt_i][max_kkt_i]);
			try_alpha = alpha[max_kkt_i] + diff_alpha;
			add_alpha = (float) 0.0;

			if(try_alpha < 0.0)
				add_alpha = (float) (-1.0 * alpha[max_kkt_i]);
			else
				add_alpha = diff_alpha;

			alpha[max_kkt_i] = alpha[max_kkt_i] + add_alpha;

			if (!is_computed[max_kkt_i]) {
				for(i = 0; i < K; i++) {
					A[i][max_kkt_i] = a[i].dot(a[max_kkt_i]); // for version 1
					is_computed[max_kkt_i] = true;
				}
			}

			for(i = 0; i < F.length; i++) {
				F[i] -= add_alpha * A[i][max_kkt_i];
				kkt[i] = F[i];
				if(alpha[i] > zero)
					kkt[i] = Math.abs(F[i]);
			}

			max_kkt = Float.NEGATIVE_INFINITY;
			max_kkt_i = -1;
			for(i = 0; i < F.length; i++)
				if(kkt[i] > max_kkt) { max_kkt = kkt[i]; max_kkt_i = i; }

			iter++;
		}

		return alpha;
	}
}
