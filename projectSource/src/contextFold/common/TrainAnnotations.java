/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */


package contextFold.common;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import contextFold.app.Train;
import contextFold.rna.OneLineReader;
import contextFold.rna.RNA;
import contextFold.rna.RNAListReader;
import contextFold.rna.RNAStrandReader;
import contextFold.rna.RNAWriter;

/**
 * 
 */
public class TrainAnnotations {

	private static final Random random = new Random(0);

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {

		String progName = TrainAnnotations.class.getSimpleName();
		String description = "A script for creating specialized parameters with respect to a specific annotation.";
		CmdArgumentsHandler argHandler = new CmdArgumentsHandler(progName, description, true);

		String annotationArg = argHandler.addRequiredArg("annot", "Annotation for which specialized parameters will be created. Spaces in the annotation string must be replaced with \'_\' (underline) characters");
		String dataInputFileArg = argHandler.addRequiredArg("train", "Path for the training data file");
		String outputPathArg = argHandler.addArg("out", "trained/", "Path for the trained model output directory"); 
		String splitPropArg = argHandler.addArg("prop", "80", "The proportion of annotated samples on which the algorithm trains, among the total occurrences of the anotation in the data (must be an integer between 0 and 100). If a negative value is given, then the exact amount of samples is used");
		String formatArg = argHandler.addEnumArg("format", "Input file format", 1, "rnaStrand", "oneLine");


		List<String> trainArgs = argHandler.processArgs(args);

		String rnaFilePath = argHandler.stringValue(dataInputFileArg);

		String annotation = argHandler.stringValue(annotationArg);
		annotation = annotation.replace('_', ' ');
		String[] annotationParts = annotation.split("=");
		String annotationType, annotationVal;
		if (annotationParts.length == 1){
			annotationType = "TYPE"; 
			annotationVal = annotationParts[0];
		}
		else{
			annotationType = annotationParts[0];
			annotationVal = annotationParts[1];
		}

		List<RNA> annotated = new ArrayList<RNA>();
		List<RNA> notAnnotated = new ArrayList<RNA>();


		String format = argHandler.stringValue(formatArg);
		RNAListReader lr;
		if ("oneLine".equals(format)){
			lr = new OneLineReader();
		}
		else{
			lr = new RNAStrandReader();
		}
		File rnaFile = new File(rnaFilePath);
		List<RNA> rnas = lr.readList(rnaFilePath); 

		for (RNA rna: rnas){
			if (annotationVal.equals(rna.annotationsMap.get(annotationType))){
				annotated.add(rna);
			}
			else notAnnotated.add(rna);
		}

		List<RNA> annotatedTest = new ArrayList<RNA>(annotated);
		Collections.shuffle(annotatedTest, random);
		int splitProp = argHandler.intValue(splitPropArg);
		int annotatedInTrain;
		if (splitProp >= 0){
			annotatedInTrain = annotatedTest.size()*splitProp/100;
		}
		else{
			annotatedInTrain = -splitProp;
		}
		

		List<RNA> annotatedTrain = new ArrayList<RNA>(annotatedTest.subList(0, annotatedInTrain));
		annotatedTest.removeAll(annotatedTrain);

		List<RNA> train = new ArrayList<RNA>(notAnnotated);
		train.addAll(annotatedTrain);

		String outputPath = argHandler.stringValue(outputPathArg);
		String outputPrefix = outputPath+rnaFile.getName()+"_"+
		(annotationType +"_"+annotationVal).replace(' ', '_') + "_"+ annotatedInTrain;
		if (splitProp >=0) outputPrefix += "P";

		RNAWriter rfw = new RNAWriter(new File(outputPrefix + ".train1"));
		rfw.writeAll(train);
		rfw = new RNAWriter(new File(outputPrefix + ".train2"));
		rfw.writeAll(annotatedTrain);
		if (annotatedTest.size()>0){
			rfw = new RNAWriter(new File(outputPrefix + ".test"));
			rfw.writeAll(annotatedTest);
		}

		trainArgs.add("train:"+outputPrefix + ".train1");
		trainArgs.add("out:"+outputPrefix);

		Train.main(CmdArgumentsHandler.toStrArr(trainArgs));

		CmdArgumentsHandler.removeArg("train",trainArgs);
		CmdArgumentsHandler.removeArg("f",trainArgs);
		CmdArgumentsHandler.removeArg("out",trainArgs);
		trainArgs.add("train:"+outputPrefix + ".train2");
		trainArgs.add("f:"+outputPrefix+".model");
		trainArgs.add("out:"+outputPrefix+"Specialized");

		Train.main(CmdArgumentsHandler.toStrArr(trainArgs));

	}

}
