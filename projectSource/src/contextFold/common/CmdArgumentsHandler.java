/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

public class CmdArgumentsHandler {

	private Map<String,String> argValues;
	private Map<String,Boolean> argRequired;
	private SortedMap<String,String> argMan;
	private SortedMap<String,Pair<String,StringModifier>> argBined;
	private HashMap<String, String[]> argEnums;
	private HashMap<String, Integer> argEnumsDefaults;
	boolean makeMan;
	private String description;
	private String progName;

	public static final String noValue = "$$$NO_VALUE!$$$";


	public CmdArgumentsHandler(String progName, String description, boolean makeMan){
		argValues = new HashMap<String, String>();
		argRequired = new HashMap<String, Boolean>();
		argMan = new TreeMap<String, String>();
		argBined = new TreeMap<String, Pair<String,StringModifier>>();
		argEnums = new HashMap<String, String[]>();
		argEnumsDefaults = new HashMap<String, Integer>();
		this.progName = progName;
		this.description = description;
		this.makeMan = makeMan;
	}


	public CmdArgumentsHandler(String progName, String description) {
		this(progName, description, false);
	}

	public CmdArgumentsHandler() {
		this("", "", false);
	}

	public String addArg(String arg, String defaultVal, String man) {
		return addArg(arg, defaultVal, man, false);
	}

	public String addArg(String arg, String man) {
		return addArg(arg, noValue, man, false);
	}

	public String addRequiredArg(String arg, String man) {
		return addArg(arg, noValue, man, true);
	}

	public String addBinedArg(String arg, String binedArg, StringModifier stringModifier,
			String man) {
		argBined.put(arg, new Pair<String, StringModifier>(binedArg, stringModifier));
		return addArg(arg, noValue, man);
	}

	public String addEnumArg(String arg, String man, int defaultIx, String... choices){
		assert (defaultIx >= -1 && defaultIx < choices.length) : "Default choice index must " +
		"be between 0 and the number of possible values -1.";
		argEnums.put(arg, choices);
		argEnumsDefaults.put(arg, defaultIx);
		return addArg(arg, choices[defaultIx], man);
	}

	private String addArg(String arg, String defaultVal, String man, boolean isRequired) {
		assert !argValues.keySet().contains(arg) : "Can't use the same argumet twice: " + arg;
		argRequired.put(arg, isRequired);
		argValues.put(arg, defaultVal);
		argMan.put(arg, man);
		return arg;
	}

	public List<String> processArgs(String[] args){
		if (makeMan){
			addArg("man", "false", "Prints this manual.");
			if (args.length == 0){
				argValues.put("man", "true");
			}
		}
		
		List<String> unknown = new ArrayList<String>();
		
		String argName, argVal;
		for (String arg : args) {
			if (arg.indexOf(':') > 0){
				String[] split = arg.split(":",2);
				argName = split[0];
				argVal = split[1];
			}
			else {
				argName = arg;
				argVal = "true";
			}

			String[] choices = argEnums.get(argName);

			if (!argValues.keySet().contains(argName)){
				unknown.add(arg);
			}
			else if(choices != null && !contain(choices, argVal)){
				System.err.println("Unknown value: " + argVal + "for argument:" + argName + ". Ignoring...");
			}
			else{
				argValues.put(argName, argVal);
			}
		}
		
		for (Entry<String, Pair<String, StringModifier>> entry : argBined.entrySet()){
			if (argValues.get(entry.getKey()) == noValue){
				String binedArg = entry.getValue().first;
				String bindArgValue = argValues.get(binedArg);
				argValues.put(entry.getKey(), entry.getValue().second.modify(bindArgValue));
			}
		}

		if (args.length == 1 && args[0].equals("man")){
			System.out.println(getMan());
			System.exit(0);
		}
		
		if (!hasAllRequired()){
			System.err.println("The following required arguments are missing:\n" +
					missingRequired());
			System.err.println("Use the 'man' argument to read the manual for this operation");
			System.exit(0);
		}
		
		return unknown;
	}


	/**
	 * @param choices
	 * @param argVal
	 * @return
	 */
	private boolean contain(String[] choices, String argVal) {
		for (String choice: choices){
			if (choice.equals(argVal)) return true;
		}
		return false;
	}


	public int intValue(String arg){
		return Integer.parseInt(argValues.get(arg));
	}

	public long longValue(String arg) {
		return Long.parseLong(argValues.get(arg));
	}

	public boolean boolValue(String arg){
		return Boolean.parseBoolean(argValues.get(arg));
	}

	public float floatValue(String arg) {
		return Float.parseFloat(argValues.get(arg));
	}

	public String stringValue(String arg){
		return argValues.get(arg);
	}

	@Override
	public String toString(){
		String str = "";
		for (Entry<String, String> entry : argValues.entrySet()){
			if (entry.getValue() != noValue){
				str += "\n\t"+entry.getKey()+"\t"+entry.getValue();
			}
		}
		return str;
	}

	public String getMan(){
		StringBuilder str = new StringBuilder();
		str.append(description);
		str.append("\nUsage: \"").append(progName);
		str.append(" <arg1 name>:X1 <arg2 name>:X2 ...\", where Xi is "); 
		str.append("the value to assign for the ith argument.");
		for (Entry<String, String> entry : argMan.entrySet()){
			String argName = entry.getKey();
			str.append("\n\t").append(argName).append("\t");
			str.append(entry.getValue()).append(" (");
			if (!argRequired.get(argName)){
				String[] values = argEnums.get(argName);
				if (values != null){
					str.append("Allowed values: ");
					int defaultVal = argEnumsDefaults.get(argName);
					for (int i=0; i<values.length; ++i){
						if (i > 0) str.append(", ");
						str.append(values[i]);
						if (i == defaultVal) str.append(" (default)");
					}
				}
				else {
					String defaultVal = argValues.get(argName);
					if (noValue.equals(defaultVal)) defaultVal = "not specified";
					str.append("Default: ").append(defaultVal);
				}
			}
			else str.append("Required");
			str.append(").");
		}
		return str.toString();
	}

	public boolean hasAllRequired(){
		for (Entry<String, Boolean> entry : argRequired.entrySet()){
			if (entry.getValue().equals(true) &&
					argValues.get(entry.getKey()) == noValue){
				return false;
			}
		}
		return true;
	}

	public String missingRequired(){
		StringBuilder sb = new StringBuilder();
		for (Entry<String, Boolean> entry : argRequired.entrySet()){
			if (entry.getValue().equals(true) &&
					argValues.get(entry.getKey()) == noValue){
				sb.append(entry.getKey()).append(" ");
			}
		}
		return sb.toString().trim();
	}

	public boolean hasValue(String arg){
		String value = argValues.get(arg);
		return value != null && !value.equals(noValue);
	}


	/**
	 * @param argList
	 * @return
	 */
	public static String[] toStrArr(List<String> argList) {
		return argList.toArray(new String[argList.size()]);
	}


	/**
	 * @param string
	 * @param trainArgs
	 */
	public static void removeArg(String arg, List<String> argList) {
		for (String element:argList){
			String[] split = element.split(":",2);
			if (arg.equals(split[0])){
				argList.remove(element);
				return;
			}
		}
	}

}
