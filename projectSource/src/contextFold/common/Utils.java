/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.common;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class Utils {

	private static final float epsilon = (float) Math.pow(10, -7);

	public static boolean floatEquals(float d1, float d2) {
		if (d2==0){
			return d1<epsilon && d1>-epsilon; 
		}
		return (d1 / d2 <1 + epsilon) && (d1 / d2 > 1-epsilon);

		//		float dif = d1-d2;
//		return dif < epsilon && dif > -epsilon;
	}

	public static boolean floatSmaller(float d1, float d2) {
		return d1+epsilon < d2;
	}

	public static void save(Serializable toSave, String outputFile) {
		try {
			FileOutputStream fout = new FileOutputStream(outputFile);
			GZIPOutputStream zipedOut = new GZIPOutputStream(fout);
			ObjectOutputStream oos = new ObjectOutputStream(zipedOut);
//			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(toSave);
			zipedOut.finish();
			oos.close();
			fout.close();
		}
		catch (Exception e) { e.printStackTrace(); }
	}

	@SuppressWarnings("unchecked")
	public static <T> T load(Class<T> objClass, String inputFile) throws IOException, ClassNotFoundException {
		FileInputStream fin = new FileInputStream(inputFile);
		GZIPInputStream zipedIn = new GZIPInputStream(fin);
		ObjectInputStream ois = new ObjectInputStream(zipedIn);
		T loaded = (T) ois.readObject();
		ois.close();
		return loaded;
	}

}
