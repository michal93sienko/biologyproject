/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.common;

import java.util.Random;


public class Env {
	public static final int MAX_SERVER_SEQUENCE_LENGTH = 1000;
	public static boolean DEBUG_MODE = false;
	public static boolean VERBOSE = false;
	public static boolean ECHO = false;

	public static int MIN_HAIRPIN_LENGTH = 3;
	public static Random random = new Random();

	public static String defaultModel = "trained/StHighCoHigh.model";

	public static int maxSuperContextCardinality = (int) Math.pow(5, 9);
	public static int maxSuperContextCardinalityTrain = (int) Math.pow(5, 5);

	public static int MAX_INT_FEATURE_WEIGHT = 10000;
}
