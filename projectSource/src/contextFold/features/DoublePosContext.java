/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.features;

import java.util.ArrayList;
import java.util.List;

public class DoublePosContext extends ContextFeatureExtractor {

	private static final long serialVersionUID = 3047027062261976385L;

	protected ContextFeatureExtractor firstPosContext;
	protected ContextFeatureExtractor secondPosContext;

	public DoublePosContext(ContextFeatureExtractor firstPosContext, 
			ContextFeatureExtractor secondPosContext){
		super(firstPosContext.length+secondPosContext.length, 
				firstPosContext.cardinality*secondPosContext.cardinality);

		this.firstPosContext = firstPosContext;
		this.secondPosContext = secondPosContext;

		reversedContext = new DoublePosContext(this);
	}

	private DoublePosContext(DoublePosContext reversedContext) {
		super(reversedContext.length, reversedContext.cardinality);

		firstPosContext = reversedContext.secondPosContext.getReversedContext();
		secondPosContext = reversedContext.firstPosContext.getReversedContext();

		this.reversedContext = reversedContext;
	}

	protected void preprocess(byte[] seq) {
		int n = seq.length;
		if (seqFeatures == null || seqFeatures.length<n*n){
			seqFeatures = new int[n*n];
		}
		firstPosContext.setSequence(seq);
		secondPosContext.setSequence(seq);
		int firstCardinality = firstPosContext.cardinality;
		int[] firstPosSeqFeatures = firstPosContext.seqFeatures;
		int[] secondPosSeqFeatures = secondPosContext.seqFeatures;
		int firstPosIx1Feature, secondPosIx1Feature;


		for (int i = 0; i<n; ++i){
			firstPosIx1Feature = firstPosSeqFeatures[i];
			secondPosIx1Feature = firstCardinality*secondPosSeqFeatures[i];

			for (int j = i; j<n; ++j){
				seqFeatures[i*n+j] = firstPosIx1Feature + 
				firstCardinality*secondPosSeqFeatures[j];


				seqFeatures[j*n+i] = firstPosSeqFeatures[j] + 
				secondPosIx1Feature;

			}
		}
	}



	public int getFeature(byte[] seq, int val) {
		int i = val/seq.length;
		int j = val%seq.length;
		return (firstPosContext.getFeature(seq, i) + 
				firstPosContext.cardinality*secondPosContext.getFeature(seq, j));
	}

	@SuppressWarnings("unchecked")
	@Override
	public Integer[][] getFeaturesMap(ContextFeatureExtractor specificContext) {
		if (length > specificContext.length ||
				cardinality > specificContext.cardinality) return null;
		if (!(specificContext instanceof DoublePosContext)) return null;
		DoublePosContext other = (DoublePosContext) specificContext;

		Integer[][] firstFeatureMap = firstPosContext.getFeaturesMap(other.firstPosContext);
		if (firstFeatureMap == null) return null;
		Integer[][] secondFeatureMap = secondPosContext.getFeaturesMap(other.secondPosContext);
		if (secondFeatureMap == null) return null;

		List<Integer>[] featuresAuxMap = new List[cardinality];
		for (int thisFirstFeature=0; thisFirstFeature<firstPosContext.cardinality; ++thisFirstFeature){
			for (int thisSecondFeature=0; thisSecondFeature<secondPosContext.cardinality; ++thisSecondFeature){
				int thisFeature = thisFirstFeature + firstPosContext.cardinality*thisSecondFeature;
				featuresAuxMap[thisFeature] = new ArrayList<Integer>();

				for (int otherFirstFeature : firstFeatureMap[thisFirstFeature]){
					for (int otherSecondFeature : secondFeatureMap[thisSecondFeature]){
						featuresAuxMap[thisFeature].add(otherFirstFeature
								+ other.firstPosContext.cardinality*otherSecondFeature);
					}
				}
			}
		}

		Integer[][] featuresMap = new Integer[cardinality][];
		for (int i=0; i<cardinality; ++i){
			featuresMap[i] = featuresAuxMap[i].toArray(new Integer[featuresAuxMap[i].size()]);
		}
		return featuresMap;
	}

	public String toString(){
		return "Pos1" + firstPosContext.toString() + "Pos2" + secondPosContext.toString();
	}

	@Override
	public int getEffectiveCardinality(){
		return firstPosContext.getEffectiveCardinality() * secondPosContext.getEffectiveCardinality();
	}

	@Override
	public String getFeatureName(int featureIx){
		return "i_"+firstPosContext.getFeatureName(featureIx%firstPosContext.cardinality) +
		"_j_"+secondPosContext.getFeatureName(featureIx/firstPosContext.cardinality);
	}

	public ContextFeatureExtractor getFirstPosContext() {
		return firstPosContext;
	}

	public ContextFeatureExtractor getSecondPosContext() {
		return secondPosContext;
	}

	/* (non-Javadoc)
	 * @see contextFold.features.ContextFeatureExtractor#getSuperContext(contextFold.features.ContextFeatureExtractor)
	 */
	@Override
	public ContextFeatureExtractor getSuperContext(ContextFeatureExtractor other) {
		DoublePosContext other1 = (DoublePosContext) other;
		return new DoublePosContext(
				firstPosContext.getSuperContext(other1.firstPosContext), 
				secondPosContext.getSuperContext(other1.secondPosContext));
	}


}