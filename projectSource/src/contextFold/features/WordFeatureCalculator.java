/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.features;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class WordFeatureCalculator {
	public static final byte[] FULL_ALPHABET = {0, 1, 2, 3, 4}; // A->0, C->1, G->2, U->3, OUT_OF_RANGE->4.
	public static final byte[] REDUCED_ALPHABET = {0, 1, 0, 1, 2}; // Purines (A, G)->0, Pyrimidines (C, U)->1.
	public static final byte[] TRIVIAL_ALPHABET = {0, 0, 0, 0, 1}; //A, C, G, U->0 (distinguished only from the OUT_OF_RANGE character).

	
	public static final byte[][] ALPHABETS = {
		FULL_ALPHABET,
		REDUCED_ALPHABET,
		TRIVIAL_ALPHABET
	};
	
	private static final String[][] ALPHABET_CHARS = {
		{"A", "C", "G", "U", "Out"},
		{"Pur", "Pyr", "Out"},
		{"In", "Out"},
	};

	
	public static final Map<Integer, Map<Integer, byte[]>> alphabetMaps = initializeAlphabetMaps();
	
	public static final int[] ALPHABET_SIZE = {5, 3, 2};

	public static final int FULL = 0;
	public static final int REDUCED = 1;
	public static final int TRIVIAL = 2;
	
	public static final byte OUT_OF_RANGE = (byte) (FULL_ALPHABET.length-1);

	public static int getFeature(byte[] word) {
		return getFeature(word, FULL);
	}

	private static Map<Integer, Map<Integer, byte[]>> initializeAlphabetMaps() {
		Map<Integer, Map<Integer, byte[]>> alphabetMaps = new HashMap<Integer, Map<Integer,byte[]>>();
		
		for (int i=0; i<ALPHABETS.length; ++i){
			alphabetMaps.put(i, new HashMap<Integer,byte[]>());
		}
		
		for (int i=0; i<ALPHABETS.length; ++i){
			for (int j = 0; j<ALPHABETS.length; ++j){
				alphabetMaps.get(i).put(j, alphabetMap(ALPHABETS[i], ALPHABETS[j]));
			}
		}
		
		return alphabetMaps;
	}

	private static byte[] alphabetMap(byte[] alphabet1, byte[] alphabet2) {
		if (Arrays.equals(alphabet1,alphabet2)) return FULL_ALPHABET;
		
		Map<Byte,Byte> byteMap = new HashMap<Byte, Byte>();
		int includingLast = 0;
		for (int i=0; i<alphabet1.length; ++i){
			if (includingLast < alphabet1[i]) includingLast = alphabet1[i];
			if (byteMap.get(alphabet1[i]) != null){
				if (alphabet2[i] != byteMap.get(alphabet1[i])) return null;
			}
			else byteMap.put(alphabet1[i], alphabet2[i]);
		}
		
		byte[] alphabetMap = new byte[includingLast+1];
		for (byte i=0; i<=includingLast; ++i) alphabetMap[i] = byteMap.get(i); 
		return alphabetMap;

		
	}

	public static int getFeature(byte[] word, int alphabetIx) {
		int feature = 0;
		for (int i=word.length-1; i>=0; --i){
			feature  = ALPHABET_SIZE[alphabetIx]*feature + word[i]; 
		}
		return feature;
	}

	//	protected byte getLetter(byte[] word, int i) {return word[i];} // overridden by the ReducedContextClass


//	public static byte[] getWord(short feature) {
//		return getWord(feature, FULL, null);
//	}

//	private static int getWordLength(short feature, int alphabetSize) {
//		return (int) Math.max(1, (Math.ceil(Math.log(feature+1)/Math.log(alphabetSize))));
//	}

	public static byte[] getWord(int feature, int alphabetIx, byte[] word) {
//		int length = getWordLength(feature, alphabetSize);
//		if (word == null){
//			word = new byte[length];
//		}
//		else assert word.length == length;

		for (int j=0; j<word.length; ++j){
			word[j] = (byte) (feature%ALPHABET_SIZE[alphabetIx]);
			feature = feature/ALPHABET_SIZE[alphabetIx];
		}
		return word;
	}

//	public static boolean includingAlphabet(byte[] isIncluding,
//			byte[] isIncluded) {
//		return alphabetMap(isIncluding, isIncluded) != null;
////		if (Arrays.equals(isIncluding, isIncluded)) return true;
////		assert isIncluding.length == isIncluded.length;
////		Map<Byte,Byte> byteMap = new HashMap<Byte, Byte>();
////		for (int i=0; i<isIncluding.length; ++i){
////			if (byteMap.get(isIncluding[i]) != null){
////				if (isIncluded[i] != isIncluding[i]) return false;
////			}
////			else byteMap.put(isIncluding[i], isIncluded[i]);
////		}
////		return true;
//	}
	
	public static byte[] alphabetMap(int alphabetIx,
			int alphabetIx2) {
		return alphabetMaps.get(alphabetIx).get(alphabetIx2);
	}

	public static String getWordStr(int featureIx, int alphabetIx, int wordLength) {
		byte[] word = getWord(featureIx, alphabetIx, new byte[wordLength]);
		String res = "";
		for (int i=0; i<wordLength; ++i){
			res += ALPHABET_CHARS[alphabetIx][word[i]];
		}
		return res;
	}

	/**
	 * @param alphabetIx
	 * @param other
	 * @return
	 */
	public static int superAlphabet(int alphabet1, int alphabet2) {
		return Math.min(alphabet1, alphabet2);
	}
}
