/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.features;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Context implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 852430395486899819L;

	private static int idCounter = 0;
	private static Map<Integer, Map<Integer, int[]>> reverseMaps = 
		new HashMap<Integer, Map<Integer,int[]>>();

	private int[] offsets;
	private byte alphabetSize;
	private byte[] alphabet;
	private int length;
	private int[] reverseArray;
	private byte OUT_OF_RANGE;
	public int cardinality;
	public final int id;

	public Context(int[] offsets, byte[] alphabet) {
		super();
		this.offsets = offsets;
		this.alphabet = alphabet;
		length = offsets.length;
		alphabetSize = 0;
		for (int letter:alphabet) alphabetSize = (byte) Math.max(alphabetSize, letter);
		alphabetSize += 1; // 1 for the 0-letter.
		OUT_OF_RANGE = (byte) (alphabetSize-1);
		cardinality = (int) Math.pow(alphabetSize, length);
		id = idCounter;
		++idCounter;
		reverseArray = getReverseArray(length, alphabetSize);
	}

	public int getLength() {return length;}
	public int getCardinality() {return cardinality;}
	public int getId() {return id;}
	public int[] getReverseArray() {return reverseArray;}
	public int[] getOffsets() {return offsets;}
	public int getAlphabetSize(){return alphabetSize;}

	public int getLocalFeature(byte[] seq, int ix){
		int localFeatureIx = 0;
		byte currLetter;
		for (int i=0; i<length; ++i){
			int currIx = ix + offsets[i];
			if (currIx < 0 || currIx >= seq.length) currLetter = OUT_OF_RANGE;
			else currLetter = alphabet[seq[currIx]];
			localFeatureIx = alphabetSize*localFeatureIx + currLetter; // adding relative context
		}
		return localFeatureIx;
	}

	public void getLocalFeatures(byte[] seq, int[] localFeatures){
		for (int ix = 0; ix<seq.length; ++ix){		
			int localFeatureIx = 0;
			byte currLetter;
			for (int i=0; i<length; ++i){
				int currIx = ix + offsets[i];
				if (currIx < 0 || currIx >= seq.length) currLetter = OUT_OF_RANGE;
				else currLetter = alphabet[seq[currIx]];
				localFeatureIx = alphabetSize*localFeatureIx + currLetter; // adding relative context
			}
			localFeatures[ix] = localFeatureIx; 
		}
	}

	public int[] getLocalFeatures(byte[] seq){
		int[] localFeatures = new int[seq.length];
		getLocalFeatures(seq, localFeatures);
		return localFeatures;
	}

	
	public int[] inclusionMap(Context other){
		if (length < other.length) return null;
		if (alphabetSize < other.alphabetSize) return null;
		int[] auxMap = new int[other.length];
		
		for (int i=0; i<other.length; ++i){
			boolean mapped = false;
			for (int j=0; j<length && !mapped; ++j){
				if (offsets[j] == other.offsets[i]){
					auxMap[i] = j;
					mapped = true;
				}
			}
			if (!mapped) return null;
		}
		
		int[] map = new int[cardinality];
		byte[] currContext = new byte[length];
		byte[] includedContext = new byte[other.length];
		makeMap(map, auxMap, currContext, 0, includedContext, other);
		return map;
	}

	private void makeMap(int[] map, int[] auxMap, byte[] currContext,
			int contextIx, byte[] includedContext, Context other) {
		if (contextIx == currContext.length){
			for (int i=0; i<auxMap.length; ++i){
				includedContext[i] = currContext[auxMap[i]];
			}
			map[getLocalFeature(currContext)] = other.getLocalFeature(includedContext);
		}
		else{
			for (byte i=0; i<alphabet.length; ++i){
				currContext[contextIx] = i;
				makeMap(map, auxMap, currContext, contextIx+1,
						includedContext, other);
			}
		}
	}

	public int getLocalFeature(byte[] context) {
		int localFeatureIx = 0;
		for (int i=0; i<context.length; ++i){
			localFeatureIx  = alphabetSize*localFeatureIx + alphabet[context[i]]; 
		}
		return localFeatureIx;
	}
	

	public static int getIdCounter() {return idCounter;}

	private static int[] getReverseArray(int length, int alphabetSize) {
		Map<Integer, int[]> reverseMap = reverseMaps.get(alphabetSize);
		if (reverseMap == null){
			reverseMap = new HashMap<Integer, int[]>();
			reverseMaps.put(alphabetSize, reverseMap);
		}
		int[] reverseArray = reverseMap.get(length);
		if (reverseArray == null){
			reverseArray = new int[(int) Math.pow(alphabetSize, length)];
			for (int i=0; i<reverseArray.length; ++i){
				int direct = i, reverse = 0;
				for (int j=0; j<length; ++j){
					int currLetter = direct%alphabetSize;
					reverse += currLetter*Math.pow(alphabetSize, length-j-1);
					direct /= alphabetSize;
				}
				reverseArray[i] = reverse;
			}
			reverseMap.put(length, reverseArray);
		}
		return reverseArray ;
	}

	public static Context[] makeContexts(int[][] offsets,
			byte[] alphabet) {
		Context[] contexts = new Context[offsets.length];
		for (int i=0; i<offsets.length; ++i) contexts[i] = new Context(offsets[i], alphabet);
		return contexts;
	}
	
	public String toString(){
		return "[alphabet size: " + alphabetSize + ", offsets: " + Arrays.toString(offsets) + "]";
	}

	/**
	 * @param offsets2
	 * @param offsets3
	 * @return
	 */
	public static int[] getSuperContext(int[] offsets1, int[] offsets2) {
		Set<Integer> allOffsets = new HashSet<Integer>();
		for (int offset : offsets1) allOffsets.add(offset);
		for (int offset : offsets2) allOffsets.add(offset);
		int[] superContext = new int[allOffsets.size()];
		int i = 0;
		for (int offset : allOffsets) {
			superContext[i] = offset;
			++i;
		}
		return superContext;
	}

}
