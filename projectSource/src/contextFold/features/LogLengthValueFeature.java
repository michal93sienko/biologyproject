/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.features;

import java.util.HashMap;
import java.util.Map;

public class LogLengthValueFeature implements ValueFeature{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6037397681940822811L;
	private Map<Integer, ElementValueCalculator> elementTypeToLengthCalculator;
	
	public LogLengthValueFeature(){
		elementTypeToLengthCalculator = new HashMap<Integer, ElementValueCalculator>();
	}
	
	public void bindTypeToLengthCalculator(int elementType, ElementValueCalculator LengthCalculator){
		elementTypeToLengthCalculator.put(elementType, LengthCalculator);
	}

	public float getValue(int[] element) {
		return (float) Math.log(elementTypeToLengthCalculator.get(element[0]).calcValue(element));
	}

}
