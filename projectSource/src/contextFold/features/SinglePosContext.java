/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.features;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import contextFold.common.Env;

public class SinglePosContext extends ContextFeatureExtractor{

	private static final long serialVersionUID = -5234559063914379876L;

	public static final SinglePosContext EMPTY_CONTEXT = new SinglePosContext(WordFeatureCalculator.TRIVIAL);

	protected int[] offsets;
	protected int alphabetIx;
	protected byte[] currWord;



	private int effectiveCardinality;

	private SinglePosContext(int length, int cardinality, int alphabet){
		super(length, cardinality);
		currWord = new byte[length];
		effectiveCardinality = -1;
		this.alphabetIx = alphabet;
	}


	public SinglePosContext(int alphabet, int... offsets) {
		this(offsets.length, (int) Math.pow(WordFeatureCalculator.ALPHABET_SIZE[alphabet],  offsets.length), alphabet);
		this.offsets = offsets;
		reversedContext = new SinglePosContext(this);
	}

	private SinglePosContext(SinglePosContext reversedContext) {
		this(reversedContext.length, reversedContext.cardinality, reversedContext.alphabetIx);
		offsets = new int[length];
		for (int i=0; i<length; ++i) offsets[i] = -reversedContext.offsets[i];
		this.reversedContext = reversedContext;
	}



	@SuppressWarnings("unchecked")
	public Integer[][] getFeaturesMap(ContextFeatureExtractor specificContext){
		if (!(specificContext instanceof SinglePosContext)) return null;
		if (length > specificContext.length ||
				cardinality > specificContext.cardinality) return null;

		SinglePosContext other = (SinglePosContext) specificContext;
		byte[] currAlphabetMap = WordFeatureCalculator.alphabetMap(other.alphabetIx, alphabetIx);
		if (currAlphabetMap == null) return null;


		int[] offsetMap = new int[length];
		for (int i=0; i<length; ++i){
			offsetMap[i] = -1;
			for (int j=0; j<other.length; ++j){
				if (offsets[i] == other.offsets[j]){
					offsetMap[i] = j;
					break;
				}
			}
			if (offsetMap[i] == -1) return null;
		}

		ArrayList<Integer>[] featuresAuxMap = new ArrayList[cardinality];
		for (int thisFeature=0; thisFeature<cardinality; ++thisFeature){
			featuresAuxMap[thisFeature] = new ArrayList<Integer>();
		}

		byte[] thisWord = new byte[length];
		byte[] otherWord = new byte[other.length];
		for (int otherFeature=0; otherFeature<other.cardinality; ++otherFeature){
			WordFeatureCalculator.getWord(otherFeature, other.alphabetIx, otherWord);
			for (int i=0; i<length; ++i) {
				thisWord[i] = currAlphabetMap[otherWord[offsetMap[i]]];
			}
			featuresAuxMap[WordFeatureCalculator.getFeature(thisWord, alphabetIx)].add(otherFeature);
		}

		Integer[][] featuresMap = new Integer[cardinality][];
		for (int i=0; i<cardinality; ++i){
			featuresMap[i] = featuresAuxMap[i].toArray(new Integer[featuresAuxMap[i].size()]);
		}
		return featuresMap;
	}


	public int[] getOffsets() {return offsets;}

	public void preprocess(byte[] seq){
		int n = seq.length;
		if (seqFeatures == null || seqFeatures.length<n){
			seqFeatures = new int[n];
		}

		for (int ix = 0; ix<seq.length; ++ix){		
			seqFeatures[ix] = getFeature(seq, ix);
			//TODO: consider automaton-based faster implementation.
		}
	}


	public int getFeature(byte[] seq, int val){
		for (int i=0; i<length; ++i){
			int currIx = val + offsets[i];
			if (currIx >= 0 && currIx < seq.length) {
				currWord[i] = seq[currIx];
			}

			else currWord[i] = WordFeatureCalculator.OUT_OF_RANGE; //out-of-range character.
		}
		return WordFeatureCalculator.getFeature(currWord, alphabetIx);
	}


	public String toString(){
		return Arrays.toString(offsets);
	}

	public static byte calcAlphabetSize(byte[] alphabetMap) {
		byte alphabetSize = 0;
		for (byte reducedLetter : alphabetMap){
			assert reducedLetter < WordFeatureCalculator.FULL_ALPHABET.length;
			alphabetSize = (byte) Math.max(reducedLetter, alphabetSize);
		}
		alphabetSize += 1; // for the 0 character.
		return alphabetSize;
	}

	@Override
	public int getEffectiveCardinality(){
		if (effectiveCardinality < 0){
			int alphabetSize = WordFeatureCalculator.ALPHABET_SIZE[alphabetIx];
			effectiveCardinality = 0;
			int[] sortedOffsets = Arrays.copyOf(offsets, length);
			Arrays.sort(sortedOffsets);
			for (int firstInRange = 0; firstInRange <= length ; ++firstInRange){
				if (firstInRange>0 && sortedOffsets[firstInRange-1] >= 0) break;
				for (int firtsOutRange = firstInRange; firtsOutRange <= length; ++firtsOutRange){
					if (firtsOutRange<length && sortedOffsets[firtsOutRange] <= 0) continue;
					effectiveCardinality += Math.pow(alphabetSize-1, firtsOutRange-firstInRange);
				}
			}
		}
		return effectiveCardinality;
	}

	@Override
	public String getFeatureName(int featureIx){
		return Arrays.toString(offsets).replaceAll(" ", "")+"_" + WordFeatureCalculator.getWordStr(featureIx, alphabetIx, offsets.length);
	}

	public SinglePosContext getSuperContext(SinglePosContext other) {
		byte[] currAlphabetMap = WordFeatureCalculator.alphabetMap(other.alphabetIx, alphabetIx);
		if (currAlphabetMap == null) {
			if (Env.DEBUG_MODE){
				assert WordFeatureCalculator.alphabetMap(alphabetIx, other.alphabetIx) != null;
			}
			return other.getSuperContext(this);
		}

		return new SinglePosContext(WordFeatureCalculator.superAlphabet(alphabetIx, other.alphabetIx),
				superOffsets(offsets, other.offsets));
	}

	public static int[] superOffsets(int[] offsets1, int[] offsets2){
		Set<Integer> allOffsets = new HashSet<Integer>();

		for (int i:offsets1) allOffsets.add(i);
		for (int i:offsets2) allOffsets.add(i);

		List<Integer> allOffsetList = new ArrayList<Integer>(allOffsets);
		Collections.sort(allOffsetList);

		int[] superOffsets = new int[allOffsetList.size()];
		for (int i=0; i<allOffsetList.size(); ++i) superOffsets[i] = allOffsetList.get(i);
		return superOffsets;
	}

	public static boolean contains(int[] offsets1, int[] offsets2){
		Set<Integer> offsets1Set = new HashSet<Integer>();

		for (int i:offsets1) offsets1Set.add(i);
		for (int i:offsets2) {
			if (!offsets1Set.contains(i)) return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see contextFold.features.ContextFeatureExtractor#getSuperContext(contextFold.features.ContextFeatureExtractor)
	 */
	@Override
	public ContextFeatureExtractor getSuperContext(ContextFeatureExtractor other) {
		SinglePosContext other1 = (SinglePosContext) other;
		int alphabet = WordFeatureCalculator.superAlphabet(alphabetIx, other1.alphabetIx);
		return new SinglePosContext(alphabet, Context.getSuperContext(offsets, other1.offsets));
	}


}
