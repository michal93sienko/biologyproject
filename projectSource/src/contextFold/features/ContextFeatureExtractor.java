/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.features;

import java.io.Serializable;

public abstract class ContextFeatureExtractor extends FeatureExtractor implements  Serializable{

	private static final long serialVersionUID = -7686018620402519501L;

	protected int length;
	protected ContextFeatureExtractor reversedContext;

	protected ContextFeatureExtractor(int length, int cardinality){
		super(cardinality);
		this.length = length;
	}

	public int getLength(){return length;}
	public ContextFeatureExtractor getReversedContext(){return reversedContext;}
	public abstract Integer[][] getFeaturesMap(ContextFeatureExtractor other);

	/**
	 * @param context
	 * @return
	 */
	public boolean contained(ContextFeatureExtractor context) {
		// TODO make a more reasonable implementation
		return getFeaturesMap(context) != null;
	}

	/**
	 * @param second
	 * @return
	 */
	public abstract ContextFeatureExtractor getSuperContext(
			ContextFeatureExtractor other);


}
