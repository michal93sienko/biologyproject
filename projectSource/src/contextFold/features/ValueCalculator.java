/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.features;

import java.io.Serializable;

public abstract class ValueCalculator implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6589412360512776664L;

	public static final ValueCalculator VALUE_1_CALCULATOR = new ValueCalculator() {
		/**
		 * 
		 */
		private static final long serialVersionUID = 7593864336927443223L;

		@Override
		public float getValue(int val) {
			return 1;
		}

		@Override
		public String getValueName(int featureIx) {
			return "";
		}
	}; 
	
	public static final ValueCalculator LOG_VALUE_CALCULATOR = new ValueCalculator() {
		/**
		 * 
		 */
		private static final long serialVersionUID = 5943205667451420574L;

		@Override
		public float getValue(int val) {
			if (val == 0) return 0;
			else return (float) Math.log(val);
		}

		@Override
		public String getValueName(int featureIx) {
			return ""+featureIx;
		}
	}; 
	
	public static final ValueCalculator SQRT_VALUE_CALCULATOR = new ValueCalculator() {
		/**
		 * 
		 */
		private static final long serialVersionUID = 4041854227089792551L;

		@Override
		public float getValue(int val) {
			return (float) Math.sqrt(val);
		}

		@Override
		public String getValueName(int featureIx) {
			return ""+featureIx;
		}
	}; 
	
	public abstract float getValue(int val);

	public abstract String getValueName(int featureIx);
}
