/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.features;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import contextFold.common.Env;
import contextFold.common.Utils;
import contextFold.models.FeatureSetterFactory;
import contextFold.rna.RNA;
import contextFold.rnaFolding.StructureRecognizer;
import contextFold.scoring.StructreElementsScorer;
import contextFold.scoring.StructreElementsScorerImpl;


/**
 * A parameterization object.
 */
public class FeatureManager implements Serializable{

	private static final long serialVersionUID = 527046066222200738L;

	private int featuresNum;

	private int[][] baseFeatures;
	private int[][] edgeFeatures;
	private int[][] lengthFeatures;
	private int[][][] seqFeatures;
	private int[][][] elementScores;



	/*	-------------------- Feature Templates -------------------------
	 * 	The feature and weight vectors are partitioned to sub-vectors that
	 * 	correspond to feature templates. Each template has a start position
	 * 	in the vector (maintained in the templateShifts filed), and a
	 * 	cardinality (maintained in the templateCardinalities filed). The
	 * 	vector-index of the f'th feature in the t'th template is given by
	 * 	templateShifts.get(t) + f.	
	 */

	/**
	 * Contains an array contexts that corresponds to this template.
	 * The array either contains a single context, or a context and
	 * its reverse context.
	 */
	private List<FeatureTemplate> featureTemplates;


	/*	-------------------- Base Contexts -------------------------
	 */

	/**
	 * All base contexts.
	 */
	private List<ContextFeatureExtractor> baseFeatureExtractors;

	/**
	 * A subset of representative baseContexts that covers all 
	 * base contexts.
	 */
	private List<ContextFeatureExtractor> repBaseFeatureExtractors; 

	/**
	 * All base-pair contexts.
	 */
	private List<ContextFeatureExtractor> edgeFeatureExtractors;

	/**
	 * A subset of representative basepairContexts that covers all 
	 * base-pair contexts.
	 */
	private List<ContextFeatureExtractor> repEdgeFeatureExtractors;



	private DenseFeatureVector weightVector;
	private int scoresUpdateTime;
	private int maxLengthVal;
	public StructureRecognizer sr;

	private List<FeatureTemplate>[] elementTypeToFeatureTemplates;
	private List<Boolean>[] elementTypeToTemplateReverseFlags;

	private HashMap<ContextFeatureExtractor,Integer[][]> featuresMap;
	private HashMap<ContextFeatureExtractor,Integer> representativeMap;
	private AbstractFeatureVector tempFeatureVector;

	private float scoreFactor; // translate the float weights to integers within a predefine range.  


	@SuppressWarnings("unchecked")
	public FeatureManager(StructureRecognizer sr) {
		super();

		maxLengthVal = 0;
		scoresUpdateTime = -1;
		this.sr = sr;

		featureTemplates = new ArrayList<FeatureTemplate>();
		featuresNum = 0;
		tempFeatureVector = new SparseFeatureVector();

		baseFeatureExtractors = new ArrayList<ContextFeatureExtractor>();
		edgeFeatureExtractors = new ArrayList<ContextFeatureExtractor>();

		elementTypeToFeatureTemplates = new List[sr.numTypes()];
		elementTypeToTemplateReverseFlags = new List[sr.numTypes()];

		for (int i=0; i<sr.numTypes(); ++i){
			elementTypeToFeatureTemplates[i] = new ArrayList<FeatureTemplate>();
			elementTypeToTemplateReverseFlags[i] = new ArrayList<Boolean>();
		}

	}

	/********************* Context adding 
	 * @param isReversed *************************************/


	public void addBaseContext(ContextFeatureExtractor context, Boolean isReversed){
		if (isReversed) context = context.getReversedContext();
		if (!baseFeatureExtractors.contains(context)){
			baseFeatureExtractors.add(context);
		}
	}

	public void addBasepairContext(ContextFeatureExtractor context, Boolean isReversed){
		if (isReversed) context = context.getReversedContext();
		if (!edgeFeatureExtractors.contains(context)){
			edgeFeatureExtractors.add(context);
		}
	}


	public void addAllBasepairContextUpToLength(int length) {
		for (ContextFeatureExtractor firstPosContext : baseFeatureExtractors){
			for (ContextFeatureExtractor secondPosContext : baseFeatureExtractors){
				if (firstPosContext.getLength() + 
						secondPosContext.getLength() <= length){
					DoublePosContext context = new DoublePosContext(
							(ContextFeatureExtractor) firstPosContext, 
							(ContextFeatureExtractor) secondPosContext);
					addBasepairContext(context, false);
				}
			}
		}
	}



	/********************* Feature-templates adding *************************************/

	private void addTemplate(FeatureTemplate template) {
		template.setFirstVectorIx(featuresNum);
		featuresNum += template.getCardinality();
		featureTemplates.add(template);
	}

	public FeatureTemplate addTemplate(String name, 
			FeatureExtractor featureExtractor){
		FeatureTemplate template = new FeatureTemplate(name, featureExtractor);
		addTemplate(template);
		return template;
	}

	public FeatureTemplate addTemplate(String name,
			ValueCalculator valueCalculator) {
		FeatureTemplate template = new FeatureTemplate(name, valueCalculator);
		addTemplate(template);
		return template;
	}

	private void addTemplates(Collection<FeatureTemplate> templates){
		for (FeatureTemplate template: templates){
			addTemplate(template);
		}
	}

	private Collection<FeatureTemplate> getTemplates(Collection<? extends FeatureExtractor> featureExtractors, 
			String templateSetName) {
		Collection<FeatureTemplate> templates = new ArrayList<FeatureTemplate>(featureExtractors.size());
		for (FeatureExtractor featureExtractor : featureExtractors){
			templates.add(new FeatureTemplate(templateSetName+
					featureExtractor.toString(), featureExtractor));
		}
		return templates;
	}

	public Collection<FeatureTemplate> addTemplates(String name, FeatureExtractor... featureExtractors) {
		Collection<FeatureTemplate> templates = new ArrayList<FeatureTemplate>();
		for (FeatureExtractor featureExtractor : featureExtractors){
			templates.add(new FeatureTemplate(name+featureExtractor.toString(), featureExtractor));
		}
		addTemplates(templates);
		return templates;
	}

	public Collection<FeatureTemplate> addBaseTemplates(String namePrefix) {
		Collection<FeatureTemplate> templates = getTemplates(baseFeatureExtractors, namePrefix);
		addTemplates(templates);
		return templates;
	}

	public Collection<FeatureTemplate> addBasepairTemplates(String namePrefix) {
		Collection<FeatureTemplate> templates = getTemplates(edgeFeatureExtractors, namePrefix);
		addTemplates(templates);
		return templates;
	}


	/********************* Feature-templates binding *************************************/

	public void bindElementTypeToFeatureTemplates(int elementType,  
			Collection<FeatureTemplate>... templatesCollections){
		bindElementTypeToFeatureTemplates(elementType, false, templatesCollections);
	}

	public void bindElementTypeToFeatureTemplates(int elementType, Boolean isReversed, 
			Collection<FeatureTemplate>... templatesCollections){
		for (Collection<FeatureTemplate> templates : templatesCollections){
			bindElementTypeToFeatureTemplate(elementType, isReversed, templates);
		}
	}


	public void bindFeatureTemplateToElementTypes(
			Collection<FeatureTemplate> templates, int... elementTypes) {
		bindFeatureTemplateToElementTypes(templates, false, elementTypes);
	}

	public void bindFeatureTemplateToElementTypes(
			Collection<FeatureTemplate> templates, Boolean isReversed, int... elementTypes) {
		for (int elementType : elementTypes){
			bindElementTypeToFeatureTemplate(elementType, isReversed, templates);
		}
	}

	public void bindElementTypeToFeatureTemplate(int elementType,
			Boolean isReversed, Collection<FeatureTemplate> templates) {
		elementTypeToFeatureTemplates[elementType].addAll(templates);
		elementTypeToTemplateReverseFlags[elementType].addAll(
				Collections.nCopies(templates.size(), isReversed)); 
		for (FeatureTemplate template : templates){
			if (template.featureExtractor instanceof SinglePosContext){
				addBaseContext((ContextFeatureExtractor) template.featureExtractor, isReversed);
			}
			else if (template.featureExtractor instanceof DoublePosContext){
				addBasepairContext((DoublePosContext) template.featureExtractor, isReversed);
				addBaseContext(((DoublePosContext) template.featureExtractor).getFirstPosContext(), isReversed);
				addBaseContext(((DoublePosContext) template.featureExtractor).getSecondPosContext(), isReversed);
			}
		}
	}




	public int numOfFeatures() {return featuresNum;}

	public void finalize(){
		weightVector = new DenseFeatureVector(featuresNum);
		for (FeatureTemplate template : featureTemplates){
			Map<Integer, Float> initWeights = template.initWeights;
			if (initWeights != null){
				for (int feature : initWeights.keySet()){
					weightVector.setFeatureWeight(feature+template.firstVectorIx,
							initWeights.get(feature));
				}
			}
		}


		elementScores = new int[sr.numTypes()][][];
		seqFeatures = new int[sr.numTypes()][][] ;
		featuresMap = new HashMap<ContextFeatureExtractor, Integer[][]>();
		representativeMap = new HashMap<ContextFeatureExtractor, Integer>();

		makeRepresentativeContexts(Env.maxSuperContextCardinality);

	}


	public void makeRepresentativeContexts(int maxSuperContextCardinality) {
		maxLengthVal = 0; //a quick and dirty patch - not the best solution

		repBaseFeatureExtractors = getRepresentativeContexts(
				baseFeatureExtractors,maxSuperContextCardinality);
		baseFeatures = new int[repBaseFeatureExtractors.size()][];
		malocTypeScores(sr.getFirstBaseType(), sr.getLastBaseType(), 
				baseFeatureExtractors, repBaseFeatureExtractors, baseFeatures);


		repEdgeFeatureExtractors = getRepresentativeContexts(
				edgeFeatureExtractors,maxSuperContextCardinality);
		edgeFeatures = new int[repEdgeFeatureExtractors.size()][];
		malocTypeScores(sr.getFirstBasepairType(), sr.getLastBasepairType(), 
				edgeFeatureExtractors, repEdgeFeatureExtractors, edgeFeatures);
	}


	private void malocTypeScores(int firstTypeIx, int lastTypeIx, 
			List<ContextFeatureExtractor> contexts, 
			List<ContextFeatureExtractor> repTypeExtractors, int[][] features) {

		for (int i=0; i<repTypeExtractors.size(); ++i){
			features[i] = new int[0];
		}

		for (ContextFeatureExtractor context : contexts){
			bindToRepresentative(repTypeExtractors, context);
			bindToRepresentative(repTypeExtractors, context.reversedContext);
		}

		for (int type=firstTypeIx; type<=lastTypeIx; ++type){
			elementScores[type] = new int[repTypeExtractors.size()][];
			//			seqFeatures[type] = new short[repTypeExtractors.size()][][];
			for (int repIx=0; repIx<repTypeExtractors.size(); ++ repIx){
				elementScores[type][repIx] = new int[repTypeExtractors.get(repIx).cardinality];
				//				seqFeatures[type][repIx] = new short[0][0];
			}
		}
	}


	private void bindToRepresentative(
			List<ContextFeatureExtractor> repTypeExtractors,
			ContextFeatureExtractor context) {
		Integer[][] currFeaturesMap;
		for (int i=0; i<repTypeExtractors.size(); ++i){
			ContextFeatureExtractor repContext = repTypeExtractors.get(i);
			currFeaturesMap = context.getFeaturesMap(repContext);
			if (currFeaturesMap != null){
				representativeMap.put(context, i);
				featuresMap.put(context, currFeaturesMap);
				break;
			}
		}
	}


	private List<ContextFeatureExtractor> getRepresentativeContexts(
			List<ContextFeatureExtractor> contexts, int maxSuperContextCardinality) {

		List<ContextFeatureExtractor> representativeContexts = new ArrayList<ContextFeatureExtractor>();

		for (ContextFeatureExtractor context: contexts){
			updateRepresentatives(representativeContexts, context);
			updateRepresentatives(representativeContexts, context.reversedContext);
		}

		makeSuperRepresentatives(representativeContexts, maxSuperContextCardinality);

		return representativeContexts;
	}


	/**
	 * @param representativeContexts
	 */
	private void makeSuperRepresentatives(
			List<ContextFeatureExtractor> representativeContexts,
			int maxSuperContextCardinality) {
		ContextFeatureExtractor nextSuperContext;
		ContextFeatureExtractor currSuperContext;
		Collection<ContextFeatureExtractor> toRemove = new ArrayList<ContextFeatureExtractor>();
		do{
			nextSuperContext = null;
			for (ContextFeatureExtractor first : representativeContexts){
				for (ContextFeatureExtractor second : representativeContexts){
					if (first != second){
						currSuperContext = first.getSuperContext(second);
						if (currSuperContext.cardinality <= maxSuperContextCardinality &&
								(nextSuperContext == null || 
										nextSuperContext.cardinality > currSuperContext.cardinality)){
							nextSuperContext = currSuperContext;
						}
					}
				}
			}
			if (nextSuperContext != null){
				for (ContextFeatureExtractor context : representativeContexts){
					if (context.contained(nextSuperContext)){
						toRemove.add(context);
					}
				}
				representativeContexts.removeAll(toRemove);
				toRemove.clear();
				representativeContexts.add(nextSuperContext);
			}
		} while (nextSuperContext != null);
	}


	private void updateRepresentatives(
			List<ContextFeatureExtractor> representativeContexts,
			ContextFeatureExtractor context) {
		Integer[][] featuresMap;
		boolean represented;
		represented = false;
		Collection<ContextFeatureExtractor> toRemove = new ArrayList<ContextFeatureExtractor>();
		for (ContextFeatureExtractor repContext : representativeContexts){
			featuresMap = context.getFeaturesMap(repContext); 
			if (featuresMap != null) {
				represented = true;
				break;
			}
			featuresMap = repContext.getFeaturesMap(context);
			if (featuresMap != null) {
				toRemove.add(repContext);
			}
		}
		if (!represented){
			representativeContexts.removeAll(toRemove);
			toRemove.clear();
			representativeContexts.add(context);
		}
	}


	public StructreElementsScorer getElementScorer(RNA rna) {

		if (baseFeatures == null){ // represnetatives are not computed
			makeRepresentativeContexts(Env.maxSuperContextCardinality);
			maxLengthVal = 0;
		}

		if (scoresUpdateTime != weightVector.getUpdateTime()){
			float maxWeight = Math.max(weightVector.max(), -weightVector.min());
			scoreFactor = Env.MAX_INT_FEATURE_WEIGHT/maxWeight;
			updateScores();
		}

		if (scoresUpdateTime != weightVector.getUpdateTime() || 
				rna.length >= maxLengthVal){
			updateLengthScores(rna);
		}


		scoresUpdateTime = weightVector.getUpdateTime();

		computeFeatures(rna, repBaseFeatureExtractors, baseFeatures,
				sr.getFirstBaseType(), sr.getLastBaseType());
		computeFeatures(rna, repEdgeFeatureExtractors, edgeFeatures,
				sr.getFirstBasepairType(), sr.getLastBasepairType());

		StructreElementsScorerImpl elementScorer = new StructreElementsScorerImpl(seqFeatures, 
				elementScores, rna.length);
		elementScorer.setManagerAndRNA(this, rna);
		return elementScorer;
	}



	private void updateLengthScores(RNA rna) {
		FeatureExtractor featureExtractor;
		ValueCalculator valueCalculator;

		if (rna.length >= maxLengthVal){
			maxLengthVal = rna.length+1;
			lengthFeatures = new int[1][maxLengthVal];
			for (int i=0; i<maxLengthVal; ++i) lengthFeatures[0][i] = i;

			for (int type=sr.getFirstLengthType(); type<=sr.getLastLengthType(); ++ type){
				elementScores[type] = new int[1][maxLengthVal];
				seqFeatures[type] = lengthFeatures;
			}
		}

		for (int type=sr.getFirstLengthType(); type<=sr.getLastLengthType(); ++ type){
			int[] lengthElementScores = elementScores[type][0]; 
			Arrays.fill(lengthElementScores, 0);
			List<FeatureTemplate> templates = elementTypeToFeatureTemplates[type];
			for (FeatureTemplate template : templates){
				featureExtractor = template.getFeatureExtractor();
				featureExtractor.setSequence(rna.sequence());
				valueCalculator = template.getValueCalculator();
				for (int i=0; i<maxLengthVal; ++i){
					lengthElementScores[i] += Math.round(valueCalculator.getValue(i)*
							weightVector.getFeatureWeight(template.firstVectorIx+
									featureExtractor.getFeature(i))*scoreFactor);
				}
			}
		}
	}


	private void computeFeatures(RNA rna, List<ContextFeatureExtractor> repContexts, 
			int[][] features, int firstTypeIx, int lastTypeIx) {

		for (int i=0; i<repContexts.size(); ++i){
			FeatureExtractor context = repContexts.get(i);
			context.setSequence(rna.sequence());
			features[i] = context.getFeatures();
		}

		for (int type=firstTypeIx; type<=lastTypeIx; ++type){
			seqFeatures[type] = features;
		}
	}

	private void updateScores() {
		//TODO: consider a mechanism for updating only modified scores.

		ContextFeatureExtractor contextExtractor;
		Integer representativeIx;
		Integer[][] specificFeatures;


		for (int type=0; type<sr.getFirstLengthType(); ++type){
			for (int[] scores : elementScores[type]){
				Arrays.fill(scores, 0);
			}
			List<FeatureTemplate> templates = elementTypeToFeatureTemplates[type]; //sr.getElementName(type)
			List<Boolean> reverseFlags = elementTypeToTemplateReverseFlags[type];
			for (int templateIx=0; templateIx<templates.size(); ++templateIx){
				FeatureTemplate template = templates.get(templateIx);
				contextExtractor = (ContextFeatureExtractor) template.getFeatureExtractor();
				if (reverseFlags.get(templateIx)) {
					contextExtractor = contextExtractor.reversedContext;
				}
				specificFeatures = featuresMap.get(contextExtractor);
				representativeIx = representativeMap.get(contextExtractor);
				for (int i=0; i<contextExtractor.cardinality; ++i){
					float featureWeight = weightVector.getFeatureWeight(template.firstVectorIx+i);
					if (featureWeight != 0){
						for (int j : specificFeatures[i]){
							elementScores[type][representativeIx][j] += Math.round(featureWeight*scoreFactor);
						}
					}
				}
			}
		}
	}



	public DenseFeatureVector getFeatureWeights() {
		return weightVector;
	}

	public void loadWeights(String initialWeightsFile) throws IOException {
		DenseFeatureVector loadedWeightVector = null;
		try {
			loadedWeightVector = Utils.load(DenseFeatureVector.class, initialWeightsFile);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} 

		if (loadedWeightVector.dimension != weightVector.dimension){
			throw new RuntimeException("Loaded weight vector dimension is " + loadedWeightVector.dimension +
					", while model dimention is " + weightVector.dimension);
		}

		weightVector.acc = loadedWeightVector.acc;
		weightVector.dimension = loadedWeightVector.dimension;
		weightVector.featureNames = loadedWeightVector.featureNames;
		weightVector.upd = loadedWeightVector.upd;
		weightVector.weights = loadedWeightVector.weights;
		weightVector.updateTime = loadedWeightVector.updateTime;
	}

	public void loadWeights(String initialWeightsFile, float propOfMaxWeight) throws IOException {
		loadWeights(initialWeightsFile);
		weightVector.filter(propOfMaxWeight);
	}

	public AbstractFeatureVector getElementWeightedFeaturs(RNA rna, int elementType, int val1){

		List<FeatureTemplate> templates = elementTypeToFeatureTemplates[elementType];
		List<Boolean> reverseFlags = elementTypeToTemplateReverseFlags[elementType];
		int featureIx;
		float featureWeight;
		tempFeatureVector.clear();

		for (int templateIx=0; templateIx<templates.size(); ++templateIx){
			FeatureTemplate template = templates.get(templateIx);
			FeatureExtractor featureExtractor = template.featureExtractor;
			if (reverseFlags.get(templateIx)){
				featureExtractor = ((ContextFeatureExtractor) featureExtractor).getReversedContext();
			}
			featureIx = template.getFirstVectorIx() + featureExtractor.getFeature(rna.sequence(), val1);
			featureWeight = (template.valueCalculator.getValue(val1)*
					weightVector.getFeatureWeight(featureIx));
			tempFeatureVector.setFeatureWeight(featureIx, featureWeight);
		}
		return tempFeatureVector;
	}

	public AbstractFeatureVector getElementFeaturs(RNA rna, int elementType, int val1){
		List<FeatureTemplate> templates = elementTypeToFeatureTemplates[elementType];
		List<Boolean> reverseFlags = elementTypeToTemplateReverseFlags[elementType];
		int featureIx;
		tempFeatureVector.clear();

		for (int templateIx=0; templateIx<templates.size(); ++templateIx){
			FeatureTemplate template = templates.get(templateIx);
			FeatureExtractor featureExtractor = template.featureExtractor;
			if (reverseFlags.get(templateIx)){
				featureExtractor = ((ContextFeatureExtractor) featureExtractor).getReversedContext();
			}
			featureIx = template.getFirstVectorIx() + featureExtractor.getFeature(rna.sequence(), val1);
			tempFeatureVector.setFeatureWeight(featureIx, template.valueCalculator.getValue(val1));
		}
		return tempFeatureVector;
	}


	public void initWeight(Collection<FeatureTemplate> basepairTemplates,
			FeatureExtractor firstPosContext, FeatureExtractor secondPosContext, 
			float initialWeight, String... contextFeatures) {
		for (FeatureTemplate template : basepairTemplates){
			DoublePosContext context = (DoublePosContext) (template.featureExtractor);
			if (context.firstPosContext.equals(firstPosContext) &&
					context.secondPosContext.equals(secondPosContext)){
				for (String contextFeature : contextFeatures){
					template.setInitialWeight(initialWeight, contextFeature);
				}
				break;
			}
		}

	}


	public void addBasepairContext(FeatureExtractor firstPosContext,
			FeatureExtractor secondPosContext) {
		DoublePosContext context = new DoublePosContext(
				(ContextFeatureExtractor) firstPosContext, 
				(ContextFeatureExtractor) secondPosContext);
		addBasepairContext(context, false);

	}


	public void addBaseContexts(SinglePosContext[] contexts) {
		for (SinglePosContext context: contexts){
			addBaseContext(context, false);
		}
	}

	public static FeatureManager make(String model){

		try {
			return FeatureSetterFactory.newInstance(model).setFeatures();
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Un known contextFold.scoring model");
		}
	}

	public float getElementScore(RNA rna, int elementType, int val){
		AbstractFeatureVector elementFeatures = getElementFeaturs(rna, elementType, val);
		return elementFeatures.dot(weightVector);
	}

	public int effectiveFeatureCount(){
		int count = 0;
		for (FeatureTemplate template : featureTemplates){
			count += template.getFeatureExtractor().getEffectiveCardinality();
		}
		return count;
	}

	public String allFeatureValues(Comparator<Feature> featureComparator,boolean fiterZeros){
		StringBuilder sb = new StringBuilder();
		List<Feature> featureList = new ArrayList<Feature>();

		for (FeatureTemplate template : featureTemplates){
			for (int i=0; i<template.getCardinality(); ++i){
				float featureWeight = weightVector.getFeatureWeight(template.getFirstVectorIx()+i);
				if (!(fiterZeros && featureWeight == 0)){
					featureList.add(new Feature(
							template.getFeatureName(i),
							featureWeight));
				}
			}
		}

		Collections.sort(featureList, featureComparator);

		for (Feature feture : featureList){
			sb.append(feture.name).append(": ").append(feture.weight).append("\n");
		}


		return sb.toString();
	}

	public static Comparator<Feature> nullComparator = new Comparator<Feature>() {

		@Override
		public int compare(Feature o1, Feature o2) {
			return -1;
		}
	};

	public static Comparator<Feature> weightComparator = new Comparator<Feature>() {
		@Override
		public int compare(Feature o1, Feature o2) {
			if (o1.weight >= o2.weight) return -1;
			else return 1;
		}
	};

	public static Comparator<Feature> absWeightComparator = new Comparator<Feature>() {
		@Override
		public int compare(Feature o1, Feature o2) {
			if (Math.abs(o1.weight) >= Math.abs(o2.weight)) return -1;
			else return 1;
		}
	};


	public static class Feature{
		String name;
		float weight;


		public Feature(String name, float weight) {
			super();
			this.name = name;
			this.weight = weight;
		}

	}


	/**
	 * @param propOfMaxWeight a fraction s.t. weights whose absolute values are 
	 * below this faction with respect to the absolute maximum weight, are set
	 * to 0.
	 * 
	 * @return the proportion of filtered weights with respect to all non-zero
	 * weights.
	 */
	public float filterWeights(float propOfMaxWeight) {
		return weightVector.filter(propOfMaxWeight);
	}


	/**
	 * 
	 */
	public void compress() {
		baseFeatures = null;
		edgeFeatures = null;
		lengthFeatures = null;

		for (int i=0; i<sr.numTypes(); ++i){
			elementScores[i] = null;
			seqFeatures[i] = null;
		}

		featuresMap.clear();
		representativeMap.clear();
		weightVector.compress();

		for (FeatureExtractor fe: baseFeatureExtractors){
			fe.seqFeatures = null;
		}
		for (FeatureExtractor fe: edgeFeatureExtractors){
			fe.seqFeatures = null;
		}
		for (FeatureExtractor fe: repBaseFeatureExtractors){
			fe.seqFeatures = null;
		}
		for (FeatureExtractor fe: repEdgeFeatureExtractors){
			fe.seqFeatures = null;
		}
	}

}
