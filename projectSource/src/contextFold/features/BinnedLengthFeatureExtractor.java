/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.features;

public class BinnedLengthFeatureExtractor extends FeatureExtractor{

	private static final long serialVersionUID = 359725666704635808L;

	int firstBinSize;
	int binSize;

	public BinnedLengthFeatureExtractor(int binSize, int cardinality, boolean isOdd){
		super(cardinality);
		this.binSize = binSize;
		if (isOdd) firstBinSize = binSize/2;
		else firstBinSize = binSize;
	}

	public String toString(){
		String odd = "_";
		if (firstBinSize != binSize) odd = "Odd";
		return "Length"+binSize+odd+cardinality;
	}

	protected void preprocess(byte[] seq) {
		
		if (seq.length>=seqFeatures.length){
			int maxVal = seq.length+1; 
			seqFeatures = new int[maxVal];
			for (int i=0; i<maxVal; ++i){
				seqFeatures[i] = getFeature(seq, i);
			}
		}
	}

	@Override
	public int getFeature(byte[] seq, int val) {
		if (val<firstBinSize) return 0;
		else return Math.min(cardinality-1, (val-firstBinSize)/binSize+1);
	}
}
