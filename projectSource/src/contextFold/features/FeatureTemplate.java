/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.features;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import contextFold.rna.RNA;


public class FeatureTemplate implements Serializable{

	private static final long serialVersionUID = 1724149770194889341L;

	private static final FeatureExtractor defaultFeatureCalculator = FeatureExtractor.FEATURE_1_EXTRACTOR;
	private static final ValueCalculator defaultValueCalculator = ValueCalculator.VALUE_1_CALCULATOR;

	protected int firstVectorIx;
	protected String name;
	protected FeatureExtractor featureExtractor;
	protected ValueCalculator valueCalculator;

	protected Map<Integer, Float> initWeights;

	public FeatureTemplate(String templateName, FeatureExtractor featureExtractor) {
		this(templateName, featureExtractor, defaultValueCalculator);
	}
	
	public FeatureTemplate(String templateName, ValueCalculator valueCalculator) {
		this(templateName, defaultFeatureCalculator, valueCalculator);
	}

	public FeatureTemplate(String templateName, FeatureExtractor featureExtractor,
			ValueCalculator valueCalculator) {
		super();
		firstVectorIx = Integer.MIN_VALUE; // will (hopefully) induce an exception if not initialized. 
		this.name = templateName;
		this.featureExtractor = featureExtractor;
		this.valueCalculator = valueCalculator;
	}

	/**
	 * 
	 * @return the index in the feature/weight vector of the
	 * first feature of the template. 	
	 */
	public int getFirstVectorIx() {return firstVectorIx;}

	protected void setFirstVectorIx(int newIx) {firstVectorIx = newIx;}

	/**
	 * @return the name of template.
	 */
	public String getName() {return name;}

	/**
	 * @return the number of contextFold.features in the template.
	 */
	public int getCardinality() {return featureExtractor.cardinality;}
	
	public FeatureExtractor getFeatureExtractor() {
		return featureExtractor;
	}

	public ValueCalculator getValueCalculator() {
		return valueCalculator;
	}
	
	public String toString(){
		return name;
	}

	public void setInitialWeight(float initialWeight, String contextFeature) {
		byte[] word = RNA.stringToByte(contextFeature); 
		if (initWeights == null) initWeights = new HashMap<Integer, Float>();
		initWeights.put(WordFeatureCalculator.getFeature(word), initialWeight);
	}

	public String getFeatureName(int i) {
		return (firstVectorIx+i) +"_" + getName()+"_"+		
		featureExtractor.getFeatureName(i) + valueCalculator.getValueName(i);
	}
}
