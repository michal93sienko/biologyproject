/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.features;

import java.util.Arrays;
import java.util.Iterator;

import contextFold.common.Env;


/**
 * A dense implementation of a feature vector
 */
public class DenseFeatureVector extends AbstractFeatureVector{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5653817811624274914L;


	protected float[] weights;
	// these are for avaraging:
	protected float[] acc; // accumulated value
	protected int[] upd;    // last update time

	protected int[] intWeights; 

	protected int dimension; // the length of the vector
	protected int updateTime; // a time-stamp of the latest update

	private float floatMin;
	private float floatMax;

	protected boolean isCompressed;

	/**
	 * A constructor.
	 * @param dimension the length of the constructed vector.
	 */
	public DenseFeatureVector(int dimension) {
		this.dimension = dimension;
		malocArrays();
		updateTime = 0;
		isCompressed = false;
	}

	private void malocArrays() {
		weights = new float[dimension];
		acc = new float[dimension]; 
		upd = new int[dimension]; 
	}

	/**
	 * A constructor.
	 * @param length the length of the constructed vector.
	 * @param epsilon an initial value to set in all the entries.
	 */
	public DenseFeatureVector(int length, float epsilon) {
		this(length);
		Arrays.fill(weights, epsilon);
	}

	/**
	 * A copy constructor.
	 * @param toCopy the vector to be copied.
	 */
	public DenseFeatureVector(DenseFeatureVector toCopy) {
		weights = Arrays.copyOf(toCopy.weights, toCopy.weights.length);
		acc = Arrays.copyOf(toCopy.acc, toCopy.acc.length);
		upd = Arrays.copyOf(toCopy.upd, toCopy.upd.length);

		dimension = toCopy.dimension;
		updateTime = toCopy.updateTime;
	}

	/**
	 * 
	 * @return the time-stamp of the last update to the vector
	 */
	public int getUpdateTime() {return updateTime;} 


	@Override
	public float dot(AbstractFeatureVector other) {
		if (!(other instanceof DenseFeatureVector)) {
			return super.dot(other);
		}
		DenseFeatureVector denseOther = (DenseFeatureVector) other; 
		float acc=0;
		for (int i=0; i<this.weights.length; i++) {
			acc+=this.weights[i]*denseOther.weights[i];
		}
		return acc;
	}


	@Override
	public void add(AbstractFeatureVector other) {
		this.add(other,1);
	}

	@Override
	public void sub(AbstractFeatureVector other, float amount) {
		this.add(other,-amount);
	}

	@Override
	public void sub(AbstractFeatureVector other) {
		this.sub(other,1);
	}

	/**
	 * Increases time stamp by one.
	 */
	public void tick() {
		++updateTime;
	}

	/**
	 * Executes add(other, s), while maintaining an inner state that allows averaging over all updates. 
	 * @param other
	 * @param s
	 */
	public void update(AbstractFeatureVector other, float s) {
		for (int i=0;i<this.weights.length;i++) {
			this.acc[i] += this.weights[i]*(updateTime-this.upd[i]);
			this.upd[i] = updateTime;
		}
		this.add(other, s);
	}


	/**
	 * Setting weights to the average values over all updates.
	 *  
	 */
	public void average() {
		for (int i=0; i<this.weights.length; i++) {
			this.acc[i] += this.weights[i]*(updateTime-this.upd[i]);
			this.weights[i] = this.acc[i]/updateTime;
			this.acc[i] = this.weights[i];
			this.upd[i] = 1;
		}
		updateTime = 1;
	}

	/**
	 * 
	 * @return a new DenseFeatureVector object of the same dimension as this one.
	 */
	public DenseFeatureVector newEmpty() {
		return new DenseFeatureVector(dimension);
	}

	/**
	 * 
	 * @return the dimension of the vector.
	 */
	public int dim() {
		return this.dimension;
	}


	@Override
	public float getFeatureWeight(int featureIx) {
		return weights[featureIx];
	}

	//	public void setFeatureWeight(int featureIx, float featureWeight) {
	//		weights[featureIx] = featureWeight;
	//	}


	@Override
	public Iterator<Integer> getNonZeroFeatures() {
		return new DenseFeatureVectorIterator(dimension);
	}

	@Override
	public void setFeatureWeight(int featureIx, float newFeatureWeight) {
		weights[featureIx] = newFeatureWeight;
	}

	private class DenseFeatureVectorIterator implements Iterator<Integer>{


		private int next;

		public DenseFeatureVectorIterator(int dimension) {
			next = 0;
			while (next < dimension && weights[next] == 0) ++next;
		}

		public boolean hasNext() {
			return next < dimension;
		}

		public Integer next() {
			int res = next;
			++next;
			while (next < dimension && weights[next] == 0) ++next;
			return res;
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}

	}

	@Override
	public void clear() {
		Arrays.fill(weights, 0);
	}

	@Override
	public float norm() {
		float norm = 0;
		for (int i=0; i<dimension; ++i){
			norm += weights[i]*weights[i];
		}
		return norm;
	}

	/* (non-Javadoc)
	 * @see contextFold.features.AbstractFeatureVector#countNonZeroEntries()
	 */
	@Override
	public int countNonZeroEntries() {
		int count = 0;

		if (isCompressed){
			for (int weight : intWeights){
				if (weight != 0) ++ count;
			}
		}
		else{
			for (float weight : weights){
				if (weight != 0) ++ count;
			}
		}
		return count;
	}

	/**
	 * @return the maximum value in the vector
	 */
	public float max() {
		float max = Float.NEGATIVE_INFINITY;
		for (float weight : weights){
			if (weight > max) max = weight;
		}
		return max;
	}

	/**
	 * @return the minimum value in the vector
	 */
	public float min() {
		float min = Float.POSITIVE_INFINITY;
		for (float weight : weights){
			if (weight < min) min = weight;
		}
		return min;
	}

	public void compress(){
		compress(Env.MAX_INT_FEATURE_WEIGHT*2);
	}

	/**
	 * Maps all float weights to the given range of integers. This allows for a more
	 * compact serialization of the object.
	 * </br>
	 * IMPORTANT! 
	 * After compressing the object, it cannot be used before applying
	 * the {@link uncompress()} method (otherwise, exceptions will be 
	 * thrown). In addition, compression deletes information which is
	 * required for averaging the vector.
	 * 
	 * @param intRange the range of integers to which the weights are mapped.
	 */
	public void compress(int intRange){
		if (isCompressed == false){
			floatMin = min();
			floatMax = max();
			float floatRange = floatMax - floatMin;
			float factor = intRange/floatRange;
			intWeights = new int[dimension];

			for (int i=0; i<dimension; ++i){
				intWeights[i] = Math.round(
						(weights[i] - floatMin)*factor);
			}

			weights = null;
			acc = null;
			upd = null;
			isCompressed = true;
		}
	}

	public void uncompress(){
		if (isCompressed == true){
			malocArrays();
			int intRange = 0;
			for (int weight : intWeights){
				if (weight > intRange) intRange = weight;
			}
			float factor = (floatMax - floatMin)/intRange;

			for (int i=0; i<dimension; ++i){
				weights[i] = intWeights[i]*factor + floatMin;
			}

			intWeights = null;
			updateTime = 1;
			isCompressed = false;

		}
	}
}
