/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.features;

import java.io.Serializable;
import java.util.Arrays;

public abstract class FeatureExtractor implements Serializable {

	private static final long serialVersionUID = -1005827948851607113L;
	public static final FeatureExtractor FEATURE_1_EXTRACTOR = new FeatureExtractor(1) {
		private static final long serialVersionUID = 2104044822381585004L;
		@Override
		public int getFeature(int val) {
			return 0;
		}
		@Override
		public void preprocess(byte[] seq) {}
		
		@Override
		public int getFeature(byte[] seq, int val1) {
			return 0;
		}
	};
	
	protected int cardinality;
	protected int[] seqFeatures;
	protected byte[] currProcessedSeq;
	
	protected FeatureExtractor(int cardinality) {
		super();
		this.cardinality = cardinality;
		seqFeatures = new int[0];
	}
	
	public int getCardinality(){return cardinality;}

	public int[] getFeatures() {return seqFeatures;}

	public int getFeature(int val) {
		return seqFeatures[val];
	}

	public abstract int getFeature(byte[] seq, int val1);

	protected abstract void preprocess(byte[] seq);

	public void setSequence(byte[] seq) {
		if (!Arrays.equals(seq, currProcessedSeq)){
			preprocess(seq);
			currProcessedSeq = seq;
		}
	}

	public int getEffectiveCardinality() {return cardinality;}

	public String getFeatureName(int featureIx) {
		return "";
	}
}
