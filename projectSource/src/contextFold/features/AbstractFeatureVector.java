/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.features;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;

import contextFold.common.Dotable;


/**
 * A weight vector object.
 */
public abstract class AbstractFeatureVector implements Dotable<AbstractFeatureVector>, Serializable  {

	private static final long serialVersionUID = 2795966210352590105L;
	
	protected Map<Integer, String> featureNames;
	
	
	/**
	 * @return the L_2 norm of the vector.
	 */
	public abstract float norm();

	
	/**
	 * Adds to this object another weight vector. Each weight entry {@code w_i}
	 * of this object is set to be {@code w_i = w_i + amount*w'_i}, where 
	 * {@code w'_i} is the i-th weight of the other vector, and {@code amount}
	 * is the given scaling parameter.
	 * 
	 * @param other a weight vector to be added.
	 * @param amount a scaling parameter.
	 */
	public void add(AbstractFeatureVector other, float amount){
		Iterator<Integer> otherFeatures = other.getNonZeroFeatures();
		int featureIx;
		float thisFeatureWeight, newFeatureWeight;
		
		while (otherFeatures.hasNext()){
			featureIx = otherFeatures.next();
			thisFeatureWeight = getFeatureWeight(featureIx);
			newFeatureWeight = thisFeatureWeight + other.getFeatureWeight(featureIx)*amount;
			setFeatureWeight(featureIx, newFeatureWeight);
		}
	}

	/**
	 * The same as {@link #add(AbstractFeatureVector, float)}, where
	 * {@code amount} = 1.
	 * @param other  a weight vector to be added.
	 */
	public void add(AbstractFeatureVector other){
		add(other, 1);
	}

	/**
	 * The same as {@link #add(AbstractFeatureVector, float)}, where
	 * {@code other} is being subtracted rather than added.
	 * @param other  a weight vector to be subtracted.
	 * @param amount a scaling parameter.
	 */
	public void sub(AbstractFeatureVector other, float amount){
		add(other, -amount);
	}

	/**
	 * The same as {@link #sub(AbstractFeatureVector, float)}, where
	 * {@code amount} = 1.
	 * @param other  a weight vector to be subtracted.
	 */
	public void sub(AbstractFeatureVector other){
		add(other, -1);
	}

	/**
	 * 
	 * @param featureIx an index of a feature.
	 * @return the weight of the feature which index is {@code featureIx}.
	 */
	public abstract float getFeatureWeight(int featureIx);

	/**
	 * Sets the weight of a given feature.
	 * 
	 * @param featureIx an index of a feature.
	 * @param newFeatureWeight a value to be set as the weight of
	 * the feature which index is {@code featureIx}.
	 */
	public abstract void setFeatureWeight(int featureIx, float newFeatureWeight);

	/**
	 * @return an Iterator over the indices of features which weights
	 * are not zero.
	 */
	public abstract Iterator<Integer> getNonZeroFeatures();

	/* (non-Javadoc)
	 * @see contextFold.common.Dotable#dot(java.lang.Object)
	 */
	public float dot(AbstractFeatureVector other){
		float res = 0;
		Iterator<Integer> thisFeatures;
		if (this instanceof SparseFeatureVector) thisFeatures = getNonZeroFeatures();
		else thisFeatures = other.getNonZeroFeatures();
		int featureIx;
		while (thisFeatures.hasNext()){
			featureIx = thisFeatures.next();
			res += getFeatureWeight(featureIx)*other.getFeatureWeight(featureIx);
		}
		return res;
	}

	
	/**
	 * Sets all weights to 0. 
	 */
	public abstract void clear(); 

	/**
	 * @return the summation of all weights.
	 */
	public float sum() {
		float res = 0;
		Iterator<Integer> thisFeatures = getNonZeroFeatures();
		int featureIx;
		while (thisFeatures.hasNext()){
			featureIx = thisFeatures.next();
			res += getFeatureWeight(featureIx);
		}
		return res;
	}

	public void setFeatureNames(Map<Integer, String> names){
		this.featureNames = names;
	}
	
	/**
	 * @return the number of features which weights are
	 * not 0.
	 */
	public abstract int countNonZeroEntries();
	
	/**
	 * 
	 * @param propOfMaxWeight a fraction s.t. weights whose absolute values are 
	 * below this faction with respect to the absolute maximum weight, are set
	 * to 0.
	 * 
	 * @return the proportion of filtered weights with respect to all non-zero
	 * weights.
	 */
	public float filter(float propOfMaxWeight){
		if (propOfMaxWeight == 0) return 0;
		int filteredCount = 0;
		int nonZeros = countNonZeroEntries();
		float maxWeight = 0;
		
		Iterator<Integer> iter = this.getNonZeroFeatures();
		while (iter.hasNext()){
			int featureIx = iter.next();
			float currWeight = getFeatureWeight(featureIx);
			if (currWeight > maxWeight) maxWeight = currWeight;
			else if (-currWeight > maxWeight) maxWeight = -currWeight;
		}
		
		float filterTreshold = maxWeight*propOfMaxWeight;
		iter = this.getNonZeroFeatures();
		while (iter.hasNext()){
			int featureIx = iter.next();
			float currWeight = getFeatureWeight(featureIx);
			if (currWeight < filterTreshold && -currWeight < filterTreshold){
				setFeatureWeight(featureIx, 0);
				++filteredCount;
			}
		}
		
		return ((float) filteredCount) / nonZeros;
	}

}
