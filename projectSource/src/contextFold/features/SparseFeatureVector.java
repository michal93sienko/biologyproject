/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.features;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import contextFold.common.Env;
import contextFold.common.NumberComparator;
import contextFold.common.Utils;

public class SparseFeatureVector extends AbstractFeatureVector{

	private static final long serialVersionUID = -8553992343906370592L;

	private Map<Integer, Float> weights;

	public SparseFeatureVector() {
		super();
		weights  = new HashMap<Integer, Float>();
	}
	
	public SparseFeatureVector(AbstractFeatureVector toCopy) {
		this();
		Iterator<Integer> iter = toCopy.getNonZeroFeatures();
		while (iter.hasNext()){
			Integer featureIx = iter.next();
			setFeatureWeight(featureIx, toCopy.getFeatureWeight(featureIx));
		}
	}


	public float getFeatureWeight(int featureIx) {
		Float weight = weights.get(featureIx);
		if (weight == null) return 0;
		else return weight;
	}
	
//	public void setFeatureWeight(int featureIx, float featureWeight) {
//		weights.put(featureIx, featureWeight);
//	}
	
	
	@Override
	public Iterator<Integer> getNonZeroFeatures() {
		return weights.keySet().iterator();
	}
	
	@Override
	public void setFeatureWeight(int featureIx, float newFeatureWeight) {
		if (Utils.floatEquals(newFeatureWeight, 0)) weights.remove(featureIx);
		else weights.put(featureIx, newFeatureWeight);
	}

	@Override
	public void clear() {
		weights.clear();
	}
	
	public String toString(){
		String res = "";
		float sum = 0;
		String featureName = ""; 
		boolean printName = Env.DEBUG_MODE && featureNames != null;
		Object[] features = weights.keySet().toArray();
		Arrays.sort(features, new NumberComparator());
		for (int i=0; i<features.length; ++i){
			int featureIx = (Integer) features[i];
			float featureWeight = getFeatureWeight(featureIx);
			if (printName) featureName = featureNames.get(featureIx) + ","; 
			res += "(" + featureName + featureIx + "," + featureWeight + "), ";
			sum += featureWeight;
		}
		res += "sum: " + sum + ", norm: " + norm();;
		return res; 
	}

	public String toString(AbstractFeatureVector weights){
		String res = "";
		float sum = 0;
		float weight = 0;
		Object[] features = this.weights.keySet().toArray();
		Arrays.sort(features, new NumberComparator());
		for (int i=0; i<features.length; ++i){
			int featureIx = (Integer) features[i];
			float featureWeight = getFeatureWeight(featureIx);
			res += "(" + featureIx + "," + featureWeight + ", "+ featureWeight*weights.getFeatureWeight(featureIx)+ "), ";
			sum += featureWeight;
			weight += featureWeight*weights.getFeatureWeight(featureIx);
		}
		res += "\nsum: " + sum + ", weight: " + weight +", norm: " + norm();;
		return res; 
	}

	public String toString(AbstractFeatureVector weights, boolean printColumn){
		if (!printColumn) return toString(weights);
		String res = "";
		float sum = 0;
		float weight = 0;
		Object[] features = this.weights.keySet().toArray();
		Arrays.sort(features, new NumberComparator());
		for (int i=0; i<features.length; ++i){
			int featureIx = (Integer) features[i];
			float featureWeight = getFeatureWeight(featureIx);
			res += featureIx + "," + featureWeight + ", "+ featureWeight*weights.getFeatureWeight(featureIx)+ "\n";
			sum += featureWeight;
			weight += featureWeight*weights.getFeatureWeight(featureIx);
		}
		res += "sum: " + sum + ", weight: " + weight +", norm: " + norm() + "\n";
		return res; 
	}
	
	
	@Override
	public float norm() {
		float norm = 0;
		for (float weight : weights.values()){
			norm += weight*weight;
		}
		return norm;
	}

	@Override
	public int countNonZeroEntries() {
		return weights.values().size();
	}
	
}
