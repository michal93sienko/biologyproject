/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.models;

public class FeatureSetterFactory {
	@SuppressWarnings("unchecked")
	public static FeatureSetter newInstance(String modelName) throws ClassNotFoundException{

		ClassLoader classLoader = FeatureSetter.class.getClassLoader();
		Class<? extends FeatureSetter> aFeatureSetterClass;
		try {
			aFeatureSetterClass = 
				(Class<? extends FeatureSetter>) classLoader.loadClass(modelName);
		} catch (ClassNotFoundException e) {
			aFeatureSetterClass = 
				(Class<? extends FeatureSetter>) classLoader.loadClass("contextFold.models." + modelName);
		}
		
		FeatureSetter featureSetter = null;
		
		try {
			featureSetter = (FeatureSetter) aFeatureSetterClass.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return featureSetter;
	}
}
