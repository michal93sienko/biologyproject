/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.models;


import java.util.Collection;

import contextFold.features.BinnedLengthFeatureExtractor;
import contextFold.features.ContextFeatureExtractor;
import contextFold.features.DoublePosContext;
import contextFold.features.FeatureExtractor;
import contextFold.features.FeatureManager;
import contextFold.features.FeatureTemplate;
import contextFold.features.SinglePosContext;
import contextFold.features.ValueCalculator;
import contextFold.features.WordFeatureCalculator;
import contextFold.rnaFolding.StructureRecognizerImpl;




public abstract class FeatureSetter {
	
	protected StructureRecognizerImpl csr;
	protected FeatureManager featureManager;

	protected FeatureSetter(){
		csr = new StructureRecognizerImpl();
		featureManager = new FeatureManager(csr);
	}

	protected FeatureSetter(int hpShortMaxLength, int[] ilLong, int[] ilMedium){
		csr = new StructureRecognizerImpl(hpShortMaxLength, ilLong, ilMedium);
		featureManager = new FeatureManager(csr);
	}

	/********************* Contexts *************************************/

	static final int fullAlphabet = WordFeatureCalculator.FULL;
	static final int reducedAlphabet = WordFeatureCalculator.REDUCED;// Purines (A, G)->0, Pyrimidines (C, U)->1.
	static final int trivalAlphabet = WordFeatureCalculator.TRIVIAL; //A, C, G, U->0 (distinguished only from the OUT_OF_RANGE character).

	//-------------------- single position contexts, full alphabet -----------------------
	// context length: 1
	//	static final SinglePosContext spc_f_m2 = new SinglePosContext(fullAlphabet, -2);
	static final SinglePosContext spc_f_m1 = new SinglePosContext(fullAlphabet, -1);
	static final SinglePosContext spc_f_0 = new SinglePosContext(fullAlphabet, 0);
	static final SinglePosContext spc_f_1 = new SinglePosContext(fullAlphabet, 1);
	//	static final SinglePosContext spc_f_2 = new SinglePosContext(fullAlphabet, 2);

	// context length: 2
	//	static final SinglePosContext spc_f_m2m1 = new SinglePosContext(fullAlphabet, -2, -1);
	static final SinglePosContext spc_f_m10 = new SinglePosContext(fullAlphabet, -1, 0);
	static final SinglePosContext spc_f_m11 = new SinglePosContext(fullAlphabet, -1, 1);
	static final SinglePosContext spc_f_01 = new SinglePosContext(fullAlphabet, 0, 1);
	//	static final SinglePosContext spc_f_12 = new SinglePosContext(fullAlphabet, 1, 2);

	// context length: 3
	static final SinglePosContext spc_f_m2m10 = new SinglePosContext(fullAlphabet, -2, -1, 0);
	static final SinglePosContext spc_f_m101 = new SinglePosContext(fullAlphabet, -1, 0, 1);
	static final SinglePosContext spc_f_012 = new SinglePosContext(fullAlphabet, 0, 1, 2);

	//-------------------- single position contexts, reduced alphabet -----------------------
	// context length: 1
	//	static final SinglePosContext spc_r_m2 = new SinglePosContext(reducedAlphabet, -2);
	static final SinglePosContext spc_r_m1 = new SinglePosContext(reducedAlphabet, -1);
	static final SinglePosContext spc_r_0 = new SinglePosContext(reducedAlphabet, 0);
	static final SinglePosContext spc_r_1 = new SinglePosContext(reducedAlphabet, 1);
	//	static final SinglePosContext spc_r_2 = new SinglePosContext(reducedAlphabet, 2);

	// context length: 2
	//	static final SinglePosContext spc_r_m2m1 = new SinglePosContext(reducedAlphabet, -2, -1);
	static final SinglePosContext spc_r_m10 = new SinglePosContext(reducedAlphabet, -1, 0);
	static final SinglePosContext spc_r_m11 = new SinglePosContext(reducedAlphabet, -1, 1);
	static final SinglePosContext spc_r_01 = new SinglePosContext(reducedAlphabet, 0, 1);
	//	static final SinglePosContext spc_r_12 = new SinglePosContext(reducedAlphabet, 1, 2);

	// context length: 3
	static final SinglePosContext spc_r_m2m10 = new SinglePosContext(reducedAlphabet, -2, -1, 0);
	static final SinglePosContext spc_r_m101 = new SinglePosContext(reducedAlphabet, -1, 0, 1);
	static final SinglePosContext spc_r_012 = new SinglePosContext(reducedAlphabet, 0, 1, 2);

	//-------------------- single position contexts, trivial alphabet -----------------------
	// context length: 0 (for non-context features)
	static final SinglePosContext empty_spc = new SinglePosContext(trivalAlphabet);

	// context length: 2 (for sequence termination features)
	static final SinglePosContext spc_t_m1 = new SinglePosContext(trivalAlphabet, -1);
	static final SinglePosContext spc_t_1 = new SinglePosContext(trivalAlphabet, 1);

	// context length: 3 (for sequence termination features)
	static final SinglePosContext spc_t_m2m1 = new SinglePosContext(trivalAlphabet, -2, -1);
	static final SinglePosContext spc_t_12 = new SinglePosContext(trivalAlphabet, 1, 2);


	//-------------------- double position contexts, full alphabet -----------------------
	
	// context length: 1-1 
	static final DoublePosContext dpc_f_0_0 = new DoublePosContext(spc_f_0, spc_f_0);

	// context length: 1-2 
	static final DoublePosContext dpc_f_m1_01 = new DoublePosContext(spc_f_m1, spc_f_01);
	static final DoublePosContext dpc_f_0_m10 = new DoublePosContext(spc_f_0, spc_f_m10);
	static final DoublePosContext dpc_f_0_01 = new DoublePosContext(spc_f_0, spc_f_01);
	static final DoublePosContext dpc_f_0_m11 = new DoublePosContext(spc_f_0, spc_f_m11);
	static final DoublePosContext dpc_f_1_m10 = new DoublePosContext(spc_f_1, spc_f_m10);

	// context lestatic final ngth: 1-3 
	static final DoublePosContext dpc_f_0_m2m10 = new DoublePosContext(spc_f_0, spc_f_m2m10);
	static final DoublePosContext dpc_f_0_m101 = new DoublePosContext(spc_f_0, spc_f_m101);
	static final DoublePosContext dpc_f_0_012 = new DoublePosContext(spc_f_0, spc_f_012);

	// context length: 2-1 
	static final DoublePosContext dpc_f_m10_1 = new DoublePosContext(spc_f_m10, spc_f_1);
	static final DoublePosContext dpc_f_m10_0 = new DoublePosContext(spc_f_m10, spc_f_0);
	static final DoublePosContext dpc_f_m11_0 = new DoublePosContext(spc_f_m11, spc_f_0);
	static final DoublePosContext dpc_f_01_m1 = new DoublePosContext(spc_f_01, spc_f_m1);
	static final DoublePosContext dpc_f_01_0 = new DoublePosContext(spc_f_01, spc_f_0);

	// context length: 2-2 
	static final DoublePosContext dpc_f_m10_01 = new DoublePosContext(spc_f_m10, spc_f_01);
	static final DoublePosContext dpc_f_01_m10 = new DoublePosContext(spc_f_01, spc_f_m10);
	static final DoublePosContext dpc_f_m11_m11 = new DoublePosContext(spc_f_m11, spc_f_m11);

	// context length: 2-3 
	static final DoublePosContext dpc_f_m10_m101 = new DoublePosContext(spc_f_m10, spc_f_m101);
	static final DoublePosContext dpc_f_01_m101 = new DoublePosContext(spc_f_01, spc_f_m101);
	static final DoublePosContext dpc_f_m11_m101 = new DoublePosContext(spc_f_m11, spc_f_m101);

	// context length: 3-1 
	static final DoublePosContext dpc_f_m2m10_0 = new DoublePosContext(spc_f_m2m10, spc_f_0);
	static final DoublePosContext dpc_f_m101_0 = new DoublePosContext(spc_f_m101, spc_f_0);
	static final DoublePosContext dpc_f_012_0 = new DoublePosContext(spc_f_012, spc_f_0);

	// context length: 3-2 
	static final DoublePosContext dpc_f_m101_01 = new DoublePosContext(spc_f_m101, spc_f_01);
	static final DoublePosContext dpc_f_m101_m10 = new DoublePosContext(spc_f_m101, spc_f_m10);
	static final DoublePosContext dpc_f_m101_m11 = new DoublePosContext(spc_f_m101, spc_f_m11);

	static final DoublePosContext dpc_f_012_m10 = new DoublePosContext(spc_f_012, spc_f_m10); //for hp3

	// context length: 3-3 
	static final DoublePosContext dpc_f_m101_m101 = new DoublePosContext(spc_f_m101, spc_f_m101);

	static final DoublePosContext dpc_f_012_m2m10 = new DoublePosContext(spc_f_012, spc_f_m2m10); //for hp4

	//-------------------- double position contexts, reduced alphabet -----------------------
	// context length: 1-1 
	static final DoublePosContext dpc_r_0_0 = new DoublePosContext(spc_r_0, spc_r_0);

	// context length: 1-2 
	static final DoublePosContext dpc_r_m1_01 = new DoublePosContext(spc_r_m1, spc_r_01);
	static final DoublePosContext dpc_r_0_m10 = new DoublePosContext(spc_r_0, spc_r_m10);
	static final DoublePosContext dpc_r_0_01 = new DoublePosContext(spc_r_0, spc_r_01);
	static final DoublePosContext dpc_r_0_m11 = new DoublePosContext(spc_r_0, spc_r_m11);
	static final DoublePosContext dpc_r_1_m10 = new DoublePosContext(spc_r_1, spc_r_m10);

	// context length: 1-3 
	static final DoublePosContext dpc_r_0_m2m10 = new DoublePosContext(spc_r_0, spc_r_m2m10);
	static final DoublePosContext dpc_r_0_m101 = new DoublePosContext(spc_r_0, spc_r_m101);
	static final DoublePosContext dpc_r_0_012 = new DoublePosContext(spc_r_0, spc_r_012);

	// context length: 2-1 
	static final DoublePosContext dpc_r_m10_1 = new DoublePosContext(spc_r_m10, spc_r_1);
	static final DoublePosContext dpc_r_m10_0 = new DoublePosContext(spc_r_m10, spc_r_0);
	static final DoublePosContext dpc_r_m11_0 = new DoublePosContext(spc_r_m11, spc_r_0);
	static final DoublePosContext dpc_r_01_m1 = new DoublePosContext(spc_r_01, spc_r_m1);
	static final DoublePosContext dpc_r_01_0 = new DoublePosContext(spc_r_01, spc_r_0);

	// context length: 2-2 
	static final DoublePosContext dpc_r_m10_01 = new DoublePosContext(spc_r_m10, spc_r_01);
	static final DoublePosContext dpc_r_01_m10 = new DoublePosContext(spc_r_01, spc_r_m10);
	static final DoublePosContext dpc_r_m11_m11 = new DoublePosContext(spc_r_m11, spc_r_m11);

	// context length: 2-3 
	static final DoublePosContext dpc_r_m10_m101 = new DoublePosContext(spc_r_m10, spc_r_m101);
	static final DoublePosContext dpc_r_01_m101 = new DoublePosContext(spc_r_01, spc_r_m101);
	static final DoublePosContext dpc_r_m11_m101 = new DoublePosContext(spc_r_m11, spc_r_m101);

	// context length: 3-1 
	static final DoublePosContext dpc_r_m2m10_0 = new DoublePosContext(spc_r_m2m10, spc_r_0);
	static final DoublePosContext dpc_r_m101_0 = new DoublePosContext(spc_r_m101, spc_r_0);
	static final DoublePosContext dpc_r_012_0 = new DoublePosContext(spc_r_012, spc_r_0);

	// context length: 3-2 
	static final DoublePosContext dpc_r_m101_01 = new DoublePosContext(spc_r_m101, spc_r_01);
	static final DoublePosContext dpc_r_m101_m10 = new DoublePosContext(spc_r_m101, spc_r_m10);
	static final DoublePosContext dpc_r_m101_m11 = new DoublePosContext(spc_r_m101, spc_r_m11);

	static final DoublePosContext dpc_r_012_m10 = new DoublePosContext(spc_r_012, spc_r_m10); //for hp3

	// context length: 3-3 
	static final DoublePosContext dpc_r_m101_m101 = new DoublePosContext(spc_r_m101, spc_r_m101);

	static final DoublePosContext dpc_r_012_m2m10 = new DoublePosContext(spc_r_012, spc_r_m2m10); //for hp4



	//-------------------- double position contexts, trivial alphabet -----------------------
	// context length: 0-0 
	static final 	DoublePosContext empty_dpc = new DoublePosContext(empty_spc, empty_spc);

	// context length: 2-2 
	static final DoublePosContext dpc_t_m2m1_12 = new DoublePosContext(spc_t_m2m1, spc_t_12);
	static final DoublePosContext dpc_t_12_m1m2 = new DoublePosContext(spc_t_12, spc_t_m2m1);


	//-------------------- context collections -----------------------

	static final SinglePosContext[] baseContextsTrivial = {
		// trivial alphabet:
		empty_spc,
	};

	
	//trivial:
	static final SinglePosContext[] baseContexts0 = {
		// trivial alphabet:
		empty_spc,
		// full alphabet:
		spc_f_0
	};


	//comprehensive, without feature sharing:
	static final SinglePosContext[] baseContexts1 = {
		// full alphabet:
		spc_f_m2m10, spc_f_m101, spc_f_012,
	};


	//comprehensive, with feature sharing:
	static final SinglePosContext[] baseContexts2 = {
		// trivial alphabet:
//		empty_spc, spc_t_m2m1, spc_t_12,
		// reduced alphabet:
//		spc_r_m1, spc_r_0, spc_r_1,
//		spc_r_m10, spc_r_m11, spc_r_01,
//		spc_r_m2m10, spc_r_m101, spc_r_012,
		// full alphabet:
//		spc_f_m1, spc_f_1,
		spc_f_0, 
		spc_f_m10, spc_f_m11, spc_f_01,
		spc_f_m2m10, spc_f_m101, spc_f_012,
	};


	static final SinglePosContext[][] baseContexts = {
		baseContexts0,
		baseContexts1,
		baseContexts2
	};


	static final DoublePosContext[] edgeContextsUltraTrivial = {
		// full alphabet:
		empty_dpc,
	};

	static final DoublePosContext[] edgeContextsTrivial = {
		// full alphabet:
		dpc_f_0_0,
	};

	//basic, without feature sharing:
	static final DoublePosContext[] edgeContexts0 = {
		// full alphabet:
		dpc_f_01_m10,
	};

	//comprehensive, without feature sharing:
	static final DoublePosContext[] edgeContexts1 = {

		// full alphabet:
		dpc_f_0_m2m10, dpc_f_0_m101, dpc_f_0_012,

		dpc_f_m10_01, dpc_f_01_m10, dpc_f_m11_m11,

		dpc_f_m2m10_0, dpc_f_m101_0, dpc_f_012_0,
	};

	//comprehensive, with feature sharing:
	static final DoublePosContext[] edgeContexts2 = {
		// trivial alphabet:
		empty_dpc, dpc_t_m2m1_12, dpc_t_12_m1m2,

		// reduced alphabet:
		dpc_r_0_0,
		dpc_r_m1_01, dpc_r_0_m10, dpc_r_0_01, dpc_r_0_m11, dpc_r_1_m10,
		dpc_r_0_m2m10, dpc_r_0_m101, dpc_r_0_m101,

		dpc_r_m10_1, dpc_r_m10_0, dpc_r_m11_0, dpc_r_01_m1, dpc_r_01_0,
		dpc_r_m10_01, dpc_r_01_m10, dpc_r_m11_m11,

		dpc_r_m2m10_0, dpc_r_m101_0, dpc_r_012_0,

		// full alphabet:
		dpc_f_0_0,
		dpc_f_m1_01, dpc_f_0_m10, dpc_f_0_01, dpc_f_0_m11, dpc_f_1_m10,
		dpc_f_0_m2m10, dpc_f_0_m101, dpc_f_0_012,

		dpc_f_m10_1, dpc_f_m10_0, dpc_f_m11_0, dpc_f_01_m1, dpc_f_01_0,
		dpc_f_m10_01, dpc_f_01_m10, dpc_f_m11_m11,

		dpc_f_m2m10_0, dpc_f_m101_0, dpc_f_012_0,
	};

	//comprehensive, without feature sharing:
	static final DoublePosContext[] edgeContexts3 = {

		// full alphabet:
		dpc_f_0_m2m10, dpc_f_0_012,		

		dpc_f_m2m10_0, dpc_f_012_0,

		dpc_f_m101_m101, dpc_f_012_m2m10,

	};

	static final DoublePosContext[] edgeContexts4 = {
		// trivial alphabet:
		empty_dpc, dpc_t_m2m1_12, dpc_t_12_m1m2,

		// reduced alphabet:
		dpc_r_0_0,
		dpc_r_m1_01, dpc_r_0_m10, dpc_r_0_01, dpc_r_0_m11, dpc_r_1_m10,
		dpc_r_0_m2m10, dpc_r_0_m101, dpc_r_0_m101,

		dpc_r_m10_1, dpc_r_m10_0, dpc_r_m11_0, dpc_r_01_m1, dpc_r_01_0,
		dpc_r_m10_01, dpc_r_01_m10, dpc_r_m11_m11,
		dpc_r_m10_m101, dpc_r_01_m101, dpc_r_m11_m101,

		dpc_r_m2m10_0, dpc_r_m101_0, dpc_r_012_0,
		dpc_r_m101_01, dpc_r_m101_m10, dpc_r_m101_m11, dpc_r_012_m10,
		dpc_r_m101_m101, dpc_r_012_m2m10,


		// full alphabet:
		dpc_f_0_0,
		dpc_f_m1_01, dpc_f_0_m10, dpc_f_0_01, dpc_f_0_m11, dpc_f_1_m10,
		dpc_f_0_m2m10, dpc_f_0_m101, dpc_f_0_012,

		dpc_f_m10_1, dpc_f_m10_0, dpc_f_m11_0, dpc_f_01_m1, dpc_f_01_0,
		dpc_f_m10_01, dpc_f_01_m10, dpc_f_m11_m11,
		dpc_f_m10_m101, dpc_f_01_m101, dpc_f_m11_m101,

		dpc_f_m2m10_0, dpc_f_m101_0, dpc_f_012_0,
		dpc_f_m101_01, dpc_f_m101_m10, dpc_f_m101_m11, dpc_f_012_m10,
		dpc_f_m101_m101, dpc_f_012_m2m10,
	};


	static final DoublePosContext[] edgeContexts5 = {
//		// trivial alphabet:
//		empty_dpc, dpc_t_m2m1_12, dpc_t_12_m1m2,


		// full alphabet:
		dpc_f_0_0,
		
		dpc_f_m1_01, dpc_f_0_m10, dpc_f_0_01, dpc_f_0_m11, dpc_f_1_m10,

		dpc_f_0_m2m10, dpc_f_0_m101, dpc_f_0_012,

		dpc_f_m10_1, dpc_f_m10_0, dpc_f_m11_0, dpc_f_01_m1, dpc_f_01_0,
		dpc_f_m10_01, dpc_f_01_m10, dpc_f_m11_m11,
		dpc_f_m10_m101, dpc_f_01_m101, dpc_f_m11_m101,

		dpc_f_m2m10_0, dpc_f_m101_0, dpc_f_012_0,
		dpc_f_m101_01, dpc_f_m101_m10, dpc_f_m101_m11, dpc_f_012_m10,
		dpc_f_m101_m101, dpc_f_012_m2m10,
	};
	
	static final DoublePosContext[][] edgeContexts = {
		edgeContexts0,
		edgeContexts1,
		edgeContexts2,
	};


	/********************* Length value extractors *************************************/

	static final int lengthTemplateCardinality = 100;
	static final FeatureExtractor length1FeatureExtractor 	= new BinnedLengthFeatureExtractor(1, lengthTemplateCardinality, false);
	static final FeatureExtractor length2FeatureExtractor 	= new BinnedLengthFeatureExtractor(2, lengthTemplateCardinality, false);
	static final FeatureExtractor length2OddFeatureExtractor	= new BinnedLengthFeatureExtractor(2, lengthTemplateCardinality, true);
	static final FeatureExtractor length4FeatureExtractor 	= new BinnedLengthFeatureExtractor(4, lengthTemplateCardinality, false);
	static final FeatureExtractor length4OddFeatureExtractor	= new BinnedLengthFeatureExtractor(4, lengthTemplateCardinality, true);
	static final FeatureExtractor length8FeatureExtractor 	= new BinnedLengthFeatureExtractor(8, lengthTemplateCardinality, false);
	static final FeatureExtractor length8OddFeatureExtractor	= new BinnedLengthFeatureExtractor(8, lengthTemplateCardinality, true);
	static final FeatureExtractor length16FeatureExtractor 	= new BinnedLengthFeatureExtractor(16, lengthTemplateCardinality, false);
	static final FeatureExtractor length16OddFeatureExtractor= new BinnedLengthFeatureExtractor(16, lengthTemplateCardinality, true);

	static final FeatureExtractor[] lengthCalculators = {length1FeatureExtractor,
		length2FeatureExtractor, length2OddFeatureExtractor,
		length4FeatureExtractor, length4OddFeatureExtractor,
		length8FeatureExtractor, length8OddFeatureExtractor,
		length16FeatureExtractor, length16OddFeatureExtractor,
	};
	protected static final int SHORT = 0;
	protected static final int LONG = 1;



	public FeatureManager setFeatures(){
		bindFeatures();
		featureManager.finalize();
		return featureManager;
	}

	protected abstract void bindFeatures();

	@SuppressWarnings({ "unchecked" })
	protected void bindIlTypesToTemplates(Collection<FeatureTemplate> templates){
		for (int shortLength = 0; shortLength <= csr.IL_MAX_IX; ++shortLength){
			for (int longLength = shortLength; longLength <= csr.IL_MEDIUM[shortLength]; ++longLength){
				featureManager.bindElementTypeToFeatureTemplates(
						csr.IL_CLOSE[shortLength][longLength], 
						templates);
				featureManager.bindElementTypeToFeatureTemplates(
						csr.IL_OPEN[shortLength][longLength], 
						templates);
				
				if (shortLength < longLength){
					featureManager.bindElementTypeToFeatureTemplates(
							csr.IL_CLOSE[longLength][shortLength], 
							templates);
					featureManager.bindElementTypeToFeatureTemplates(
							csr.IL_OPEN[longLength][shortLength], 
							templates);
				}
			}
		}
	}

	@SuppressWarnings({ "unchecked" })
	protected void bindHpTypesToTemplates(Collection<FeatureTemplate> templates){
		for (int i=0; i<csr.HP_CLOSE.length; ++i){
			int elementType = csr.HP_CLOSE[i];
			if (elementType >= 0){
				featureManager.bindElementTypeToFeatureTemplates(
						elementType, templates);
			}
		}
	}

	@SuppressWarnings("unchecked")
	protected void addAndBindLengthTemplates(int... lengthTypes){
		String templateName = "";
		for (int lengthType : lengthTypes){
			templateName += csr.getElementName(lengthType)+"_";
		}
		Collection<FeatureTemplate> lengthTemplates =  
			featureManager.addTemplates(templateName, lengthCalculators);
		lengthTemplates.add(featureManager.addTemplate(templateName + "Log", 
				ValueCalculator.LOG_VALUE_CALCULATOR));
		for (int lengthType : lengthTypes){
			featureManager.bindElementTypeToFeatureTemplates(lengthType, lengthTemplates);
		}
	}

	protected Collection<FeatureTemplate> addTemplates(String templatePrefix, ContextFeatureExtractor[] contexts, int... types) {
		Collection<FeatureTemplate> templates = featureManager.addTemplates(templatePrefix, contexts);
		for (int type : types){
			featureManager.bindFeatureTemplateToElementTypes(templates,	type);
		}
		return templates;
	}

	protected void bindTemplates(Collection<FeatureTemplate> templates, int... types) {
		for (int type : types){
			featureManager.bindFeatureTemplateToElementTypes(templates, type);
		}
	}

}
