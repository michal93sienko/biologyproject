/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.models;

import java.util.Collection;

import contextFold.features.DoublePosContext;
import contextFold.features.FeatureTemplate;
import contextFold.features.SinglePosContext;


public class StHighCoHigh extends FeatureSetter{

	@SuppressWarnings("static-access")
	@Override
	protected void bindFeatures() {
		SinglePosContext[] modelBaseContexts = baseContexts1;
		DoublePosContext[] modelEdgeContexts = edgeContexts1;


		// hp features:
		addTemplates("HP_BASE", modelBaseContexts, csr.HP_BASE);
		for (int hpLength = csr.MIN_HAIRPIN_LENGTH; hpLength <= csr.HP_MAX_IX; ++hpLength){
			addTemplates("HP_CLOSE_"+hpLength, modelEdgeContexts, csr.HP_CLOSE[hpLength]);
		}

		// il features:
		Collection<FeatureTemplate> il_atLeast_1_1_CLOSE_Templates	= 
			featureManager.addTemplates("IL_AT_LEAST_1_1_CLOSE_", modelEdgeContexts);
		Collection<FeatureTemplate> il_atLeast_1_1_OPEN_Templates	= 
			featureManager.addTemplates("IL_AT_LEAST_1_1_OPEN", modelEdgeContexts);

		String shortStr, longStr;
		for (int shortLength = 0; shortLength <= csr.IL_MAX_IX; ++shortLength){
			if (shortLength == csr.IL_MAX_IX) shortStr = "LONG";
			else shortStr = "" + shortLength;

			addTemplates("IL_BASE"+shortStr+"_SHORT", modelBaseContexts, csr.IL_BASE[shortLength][SHORT]);
			addTemplates("IL_BASE"+shortStr+"_LONG", modelBaseContexts, csr.IL_BASE[shortLength][LONG]);

			for (int longLength = shortLength; longLength <= csr.IL_MEDIUM[shortLength]; ++longLength){
				if (longLength == csr.IL_MEDIUM[shortLength]) longStr = "LONG";
				else longStr = "" + longLength;

				addTemplates("IL_CLOSE_"+shortStr+"_"+ longStr, modelEdgeContexts, 
						csr.IL_CLOSE[shortLength][longLength]);
				addTemplates("IL_OPEN_"+shortStr+"_"+ longStr, modelEdgeContexts, 
						csr.IL_OPEN[shortLength][longLength]);

				if (shortLength < longLength){
					addTemplates("IL_CLOSE_"+longStr+"_"+shortStr, modelEdgeContexts, 
							csr.IL_CLOSE[longLength][shortLength]);
					addTemplates("IL_OPEN_"+longStr+"_"+ shortStr, modelEdgeContexts, 
							csr.IL_OPEN[longLength][shortLength]);
				}
				
				if (shortLength >= 1){
					featureManager.bindFeatureTemplateToElementTypes(il_atLeast_1_1_CLOSE_Templates,
							csr.IL_CLOSE[shortLength][longLength]);
					featureManager.bindFeatureTemplateToElementTypes(il_atLeast_1_1_OPEN_Templates,
							csr.IL_OPEN[shortLength][longLength]);
					if (shortLength < longLength){
						featureManager.bindFeatureTemplateToElementTypes(il_atLeast_1_1_CLOSE_Templates,
								csr.IL_CLOSE[longLength][shortLength]);
						featureManager.bindFeatureTemplateToElementTypes(il_atLeast_1_1_OPEN_Templates,
								csr.IL_OPEN[longLength][shortLength]);
					}
				}
			}
		}
		
		// ml templates:
		addTemplates("ML_BASE", modelBaseContexts, csr.ML_BASE);
		addTemplates("ML_CLOSE", modelEdgeContexts, csr.ML_CLOSE);
		addTemplates("ML_OPEN", modelEdgeContexts, csr.ML_OPEN);

		
		// xl templates:
		addTemplates("XI_BASE_5", modelBaseContexts, csr.XI_BASE_5);
		addTemplates("XI_BASE_3", modelBaseContexts, csr.XI_BASE_3);
		addTemplates("XI_BASE_MID", modelBaseContexts, csr.XI_BASE_MID);
		addTemplates("XL_OPEN", modelEdgeContexts, csr.XL_OPEN);

		
		// length features:
		addAndBindLengthTemplates(csr.HP_LENGTH);
		for (int shortLength = 0; shortLength < csr.IL_MAX_IX; ++shortLength){
			addAndBindLengthTemplates(csr.IL_MEDIUM_LENGTH[shortLength]);
		}

		addAndBindLengthTemplates(csr.XI_LENGTH_3, csr.XI_LENGTH_5, csr.XI_LENGTH_MID);
		addAndBindLengthTemplates(csr.XI_LENGTH_3, csr.XI_LENGTH_5);
		addAndBindLengthTemplates(csr.XI_LENGTH_3);
		addAndBindLengthTemplates(csr.XI_LENGTH_5);

	}

}
