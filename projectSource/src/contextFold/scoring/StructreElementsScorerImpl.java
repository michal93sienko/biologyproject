/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.scoring;


import contextFold.features.FeatureManager;
import contextFold.rna.RNA;

public class StructreElementsScorerImpl implements StructreElementsScorer {

	private int[][][] features;   
	private int[][][] scores;
	private int seqLength;
	
	//DEBUG!!!
	@SuppressWarnings("unused")
	private FeatureManager fm;
	private RNA rna;

	
	public StructreElementsScorerImpl(int[][][] features,
			int[][][] scores,
			int seqLength) {
		super();
		this.features = features;
		this.scores = scores;
		this.seqLength = seqLength;
	}

	public int elementScore(int type, int val) {
		int score = 0;
		int[][] typeScores = scores[type];
		int[][] typeFeatures = features[type];
		for (int i=0; i<typeFeatures.length; ++i){
			score += typeScores[i][typeFeatures[i][val]];
		}
		return score;
	}


	public int sequenceLength() {
		return seqLength;
	}
	
	public int[][][] getScores(){return scores;}
	public int[][][] getFeatures(){return features;}

	public void setManagerAndRNA(FeatureManager manager, RNA rna){
		this.fm = manager;
		this.rna = rna;
		
	}

	@Override
	public RNA getRna() {
		return rna;
	}

}
