/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.scoring;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import contextFold.rna.OneLineReader;
import contextFold.rna.RNA;
import contextFold.rna.RNAListReader;


public class PredictionQuality implements Comparable<PredictionQuality>{
	private int seqLength;
	private int goldBasepairNum;
	private int guessBasepairNum;
	private int truePositiveNum;
	private float betaSqr;

	public PredictionQuality(int[] guessFold, int[] goldFold){
		this(guessFold, goldFold, 1);
	}

	public PredictionQuality(int[] guessFold, int[] goldFold, float beta){
		assert guessFold.length == goldFold.length;
		seqLength = guessFold.length;
		goldBasepairNum = 0;
		guessBasepairNum = 0;
		truePositiveNum = 0;
		betaSqr = beta*beta;

		for (int i=0; i<seqLength; ++i){
			if (guessFold[i]>i) ++guessBasepairNum;
			if (goldFold[i]>i) {
				++goldBasepairNum;
				if (guessFold[i] == goldFold[i]) ++truePositiveNum;
			}
		}
	}

	public int getSeqLength() {
		return seqLength;
	}

	public int getGoldBasepairNum() {
		return goldBasepairNum;
	}

	public int getGuessBasepairNum() {
		return guessBasepairNum;
	}

	public int getTruePositiveNum() {
		return truePositiveNum;
	}

	public float sensitivity(){
		float val = ((float) truePositiveNum)/goldBasepairNum;
		if (Float.isNaN(val)) return 1;
		return val;
	}

	public float PPV(){
		float val = ((float) truePositiveNum)/guessBasepairNum;
		if (Float.isNaN(val)) return 1;
		return val;
	}

	public float fMeasure(){
		float val = (1+betaSqr)*truePositiveNum/(betaSqr*goldBasepairNum + guessBasepairNum);
		if (Float.isNaN(val)) return 1;
		return val;
	}
	
	

	public int compareTo(PredictionQuality other) {
		return seqLength - other.seqLength;
	}

	public static void predictionStatistics(String predictionFilePath) throws IOException{
		predictionStatistics(predictionFilePath, 100);
	}

	public static void predictionStatistics(String predictionFilePath, int binSize) throws IOException{
		predictionStatistics(predictionFilePath, binSize, true);
	}

	public static void predictionStatistics(String predictionFilePath, int binSize, boolean verbose) throws IOException{
//		RNAFileReader fr = new RNAFileReader(new File(predictionFilePath), RNA.ROUND);
//		List<RNA> rnas = fr.readAll();
		RNAListReader reader = new OneLineReader();
		List<RNA> rnas = reader.readList(new File(predictionFilePath));
		predictionStatistics(rnas, binSize, verbose);

	}

	public static void predictionStatistics(List<RNA> rnas, int binSize) throws IOException {
		predictionStatistics(rnas, binSize, true);
	}

	public static void predictionStatistics(List<RNA> rnas, int binSize, boolean verbose) throws IOException {
		final Map<Entry<String, String>, Float> annotationFMeasure = new HashMap<Entry<String, String>, Float>();
		Map<Entry<String, String>, Integer> annotationCounts = new HashMap<Entry<String, String>, Integer>();
		
		int maxLength = 0;
		for(RNA rna : rnas){
			maxLength = Math.max(maxLength, rna.length);
		}

		int binNum = (int) Math.ceil(((float) maxLength)/binSize);

		float[] binGuessBasepairsNum = new float[binNum];
		float[] binGoldBasepairsNum = new float[binNum];
		float[] binTruePositiveNum = new float[binNum];

		float[] binAverageSens = new float[binNum];
		float[] binAverageSpec = new float[binNum];
		float[] binAverageF = new float[binNum];

		int[] binCount = new int[binNum];

		float totalGuessBasepairsNum = 0;
		float totalGoldBasepairsNum = 0;
		float totalTruePositiveNum = 0;

		float avrageSensitivity = 0;
		float avragePPV = 0;
		float avrageFMeasure = 0;


		for (RNA rna : rnas){
			PredictionQuality prediction = new PredictionQuality(rna.lastPrediction(), rna.goldFold());

			for (Entry<String, String> annotationEntry : rna.annotationsMap.entrySet()){
				if (annotationEntry.getKey().equals("SSTRAND_ID") ||
						annotationEntry.getKey().equals("EXT_ID") || 
						annotationEntry.getKey().equals("ORGANISM") ) continue;
				float annotationF;
				Integer annotationCount = annotationCounts.get(annotationEntry);
				if (annotationCount == null){
					annotationCount = 0;
					annotationF = 0;
				}
				else{
					annotationF = annotationFMeasure.get(annotationEntry);
				}
				annotationCounts.put(annotationEntry, annotationCount+1);
				annotationFMeasure.put(annotationEntry, annotationF+prediction.fMeasure());
			}

			int bin = (prediction.seqLength-1)/binSize;
			binGuessBasepairsNum[bin] += prediction.guessBasepairNum;
			binGoldBasepairsNum[bin] += prediction.goldBasepairNum;
			binTruePositiveNum[bin] += prediction.truePositiveNum;

			binAverageSens[bin] += prediction.sensitivity();
			binAverageSpec[bin] += prediction.PPV();
			binAverageF[bin] += prediction.fMeasure();

			++binCount[bin];


			totalGuessBasepairsNum += prediction.guessBasepairNum;
			totalGoldBasepairsNum += prediction.goldBasepairNum;
			totalTruePositiveNum += prediction.truePositiveNum;

			avrageSensitivity += prediction.sensitivity();
			avragePPV += prediction.PPV();
			avrageFMeasure += prediction.fMeasure();
		}

		float globalSensitivity = totalTruePositiveNum/totalGoldBasepairsNum;
		float globalPPV = totalTruePositiveNum/totalGuessBasepairsNum;
		float globalFMeasure = 2*(totalTruePositiveNum)/(totalGoldBasepairsNum + totalGuessBasepairsNum);

		String statistics = "";
		int length = rnas.size();
		if (verbose){
			statistics += RNA.RNAListStatistics(rnas)+"\n";

			statistics += "Average sensitivity: " + avrageSensitivity/length;
			statistics += "\nAverage PPV: " + avragePPV/length +"\n";
		}
		statistics += "Average f-measure: " + avrageFMeasure/length;

		if (verbose){
			statistics += "\nGlobal sensitivity: " + globalSensitivity;
			statistics += "\nGlobal PPV: " + globalPPV;
			statistics += "\nGlobal f-measure: " + globalFMeasure;

			statistics += "\n\nAnnotations f-measures:";

			List<Entry<String, String>> sortedAnnotations = new ArrayList<Entry<String, String>>(annotationFMeasure.keySet());
			for (Entry<String, String> annotation : sortedAnnotations){
				annotationFMeasure.put(annotation, annotationFMeasure.get(annotation)/annotationCounts.get(annotation));
			}

			Collections.sort(sortedAnnotations, new Comparator<Entry<String, String>>(){
				public int compare(Entry<String, String> entry0, Entry<String, String> entry1) {
					int compareVal = entry0.getKey().compareTo(entry1.getKey());
					if (compareVal == 0){
						compareVal = annotationFMeasure.get(entry0).compareTo(annotationFMeasure.get(entry1));
					}
					return compareVal;
				}
			});

			for (Entry<String, String> entry : sortedAnnotations){
				statistics += "\n" + entry.getKey()+ "=" + entry.getValue() + 
				"(" +annotationCounts.get(entry)+ "): " +
				annotationFMeasure.get(entry);
			}

			statistics += "\n\nHistogram: (range, samples, ave sens, ave PPV, ave f, glob sens, glob PPV, glob f)";
			for (int bin=0; bin<binNum; ++bin){
				statistics += "\n"+(bin*binSize+1)+"-"+((bin+1)*binSize)+"\t"+binCount[bin];
				if (binCount[bin]==0) continue;

				statistics += "\t" + binAverageSens[bin]/binCount[bin];
				statistics += "\t " + binAverageSpec[bin]/binCount[bin];
				statistics += "\t " + binAverageF[bin]/binCount[bin];

				statistics += "\t" + binTruePositiveNum[bin]/binGoldBasepairsNum[bin];
				statistics += "\t" + binTruePositiveNum[bin]/binGuessBasepairsNum[bin];
				statistics += "\t" + 2*binTruePositiveNum[bin]/(binGuessBasepairsNum[bin]+binGoldBasepairsNum[bin]);
			}
		}
		
		System.out.println(statistics);
	}

	public static void main(String[] args) throws IOException{
		predictionStatistics(args[0], 100);
	}


}
