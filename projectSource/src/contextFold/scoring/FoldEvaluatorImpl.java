/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.scoring;

import java.util.Collection;

import contextFold.features.AbstractFeatureVector;
import contextFold.features.FeatureManager;
import contextFold.features.SparseFeatureVector;
import contextFold.rna.RNA;
import contextFold.rnaFolding.StructureRecognizer;





public class FoldEvaluatorImpl implements FoldEvaluator{

	static final float ADD_AMOUNT=1; // how much to add for each missing pair feature
	static final float SUB_AMOUNT=1; // how much to subtract for each extra pair feature

	private StructureRecognizer structRecognizer;
	private FeatureManager featureManager;

	public FoldEvaluatorImpl(FeatureManager featureManager) {
		this.structRecognizer = featureManager.sr;
		this.featureManager = featureManager;
	}

//	public float loss(int[] guessFold, int[] goldFold) {
//		int loss = 0;
//		int j=0;
//		for (int i=0;i<guessFold.length;i++) {
//			j++;
//			if (guessFold[i]!=goldFold[i]) {
//				loss+=ADD_AMOUNT+SUB_AMOUNT;
//			}
//		}
//		return loss;
////		return new PredictionQuality(guessFold, goldFold).fMeasure();
//	}

	public float scoreFold(RNA rna, int[] fold) {
//		if (contextFold.common.Env.DEBUG_MODE) return scoreFoldDebug(contextFold.rna, fold);

		float score=0;
		Collection<int[]> elements = this.structRecognizer.getCombinedElements(fold);
		StructreElementsScorer elementScorer = featureManager.getElementScorer(rna);

		for (int[] element : elements) {
			score += elementScorer.elementScore(element[0], element[1]);
		}
		return score;
	}

	public void differenceFeatureVector(int[] guessFold, int[] goldFold, AbstractFeatureVector diffVector, RNA rna) {

		Collection<int[]> guessElements = structRecognizer.getCombinedElements(guessFold);
		Collection<int[]> goldElements = structRecognizer.getCombinedElements(goldFold);


		for (int[] element : goldElements) {
			diffVector.add(featureManager.getElementFeaturs(rna, element[0], element[1]),ADD_AMOUNT);
		}

		for (int[] element : guessElements) {
			diffVector.sub(featureManager.getElementFeaturs(rna, element[0], element[1]),SUB_AMOUNT);
		}
	}


	// A debug version
//	public float scoreFoldDebug(RNA contextFold.rna, int[] fold) {
//		int[] tmpFold = contextFold.rna.fold;
//		if (contextFold.common.Env.ECHO){
//			contextFold.rna.fold = fold;
//			System.out.println();
//			for (int i=0; i<contextFold.rna.length; ++i) System.out.print(i%10); 
//			System.out.println("\n"+contextFold.rna.toString());
//			contextFold.rna.fold = tmpFold;
//		}
//
//		float score = 0;
//		Collection<int[]> elements = this.structRecognizer.getCombinedElements(fold);
//		StructreElementsScorer elementScorer = featureManager.getElementScorer(contextFold.rna);
//
//		for (int[] element : elements) {
//			AbstractFeatureVector elementFeaturs = 
//				featureManager.getElementWeightedFeaturs(contextFold.rna, element[0], element[1], element[2]);
//			//			System.out.println(featureManager.featuresToString(elementFeaturs));
//			float score1 = elementFeaturs.sum();
//			float score2 = elementScorer.elementScore((short)(element[0]), (short)(element[1]), (short)(element[2]));
//			if (contextFold.common.Env.VERBOSE && (score1 != 0 || score2 != 0)) {
//				System.out.println(featureManager.elementToString(element) + ", score1: " + score1 + ", score2: " + score2);
//				//				System.out.println(Arrays.toString(element) + ", score: " + score1);
//			}
//			assert contextFold.common.Utils.floatEquals(score1,score2);
//			score += score2;
//		}
//		if (contextFold.common.Env.VERBOSE) System.out.println("Total score:" + score);
//		return score;
//	}

	public AbstractFeatureVector differenceFeatureVector(int[] guessFold,
			int[] goldFold, RNA rna) {
		AbstractFeatureVector diffVector = new SparseFeatureVector();
		differenceFeatureVector(guessFold, goldFold, diffVector, rna);
		return diffVector;
	}

}
