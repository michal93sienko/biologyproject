/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.scoring;

import contextFold.features.AbstractFeatureVector;
import contextFold.rna.RNA;


public interface FoldEvaluator {


	/**
	 * Computes the score of a given sequence-folding pair.
	 * 
	 * @param rna an RNA sequence.
	 * @param fold a folding of the sequence.
	 * @return the score of a given sequence-folding pair.
	 */
	public float scoreFold(RNA rna, int[] fold); 

	/**
	 * Computes the feature difference between two foldings.
	 * 
	 * @param guessFold a predicted folding of the RNA sequence.
	 * @param goldFold a known folding of the RNA sequence.
	 * @param result a feature vector, which feature weights correspond to feature differences between the two foldings.
	 * Negatively weighted features are those that appeared more in the predicted folding, positively weighted features
	 * are those that appeared more in the known folding, and zero weighted feature appeared the same in both foldings.  
	 * @param rna an RNA sequence
	 */
	public void differenceFeatureVector(int[] guessFold, int[] goldFold, AbstractFeatureVector result, RNA rna);

	public AbstractFeatureVector differenceFeatureVector(int[] guessFold, int[] goldFold,
			RNA rna);
	
//	public void setElementScorer(StructreElementsScorer scorer);
	

}
