/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.rna;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Writer;
import java.util.List;

public class RNAWriter {
	BufferedWriter out;

	public RNAWriter(File f) throws IOException {
		out = new BufferedWriter(new FileWriter(f));
	}
	
	public RNAWriter(final PrintStream stream) throws IOException {
		Writer writer = new Writer() {
			
			@Override
			public void write(char[] arg0, int arg1, int arg2) throws IOException {
				stream.append(new String(arg0), arg1, arg2);
			}
			
			@Override
			public void flush() throws IOException {
				stream.flush();
			}
			
			@Override
			public void close() throws IOException {
				stream.close();
			}
		};
		out = new BufferedWriter(writer);
	}


	public void writeAll(List<RNA> rnas) {
		try {
			for (RNA rna : rnas) {
				out.write(rna.toString());
				out.newLine();
				out.newLine();
			}
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void write(RNA rna) {
		try {
			out.write(rna.toString());
			out.newLine();
			out.newLine();
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void write(int[] fold) {
		try {
			out.write(RNA.foldToBracets(fold));
			out.write("\n");
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void close() {
		try {
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
