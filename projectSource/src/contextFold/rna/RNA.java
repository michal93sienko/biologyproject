/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.rna;

import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import contextFold.common.Env;
import contextFold.rnaFolding.StructureRecognizer;


public class RNA {

	private static Random random = new Random(0);

	public static char annotationLineStart = '>';
	public static String annotationSplit = ";"; 


	public static final byte A = 0;
	public static final byte C = 1;
	public static final byte G = 2;
	public static final byte U = 3;
	public static final byte T = U;

	public static final int ROUND = 0;
	public static final int SHARP = 1;

	public static final int OPEN = 0;
	public static final int CLOSE = 1;
	public static final int UN_PAIRED = 2;
	public static final int PS_OPEN = 3;
	public static final int PS_CLOSE = 4;

	public static final char[][] structFormat = {
		{'(', ')', '.', '[', ']', '{', '}', '<', '>'}, // ROUND
		{'>', '<', '.', '[', ']', '{', '}', '(', ')'}  // SHARP
	};

	private static final byte[] charToByte = new byte['z'+1];
	private static final char[] byteToChar = new char[4];
	private static final byte ILEGAL_CHARACTER = Byte.MIN_VALUE;

	public static final char[] ALL_RNA_CHARS = {'a', 'c', 'g', 'u'};


	static{
		Arrays.fill(charToByte, ILEGAL_CHARACTER);
		charToByte['A'] = A;
		charToByte['C'] = C;
		charToByte['G'] = G;
		charToByte['U'] = U;
		charToByte['T'] = T;

		charToByte['a'] = A;
		charToByte['c'] = C;
		charToByte['g'] = G;
		charToByte['u'] = U;
		charToByte['t'] = T;

		byteToChar[A] = 'A';
		byteToChar[C] = 'C';
		byteToChar[G] = 'G';
		byteToChar[U] = 'U';
	}

	public static final String standardOpen = "(";
	public static final String standardClose = ")";
	public static final String pseudoOpen = "(<{\\[A-Z";
	public static final String pseudoClose = ")>}\\]a-z";

	public static final Pattern standardOpenPattern = Pattern.compile("[" + standardOpen + "]");
	public static final Pattern standardClosePattern = Pattern.compile("[" + standardClose + "]");
	public static final Pattern standardFoldPattern = Pattern.compile("[" +
			standardOpen + standardClose + "\\." + "]+");
	public static final Pattern pseudoOpenPattern = Pattern.compile("["+pseudoOpen+"]");
	public static final Pattern pseudoClosePattern = Pattern.compile("["+pseudoClose+"]");
	public static final Pattern pseudoFoldPattern = Pattern.compile("["+
			pseudoOpen + pseudoClose + "\\." + "]+");

	public static final Map<Character, Character> standardComplementary = 
		new HashMap<Character, Character>();
	public static final Map<Character, Character> close2open = 
		new HashMap<Character, Character>();
	public static final Map<Character, char[]> possibleReplacements =
		new HashMap<Character, char[]>();

		static{
			standardComplementary.put('a', 'u');
			standardComplementary.put('u', 'a');
			standardComplementary.put('c', 'g');
			standardComplementary.put('g', 'c');
			standardComplementary.put('t', 'a');

			standardComplementary.put('A', 'U');
			standardComplementary.put('U', 'A');
			standardComplementary.put('C', 'G');
			standardComplementary.put('G', 'C');
			standardComplementary.put('T', 'A');

			close2open.put(')', '(');
			close2open.put('>', '<');
			close2open.put('}', '{');
			close2open.put(']', '[');

			for (int i=0; i<'z'-'a'; ++i){
				close2open.put((char) ('a'+i), (char) ('A'+i));
			}

			possibleReplacements.put('w', new char[]{'a', 'u'});
			possibleReplacements.put('s', new char[]{'c', 'g'});
			possibleReplacements.put('m', new char[]{'a', 'c'});
			possibleReplacements.put('k', new char[]{'g', 'u'});
			possibleReplacements.put('r', new char[]{'a', 'g'});
			possibleReplacements.put('y', new char[]{'c', 't'});
			possibleReplacements.put('b', new char[]{'c', 'g', 'u'});
			possibleReplacements.put('d', new char[]{'a', 'g', 'u'});
			possibleReplacements.put('h', new char[]{'a', 'c', 'u'});
			possibleReplacements.put('v', new char[]{'a', 'c', 'g'});
		}

		static{
			List<Character> rnaChars = new ArrayList<Character>(possibleReplacements.keySet());
			for (int i=0; i<rnaChars.size(); ++i){
				Character ch = rnaChars.get(i);
				possibleReplacements.put((char) (ch-'a'+'A'), possibleReplacements.get(ch));
			}

			for (Entry<Character, char[]> entry : possibleReplacements.entrySet()){
				byte[] replacements = new byte[entry.getValue().length];
				for (int i=0; i<replacements.length; ++i){
					replacements[i] = charToByte[entry.getValue()[i]];
				}
			}

		}

		public static final String STANDARD_RNA_CHARS = "acgutACGUT";
		public static final Pattern RNAStrPattern = Pattern.compile("["+STANDARD_RNA_CHARS+"]+");
		public static final Pattern nonRNACharPattern = Pattern.compile("[^"+STANDARD_RNA_CHARS+"]");

		public static final String annotationRegexp = "[>#][^\\n\\r]*[\\n\\r]";
		public static final Pattern annotationPattern = Pattern.compile("("+annotationRegexp+")+");


		public byte[] seq;
		public String seqString = null;

		public List<int[]> foldings; // don't change from outside..
		public int[] predicted=null;

		public final int length;  
		public String annotations;
		public Map<String, String> annotationsMap;

		public RNA(String seq, String annotations) {
			this(charToByte(seq.toCharArray()), annotations);
			this.seqString = seq;
		}

		public RNA(String input) {
			this(getSequence(input), getAnnotation(input));
		}

		/**
		 * @param input
		 * @return
		 */
		private static String getAnnotation(String input) {
			String splited[] = input.split("\\s");
			StringBuilder annotations = new StringBuilder();
			for (int i=0; i<splited.length && isAnnotation(splited[i]); ++i){
				annotations.append(splited[i]).append("\n");
			}
			return annotations.toString();
		}

		/**
		 * @param input
		 * @return
		 */
		private static String getSequence(String input) {
			String splited[] = input.split("\\s");
			StringBuilder sb = new StringBuilder();
			int i=0;
			for (; i<splited.length && isAnnotation(splited[i]); ++i);
			for (; i<splited.length; ++i){
				sb.append(splited[i].trim());
			}
			return sb.toString();
		}

		/**
		 * @param string
		 * @return
		 */
		private static boolean isAnnotation(String string) {
			return annotationPattern.matcher(string).matches();
		}


		public static byte[] stringToByte(String seq) {
			return charToByte(seq.toCharArray());
		}

		public static byte[] charToByte(char[] charSeq) {
			byte[] byteSeq = new byte[charSeq.length];
			byte byteValue;
			for (int i=0; i<charSeq.length; ++i){
				try{
					byteValue = charToByte[charSeq[i]];
				} catch(ArrayIndexOutOfBoundsException e){
					byteValue = ILEGAL_CHARACTER;
				}
				
				if (byteValue == ILEGAL_CHARACTER){
					System.err.println("Ilegal character: " + charSeq[i]); 
				}
				byteSeq[i] = byteValue;
			}
			return byteSeq ;
		}

		public RNA(byte[] seq, String annotations) {
			this.seq = seq;
			this.length = seq.length;
			this.annotations = annotations;
			foldings = new ArrayList<int[]>();

			annotationsMap = new HashMap<String, String>();
			String[] splited = annotations.split("[\\n\\r" + annotationSplit + "]");
			for (String annotation : splited){
				String[] annotationParts = annotation.split("=");
				if (annotationParts.length > 1){
					annotationsMap.put(annotationParts[0].trim(), annotationParts[1].trim());
				}
			}

		}


		public void addFold(int[] fold) {
			assert(this.seq.length == fold.length);
			foldings.add(fold);
		}

		public void addFold(String foldSeq) {
			addFold(foldSeq, ROUND);
		}

		public void addFold(String foldStr, int format){
			addFold(getFold(foldStr, format));
		}

		/**
		 * Returns the index paring described by a dot-bracket string.
		 * Note that pseudoknots are currently ignored.
		 * 
		 * @param foldStr dot-bracket string.
		 * @param format round/sharp.
		 * @return an integer array {@code fold}, s.t. {@code fold[i] = j} implies
		 * that indices i and j are paired. 
		 */
		public static int[] getFold(String foldStr, int format) {
			foldStr = foldStr.trim();
			int[] fold = new int[foldStr.length()];
			Stack<Integer> st = new Stack<Integer>();

			char open = structFormat[format][OPEN];
			char close = structFormat[format][CLOSE];
			//		char unPaired = structFormat[format][UN_PAIRED];

			for (int j=0; j<foldStr.length(); ++j){
				char ch = foldStr.charAt(j);
				if (ch == open) st.push(j);
				else if (ch == close) {
					int i = st.pop();
					fold[i] = j;
					fold[j] = i;
				}
				else fold[j] = StructureRecognizer.UNPAIRED_BASE;
			}
			assert st.isEmpty();
			return fold;
		}

		public byte get(int i) {
			return this.seq[i];
		}


		public byte[] sequence() {
			return seq;
		}

		public String toString(){
			StringBuilder str = new StringBuilder();
			if (!annotations.isEmpty()) {
				str.append(annotations);
			}
			str.append(byteToStr(seq));
			for (int[] fold : foldings){
				str.append("\n").append(foldToBracets(fold, ROUND));
			}
			return str.toString();
		}



		public static String foldToBracets(int[] fold) {
			return foldToBracets(fold, ROUND);
		}

		public static String foldToBracets(int[] fold, int format) {
			if (fold == null) return "";
			StringBuilder str = new StringBuilder();
			for (int i=0; i<fold.length; ++i){
				if (fold[i] == StructureRecognizer.UNPAIRED_BASE)
					str.append(structFormat[format][UN_PAIRED]);
				else if (fold[i] < i) str.append(structFormat[format][CLOSE]);
				else str.append(structFormat[format][OPEN]);
			}
			return str.toString();
		}

		public static String byteToStr(byte[] seq) {
			String str = "";
			for (int i=0; i<seq.length; ++i){
				str += byteToChar[seq[i]];
			}
			return str;
		}

		public String getAnnotationVal(String annotation){
			return annotationsMap.get(annotation);
		}

		public int[] goldFold() {
			return getFoldNo(0);
		}

		public int[] getFoldNo(int i) {
			return foldings.get(i);
		}

		public int[] lastPrediction() {
			if (foldings.size() == 0) return null;
			else return foldings.get(foldings.size()-1);
		}


		public static void filter(List<RNA> rnas, String annotationFilter) {
			annotationFilter = annotationFilter.replace('_', ' ');
			List<RNA> toRemove = new ArrayList<RNA>();
			String[] annotationParts = annotationFilter.split("=");			
			for (RNA rna: rnas){
				if (!(annotationParts[1].equals(rna.annotationsMap.get(annotationParts[0])))){
					toRemove.add(rna);
				}
			}
			rnas.removeAll(toRemove);
		}

		public static void filter(List<RNA> rnas, int maxLength) {
			List<RNA> toRemove = new ArrayList<RNA>();
			for (RNA rna: rnas){
				if (rna.length>maxLength){
					toRemove.add(rna);
				}
			}
			rnas.removeAll(toRemove);
		}


		public static int[] getFold(String string) {
			return getFold(string, ROUND);
		}

		public String getStringFold() {
			return RNA.foldToBracets(this.lastPrediction());
		}

		public String getStringFold(int foldIx) {
			return RNA.foldToBracets(foldings.get(foldIx));
		}

		public String getStringSequence() {
			return this.seqString;
		}

		public static void setRandomSeed(long seed){
			random.setSeed(seed);
		}

		public static void setRandomSeed(){
			random = new Random();
		}

		public static RNA randomRNA(int length){
			return new RNA(randomRNAString(length));
		}

		public static String randomRNAString(int length){
			char[] rnaChars = new char[length];
			for (int i=0; i<length; ++i){
				rnaChars[i] = byteToChar[random.nextInt(4)];
			}
			return new String(rnaChars);
		}


		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
			+ ((foldings == null) ? 0 : foldings.hashCode());
			result = prime * result + Arrays.hashCode(seq);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			RNA other = (RNA) obj;
			if (foldings == null) {
				if (other.foldings != null)
					return false;
			} else if (!foldings.equals(other.foldings))
				return false;
			if (!Arrays.equals(seq, other.seq))
				return false;
			return true;
		}

		public static boolean isStandardRNAString(String str){
			return RNAStrPattern.matcher(str).matches();
		}

		/**
		 * Returns the index paring described by a dot-bracket string.
		 * 
		 * @param foldStr (extended) dot-bracket string.
		 * @param standardOpenPattern
		 * @param standardClosePattern
		 * @return an integer array {@code fold}, s.t. {@code fold[i] = j} implies
		 * that indices i and j are paired. 
		 */
		public static int[] getFold(String foldStr, Pattern openPattern, 
				Pattern closePattern) {
			final Map<Character, Stack<Integer>> stacks = 
				new HashMap<Character, Stack<Integer>>();
			foldStr = foldStr.trim();
			int[] fold = new int[foldStr.length()];
			Arrays.fill(fold, StructureRecognizer.UNPAIRED_BASE);

			for (int j=0; j<foldStr.length(); ++j){
				char ch = foldStr.charAt(j);
				if (openPattern.matcher(""+ch).find()){
					Stack<Integer> st = stacks.get(ch);
					if (st == null){
						st = new Stack<Integer>();
						stacks.put(ch, st);
					}
					st.push(j);
				}
				else if (closePattern.matcher(""+ch).find()) {
					Stack<Integer> st = stacks.get(close2open.get(ch));
					if (st == null){
						return null; //unbalanced structure
					}
					int i = st.pop();
					if (j-i-1 >= Env.MIN_HAIRPIN_LENGTH){
						fold[i] = j;
						fold[j] = i;
					}
				}
			}
			for (Stack<Integer> st : stacks.values()){
				if (!st.isEmpty()){
					return null; //unbalanced structure
				}
			}
			return fold;
		}

		public static int[] getPseudoFold(String struct) {
			return getFold(struct, pseudoOpenPattern, pseudoClosePattern);
		}

		public static int[] getNonCrossingFold(String struct) {
			return getFold(struct, standardOpenPattern, standardClosePattern);
		}

		/**
		 * Replacing ambiguous characters by RNA nucleotides,
		 * for the sake of efficient learning.
		 * Replacing ambiguous characters which are paired to a nucleotide 
		 * with the canonical complementary nucleotide, and replacing 
		 * ambiguous base-pairs and unpaired bases with random base-pairs.
		 *  
		 * @param seq the original sequence.
		 * @param foldStr the folding of the sequence.
		 * @return the standardized sequence.
		 */
		public static String standardized(String seq, String foldStr){
			char[] standardChars = seq.toCharArray();

			int[] fold = getPseudoFold(foldStr);
			Matcher matcher = nonRNACharPattern.matcher(seq);
			while (matcher.find()){
				int i = matcher.start();
				int j = fold[i];
				if (j==-1){
					//						unpaired character - choosing a random alternative.
					standardChars[i] = getPossibleRplacement(standardChars[i]); 
				}
				else {
					CharSequence pairedChar = seq.subSequence(j, j+1);
					if (RNAStrPattern.matcher(pairedChar).matches()){
						standardChars[i] = standardComplementary.get(standardChars[j]);
					}
					else {
						standardChars[j] = getPossibleRplacement(standardChars[j]); 
						standardChars[i] = standardComplementary.get(standardChars[j]);
					}
				}
			}
			String standardSeq = new String(standardChars);
			return standardSeq;
		}

		public static char getPossibleRplacement(char ch) {
			char[] possibleReplace = possibleReplacements.get(ch);
			if (possibleReplace ==  null) {
				possibleReplace = ALL_RNA_CHARS;
			}
			return possibleReplace[(int) (Math.random()*possibleReplace.length)];
		}

		public static boolean isPseudoFold(String foldStr) {
			if (pseudoFoldPattern.matcher(foldStr).matches()){
				if (foldStr.indexOf("(((")>=0 && foldStr.indexOf(")))")>=0){
					return true;
				}
			}
			return false;
		}

		public static String RNAListStatistics(List<RNA> rnas){
			double totalLength = 0;
			double totalLengthSqr = 0;
			int maxLength = 0;
			int minLength = Integer.MAX_VALUE;
			int n = rnas.size();

			for (RNA rna : rnas){
				int l = rna.length;
				totalLength += l;
				totalLengthSqr += l*l;
				if (l > maxLength) maxLength = l;
				if (l < minLength) minLength = l;
			}

			double averageLength = totalLength/n;
			double averageSqrLength = totalLengthSqr/n;
			double totalLengthStd = 0;
			double totalLengthSqrStd = 0;

			for (RNA rna : rnas){
				int l = rna.length;
				totalLengthStd += (l-averageLength)*(l-averageLength);
				totalLengthSqrStd += (l*l-averageSqrLength)*(l*l-averageSqrLength);
			}

			double lengthDev = Math.sqrt(totalLengthStd/n);
			double lengthSqrDev = Math.sqrt(totalLengthSqrStd/n);

			StringBuilder sb = new StringBuilder();

			sb.append("Number of sequences: ").append(rnas.size());
			sb.append(", max length: ").append(maxLength);
			sb.append(", min length: ").append(minLength).append("\n");
			sb.append("Average length: ").append(averageLength);
			sb.append(", length standard deviation: ").append(lengthDev).append("\n");
			sb.append("Average square length: ").append(averageSqrLength); 
			sb.append(", square length standard deviation: ").append(lengthSqrDev).append("\n");

			return sb.toString();

		}

		public static boolean isStandardRNAChar(char charAt) {
			return RNAStrPattern.matcher(""+charAt).matches();
		}
}
