package contextFold.rna;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public abstract class RNAListReader {
	public abstract List<RNA> readList(CharSequence list);

	public List<RNA> readList(File f) throws IOException{
		BufferedReader br = new BufferedReader(new FileReader(f));
		StringBuilder sb = new StringBuilder();
		String line;
		line = br.readLine();
		while (line != null){
			sb.append(line).append("\n");
			line = br.readLine();
		}
		return readList(sb);
	}

}
