package contextFold.rna;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RNAStrandReader extends RNAListReader {

	private static final String ANNOTATION_REGEXP = "((#[^\\n\\r]*[\\n\\r])+)";
	private static final String SEQ_STRUCT_REGEXP = "(([^\\n\\r]{50}[\\n\\r])*[^\\n\\r]{0,49}[\\n\\r])";

	//	private static final String SEQ_STRUCT_REGEXP = "([^([\\n\\r][\\n\\r])]*)[\\n\\r][\\n\\r]";
	private static final String ITEM_REGEXP = 
		ANNOTATION_REGEXP + "[\\n\\r]" + SEQ_STRUCT_REGEXP + SEQ_STRUCT_REGEXP;

	private static final Pattern ITEM_PATTERN = Pattern.compile(ITEM_REGEXP);

	private static final int ANNOTATION_GROUP = 1;
	private static final int SEQ_GROUP = 3;
	private static final int FOLD_GROUP = 5;

	@Override
	public List<RNA> readList(CharSequence list) {
		List<RNA> rnas = new ArrayList<RNA>();
		Matcher matcher = ITEM_PATTERN.matcher(list);
		String annotation, seq, foldStr;
		StringBuilder sb = new StringBuilder();

		while (matcher.find()){
			annotation = matcher.group(ANNOTATION_GROUP);
			seq = matcher.group(SEQ_GROUP);
			String[] seqStructLines = seq.split("\\s");
			for (String line : seqStructLines){
				sb.append(line.trim());
			}
			seq = sb.toString();
			sb.setLength(0);

			foldStr = matcher.group(FOLD_GROUP);
			String[] foldLines = foldStr.split("\\s");
			for (String line : foldLines){
				sb.append(line.trim());
			}
			foldStr = sb.toString();
			sb.setLength(0);

			if (seq.length() != foldStr.length()){
				continue; //invalid item
			}

			if (!RNA.isPseudoFold(foldStr)){
				continue; //invalid structure
			}

//			if (!RNA.isStandardRNAString(seq)){
//				//trying to correct nonstandard letters
//				seq = RNA.standardized(seq, foldStr);
//			}
			RNA rna = new RNA(seq, annotation);
			rna.addFold(RNA.getNonCrossingFold(foldStr));
			rnas.add(rna);
		}

		return rnas;
	}

}
