package contextFold.rna;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OneLineReader extends RNAListReader {
	public static final String extendedRNARegexp = "([a-zA-Z]*[acgutACGUT]+[a-zA-Z]*)+"; //bBkKmMnNrRsSwWxXyY
	public static final String FoldingRegexp = "([a-zA-Z\\[\\]\\{\\}<>]*[()\\.]+[a-zA-Z\\[\\]\\{\\}<>]*)+";

	public static final String RNALine_Regexp = "[ \\t]*(" + extendedRNARegexp+ ")[ \\t]*[\\n\\r]";
	public static final String FoldingLineRegexp = "[ \\t]*" + FoldingRegexp +"[ \\t]*[\\n\\r]";


	public static final String RNAItemRegexp = (
			"((" + RNA.annotationRegexp + ")*)" +
			"[\\n\\r]*"+
			"(" + RNALine_Regexp + ")" + 
			"((" + FoldingLineRegexp+ ")*)");

	public static final Pattern RNAItemPattern = Pattern.compile(RNAItemRegexp);
	public static final int ITEM_GROUP = 0;
	public static final int ANNOTATION_GROUP = 1;
	public static final int SEQUENCE_GROUP = 3;
	public static final int STRUCTURE_GROUP = 6;



	@Override
	public List<RNA> readList(CharSequence list){
		List<RNA> rnas = new ArrayList<RNA>();
		Matcher matcher = RNAItemPattern.matcher(list +"\n");
		//		StringBuilder sb  = new StringBuilder(); 
		while (matcher.find()){
			String annotations = matcher.group(ANNOTATION_GROUP);

			String sequence = matcher.group(SEQUENCE_GROUP).trim();

			if (!RNA.isStandardRNAString(sequence)){
				System.err.println("Skipping nonstandard RNA string:\n" +  
						sequence);
				continue;
			}

			RNA rna = new RNA(sequence, annotations);
			if (matcher.group(STRUCTURE_GROUP).length() > 0){
				String[] structureLines = matcher.group(STRUCTURE_GROUP).split("[\\n\\r]");
				for (String structureLine : structureLines){
					structureLine = structureLine.trim();
					if (structureLine.length() != rna.length){
						System.err.println("Skeeping structure. length of structure description doesn't fit" +
						" length of RNA string in the following item:");
						System.err.println(matcher.group(ITEM_GROUP));
						continue;
					}
					else{
						rna.addFold(structureLine);
					}
				}
			}
			rnas.add(rna);
		}

		return rnas;
	}


}
