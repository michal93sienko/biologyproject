package contextFold.rna;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import contextFold.common.CmdArgumentsHandler;

public class RandomSplit {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		String progName = RandomSplit.class.getSimpleName();
		String description = "Generates random train-test splits of a given dataset.";
		CmdArgumentsHandler argHandler = new CmdArgumentsHandler(progName, description, true);

		String inputFileArg = argHandler.addRequiredArg("in", "A Path to the RNA data file");
		String outputPathArg = argHandler.addArg("out", "A Path to the output files");
		String splitArg = argHandler.addArg("s", "0.8", "A spliting criteria");

		argHandler.processArgs(args);
		
		File inputFile = new File(argHandler.stringValue(inputFileArg));
		BufferedReader br = new BufferedReader(new FileReader(inputFile));
		StringBuilder sb = new StringBuilder();
		String line;
		line = br.readLine();
		while (line != null){
			sb.append(line).append("\n");
			line = br.readLine();
		}
		RNAStrandReader reader = new RNAStrandReader();
		List<RNA> rnas = reader.readList(sb);
		Collections.shuffle(rnas);
		
		double splitProp = argHandler.floatValue(splitArg);
		String outputPath = argHandler.stringValue(outputPathArg);
		if (CmdArgumentsHandler.noValue.equals(outputPath)){
			outputPath = inputFile.getParent();
		}
		outputPath += File.separator;
		
		int splitIx = (int) (splitProp*rnas.size());
		List<RNA> train = rnas.subList(0, splitIx);
		List<RNA> test = rnas.subList(splitIx, rnas.size());
		
		String baseFileName, baseFileExt; 
		String inputFileName = inputFile.getName();
		int extIndex = inputFileName.lastIndexOf('.');
		if (extIndex > 0){
			baseFileName = inputFileName.substring(0, extIndex);
			baseFileExt = inputFileName.substring(extIndex);
		}
		else{
			baseFileName = inputFileName;
			baseFileExt = "";
		}
		
		String splitTrainStr = "Train"+("" +splitProp).substring(0,4);
		File trainOutput = new File(outputPath+baseFileName+splitTrainStr+baseFileExt);
		RNAWriter rw = new RNAWriter(trainOutput);
		rw.writeAll(train);
		rw.close();

		String splitTestStr = "Test"+("" +splitProp).substring(0,4);
		File testOutput = new File(outputPath+baseFileName+splitTestStr+baseFileExt);
		rw = new RNAWriter(testOutput);
		rw.writeAll(test);
		rw.close();
	}

}
