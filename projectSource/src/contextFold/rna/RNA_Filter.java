/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */


package contextFold.rna;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public abstract class RNA_Filter {

	public abstract boolean filter(RNA rna);
	private List<RNA> filtered;
	
	protected RNA_Filter(){
		filtered = new ArrayList<RNA>();
	}

	public List<RNA> filter(Collection<RNA> rnas){
		return filter(rnas, 0, false);
	}

	public List<RNA> filter(Collection<RNA> rnas, int keep, boolean shuffle){
		List<RNA> toRemove = new ArrayList<RNA>();

		for (RNA rna : rnas){
			if (filter(rna)) toRemove.add(rna);
		}

		if (shuffle) Collections.shuffle(toRemove);
		toRemove.subList(0, keep).clear();
		filtered.clear();
		filtered.addAll(toRemove);

		List<RNA> filtered = new ArrayList<RNA>(rnas);
		filtered.removeAll(toRemove);
		return filtered;
	}

	/**
	 * @return
	 */
	public List<RNA> getLastFiltered() {
		return new ArrayList<RNA>(filtered);
	}

}
