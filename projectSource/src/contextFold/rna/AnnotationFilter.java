 /*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */


package contextFold.rna;

public class AnnotationFilter extends RNA_Filter{

	public static final String SPACE_RPLACE = "@@@";
	
	public AnnotationFilter(String annotation, String annotationVal) {
		super();
		this.annotation = annotation;
		this.annotationVal = annotationVal.replace(SPACE_RPLACE, " ");
	}

	private String annotation, annotationVal;

	/* (non-Javadoc)
	 * @see contextFold.rna.RNA_Filter#accept(contextFold.rna.RNA)
	 */
	@Override
	public boolean filter(RNA rna) {
		return annotationVal.equals(rna.getAnnotationVal(annotation));
	}

}
