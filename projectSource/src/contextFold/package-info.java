/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */


/** 
 * <p>
 * The {@code contextFold} package supplies java source code
 * for RNA secondary structure prediction utilities. Its main uniqueness
 * is in that it allows for a flexible scoring model design, and
 * implements an efficient machine-learning algorithm for inferring 
 * weight parameters for such scoring models. Latest version is
 * available at 
 * <a href="http://www.cs.bgu.ac.il/~negevcb/contextfold/">http://www.cs.bgu.ac.il/~negevcb/contextfold/</a>.
 * </p>
 * <p>
 * The package is developed by Michal Ziv-Ukelson's
 * bioinformatics group, Department of Computer Science, Ben Gurion 
 * University of the Negev, Israel. Group's homepage:
 * <a href="http://www.cs.bgu.ac.il/~negevcb/">http://www.cs.bgu.ac.il/~negevcb/</a>.
 * 
 * The main algorithmic notions are
 * described in the papers:
 * </p>
 * 
 *  <p>
 *  <a href="http://www.springerlink.com/content/h2380543713373k2/">[1]</a>
 *  Shay Zakov, Yoav Goldberg, Michael Elhadad, and Michal Ziv-Ukelson. 
 *  Rich Parameterization Improves RNA Structure Prediction. Research in Computational Molecular Biology 
 *  (RECOMB 2011) 546-562.
 *  <br />
 *  <a href="http://portal.acm.org/citation.cfm?id=1248566">[2]</a>
 *  Koby Crammer, Ofer Dekel, Joseph Keshet, Shai Shalev-Shwartz, and Yoram Singer.
 *  Online Passive-Aggressive Algorithms. Journal of Machine Learning Research 7 (2006) 551�585.
 *  <br />
 *  <a href="http://dx.doi.org/10.1016/j.jda.2010.09.001">[3]</a>
 *  Rolf Backofen, Dekel Tsur, Shay Zakov, and Michal Ziv-Ukelson.
 *  Sparse RNA Folding: Time and Space Efficient Algorithms. 
 *  Journal of Discrete Algorithms Volume 9, Issue 1, March 2011, Pages 12-31,
 *  20th Anniversary Edition of the Annual Symposium on Combinatorial Pattern 
 *  Matching (CPM 2009).
 * </p>
 *  
 *  
 * <p>
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *  </p>
 * <p>
 * {@code contextFold} is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * </p>
 *  <p>
 * 
 * {@code contextFold} is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * <a href="http://www.gnu.org/licenses/">GNU General Public License</a>
 * for more details.
 * </p>
 * 
 *  <p>
 *
 * Contact: <br /> 
 * Shay Zakov:		zakovs@cs.bgu.ac.il <br /> 
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il <br /> 
 * </p>
 * 
 * @version 1.00
 * 
 * 
 */ 


package contextFold; 