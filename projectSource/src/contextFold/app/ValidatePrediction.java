/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */


package contextFold.app;

import java.io.IOException;

import contextFold.common.CmdArgumentsHandler;
import contextFold.scoring.PredictionQuality;

/**
 * 
 * Validates the quality of a prediction file, which was
 * obtained as an output from the {@link Predict} program.
 * 
 * <br>
 * Usage (from the 'ContextFold' directory):<br>
 * &nbsp &nbsp "java -cp bin contextFold.app.ValidatePrediction {@literal <arg1 name>:X1 <arg2 name>:X2} ...", where
 * Xi is the value to assign for the i-th argument. <br>
 * <br>
 * Usage examples: <br>
 * &nbsp &nbsp java -cp bin contextFold.app.ValidatePrediction 
 * path:predictions/sequences.pred<br>
 * &nbsp &nbsp java -cp bin contextFold.app.ValidatePrediction
 * path:predictions/sequences.pred verbose binSize:150<br>
 * <br>
 * Required argument: <br>
 * &nbsp &nbsp path	- A path to a folding prediction file.<br>
 * <br>
 * Main optional arguments (use the 'man' argument for the complete list): <br>
 * &nbsp &nbsp man - Prints the usage manual.<br>
 * &nbsp &nbsp verbose - Verbose mode (Allowed values: false (default), true).<br>
 * &nbsp &nbsp binSize - Size of length bin used in the verbose mode (Default: 
 * 100).<br> 
 * 
 */
public class ValidatePrediction {

	/**
	 * 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {

		String progName = ValidatePrediction.class.getSimpleName();
		String description = "Prints prediction quality statistics.";
		CmdArgumentsHandler argumentsHandler = 
			new CmdArgumentsHandler(progName, description, true);


		String path = argumentsHandler.addRequiredArg("path", "A path to a folding prediction file");
		String bin = argumentsHandler.addArg("binSize", "100", "Size of length bin");
		String verb = argumentsHandler.addEnumArg("verbose", "Verbose mode", 0, "false", "true");

		argumentsHandler.processArgs(args);
		
		String predictionFilePath = argumentsHandler.stringValue(path);
		int binSize = argumentsHandler.intValue(bin);
		boolean verbose = argumentsHandler.boolValue(verb);

		PredictionQuality.predictionStatistics(predictionFilePath, binSize, verbose);
	}

}
