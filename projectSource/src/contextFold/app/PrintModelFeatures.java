/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.app;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Comparator;

import contextFold.common.CmdArgumentsHandler;
import contextFold.common.SuffixAdder;
import contextFold.common.Utils;
import contextFold.features.FeatureManager;

/**
 * Prints feature names and their weights for a given scoring
 * model.
 *  
 * Run with the 'man' argument for listing the arguments
 * for this program.
 * 
 */
public class PrintModelFeatures {

	public static void main(String[] args) {
		String description = "Prints feature names and their weights for " +
		"a given scoring model.";
		String progName = PrintModelFeatures.class.getSimpleName();

		CmdArgumentsHandler argumentsHandler = new CmdArgumentsHandler(progName, description, true);

		String model = argumentsHandler.addRequiredArg("model", "Name of the model's file (a \"*.model\" file)");
		String isFilterZero = argumentsHandler.addEnumArg("filterZeros", "Filters features with 0 weights", 0, "true", "false");
		String weightPropFiterArg = argumentsHandler.addArg("weightPropFilter", "0",
		"Dictates the treshold proportion for feature filtering. For features i such wi/wmax is smaller than this proportion, where wi is the absulte feature weight and wmax is the maximum absulte weight of a featue in the model, wi is set to 0"); 
		String sort = argumentsHandler.addEnumArg("sort", "Sorting criterion", 0, "absWeights", "weights", "no");
		String outputFile = argumentsHandler.addBinedArg("out", model,
				new SuffixAdder(".rep"), "The path for the output file. If 'no' is given as a value, the output is printed to the screen");
		String count = argumentsHandler.addEnumArg("count", "Prints the number of non-zero parameters", 0, "yes", "no", "only");

		argumentsHandler.processArgs(args);

		FeatureManager fm = null;
		String modelFile = argumentsHandler.stringValue(model);
		try {
			fm = Utils.load(FeatureManager.class, modelFile);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		fm.getFeatureWeights().uncompress();
		fm.getFeatureWeights().average();
		float weightPropFilter = argumentsHandler.floatValue(weightPropFiterArg);
		fm.filterWeights(weightPropFilter);
		StringBuilder modelRep = new StringBuilder();

		modelRep.append("Model file: ").append(modelFile);

		String countMode = argumentsHandler.stringValue(count);
		if (!countMode.equals("no")){
			modelRep.append("\nFeature dimension (vector length): ").append(fm.numOfFeatures()); 
			modelRep.append("\nEffective feature number (excluding infeasible features): ").append(fm.effectiveFeatureCount());
//			modelRep.append("\nLength of weight vector: ").append(fm.numOfFeatures());
			modelRep.append("\nNumber of non-zero weights (excluding features which absolute weight is smaller than " +
					weightPropFilter + " from the maximal absolute feature weight): ").append(fm.getFeatureWeights().countNonZeroEntries());
		}

		if (!countMode.equals("only")){
			String sortType = argumentsHandler.stringValue(sort);
			Comparator<FeatureManager.Feature> comparator = null;
			if (sortType.equals("absWeights")) comparator = FeatureManager.absWeightComparator;
			else if (sortType.equals("weights")) comparator = FeatureManager.weightComparator;
			else if (sortType.equals("no")) comparator = FeatureManager.nullComparator;

			modelRep.append("\n\n").append(fm.allFeatureValues(comparator, argumentsHandler.boolValue(isFilterZero)));
		}

		modelRep.append("\n");
		
		String outputFilePath = argumentsHandler.stringValue(outputFile);
		if (!outputFilePath.equals("no")){

			try {
				System.setOut(new PrintStream(new File(outputFilePath)));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}

		System.out.print(modelRep);

	}

}
