/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */

package contextFold.app;

import java.util.List;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import contextFold.common.CmdArgumentsHandler;
import contextFold.common.Env;
import contextFold.common.SuffixAdder;
import contextFold.common.Utils;
import contextFold.features.FeatureManager;
import contextFold.learn.*;
import contextFold.rna.*;
import contextFold.scoring.FMeasureLossCalculator;
import contextFold.scoring.FoldEvaluator;
import contextFold.scoring.FoldEvaluatorImpl;
import contextFold.scoring.HammingLossCalculator;
import contextFold.scoring.LossCalculator;




/**
 * Obtains feature weight parameters of scoring models via a 
 * machine-learning procedure.<br>
 *
 * Usage (from the 'ContextFold' directory):<br>
 * &nbsp &nbsp "java -cp bin contextFold.app.Train {@literal <arg1 name>:X1 <arg2 name>:X2}
 *  ...", where Xi is the value to assign for the i-th argument. <br>
 * <br>
 * Usage examples: <br>
 * &nbsp &nbsp java -cp bin contextFold.app.Train train:C:/RNAdata/trainData.txt<br>
 * &nbsp &nbsp java -cp bin contextFold.app.Train train:C:/RNAdata/trainData.txt 
 * f:StMedCoHigh<br>
 * &nbsp &nbsp java -cp bin contextFold.app.Train train:C:/RNAdata/trainData.txt 
 * f:trained/Baseline.model iter:6 out:trained/BaselineRetrained improve:0.5<br>
 * <br>
 * Required argument: <br>
 * &nbsp &nbsp train - Path for the training data file (must contain 
 * structures in addition to sequences).<br>
 * <br>
 * Main optional arguments (use the 'man' argument for the complete list): <br>
 * &nbsp &nbsp man - Prints the usage manual.<br>
 * &nbsp &nbsp f - A scoring model file. Can be either a name of parameterization
 * java file (extending the contextFold.models.FeatureSetter) found in the 
 * contextFold.models package, or a path to a previously trained scoring model 
 * file (a '.model' file). Default value: 'StHighCoHigh' (using the model 
 * defined in the StHighCoHigh.java file, in the contextFold.models package).<br>
 * &nbsp &nbsp out - Path for the trained model output file (if not specified, 
 * a file with the same name as the input file and an the added '.model' extension 
 * will be generated in the same directory as the input file).<br>
 * &nbsp &nbsp iter	- Maximum number of iterations of the training procedure 
 * (Default: 20).<br>
 * &nbsp &nbsp shuffle - If true, shuffles the training sequences after each 
 * phase of processing the complete set (Allowed values: true (default), 
 * false).<br>
 * &nbsp &nbsp log - Path to a log file. If not specified, log output is 
 * printed to screen.<br>
 * &nbsp &nbsp loss - Loss metric (Allowed values: fMeasure (default), 
 * hamming).<br>
 * &nbsp &nbsp keepIterationWeights - Saving intermediate weights after each 
 * iteration (Allowed values: false (default), true).<br>
 * &nbsp &nbsp improve - improve:X requires that the loss estimation (based on 
 * the training set) will improve in at least X percentage (Default: X = 1) after 
 * each Y consecutive iterations, where Y is defined by the 'maxNI' argument.<br>
 * &nbsp &nbsp maxNI - maxNI:Y imposes that if there was no sufficient improvement 
 * in the last Y iterations (according to the 'improve' parameter), the process 
 * terminates (Default: Y = 3).
 * 
 */
public class Train {

	private static CmdArgumentsHandler argumentHandler;

	// parameters:
	private static int ITERS;
	private static int KBEST;
	private static boolean verboseMode;
	private static boolean echoMode;
	private static boolean shuffleMode;
	private static FeatureManager featureManager;
	private static String weightsOutputFile;
	private static int averageEvery;
	private static FoldEvaluator foldEvaluator;
	private static List<RNA> rnas;
	private static boolean debugMode;
	private static LossCalculator lossCalculator;
	private static float requiredImproveFactor;
	private static int maxNoImporvments;

	private static boolean keepIterationWeights;

	private static long startTime;

	public static void main(String[] args) throws IOException {

		setEnvironment(args);

		System.out.println("Feature dimension:" + featureManager.numOfFeatures() 
				+ " (effective feature number: " + 
				featureManager.effectiveFeatureCount() +
		")\n");
		System.out.println(RNA.RNAListStatistics(rnas));

		Learn lr = new Learn(featureManager, foldEvaluator, 
				KBEST, lossCalculator);

		lr.learn(ITERS, rnas, weightsOutputFile, averageEvery, 
				shuffleMode, requiredImproveFactor, maxNoImporvments, keepIterationWeights);

		System.out.println("Total time(min):" + (System.currentTimeMillis()-startTime)/60000);
		System.out.println("Non-zero feature count:" + 
				featureManager.getFeatureWeights().countNonZeroEntries());
	}


	private static void setEnvironment(String[] args) throws IOException {
		KBEST = 1;
		//		FEATURE_PRUNING_THRESHOLD = 0;
		String progName = Train.class.getSimpleName();
		String description = "A machine-learning training procedure, for inferring feature weight parameters.";
		argumentHandler = new CmdArgumentsHandler(progName, description, true);

		String modelArg = addArg("f", "StHighCoHigh", "A parameterization file");
		String dataInputFileArg = argumentHandler.addRequiredArg("train", "Path for the training data file (must contain" +
				" structures in addition to sequences)");
		String trainedModelArg = argumentHandler.addBinedArg("out", modelArg, new SuffixAdder(""), "Path for the trained model output file");
		String lossArg = argumentHandler.addEnumArg("loss", "Loss metric", 0, "fMeasure", "hamming");
		String itersArg = addArg("iter", "20", "Number of iterations of the training procedure");
		String averageEveryArg = addArg("a", null, "Decide the number of iterations between averaging the weights. Usage: a:X averages the weights every X iterations"); 
		String verboseArg = addArg("verbose", "false", "If specified, prints additional info along the run (mainly for debugging)");
		String echoArg = addArg("echo", "false", "If specified, prints folding scores and predictions along the run");
		String shuffleArg = argumentHandler.addEnumArg("shuffle", "If true, shuffles the training sequences after each phase of processing the complete set", 0, "true", "false"); 
		String prefixArg = addArg("prefix", null, "Allows to read only the prefix of the training set. Usage: prefix:X will process only the first X sequences in the set"); 
		String debugArg = addArg("debug", "false", "Used for debugging");
		String annotationFilterArg = addArg("annot", null, "annot:X Process only sequences with a given annotation X");
		String requiredImproveFactorArg = addArg("improve", "1", "improve:X requires that the loss estimation (based on the training set) will improve in at least X percentage in each iteration. Together with the 'maxNI' parameter, a termination criteria is defined");
		String maxNoImporvmentsArg = addArg("maxNI", "3", "maxNI:X imposes that if there was no sufficient improvement in the last X iterations (according to the 'improve' parameter), the process terminates");
		String maxLengthArg = addArg("maxLength", null, "maxLength:X filters all sequences in the training set longer than X");
		String rnaFilterArg = addArg("filter", CmdArgumentsHandler.noValue, "Allows to filter the RNAs according to some annotation value. Usage: filter:<X>=<Y>, where <X> is the annotation type and <Y> is the values that should be filtered. Note that every space character in the strings <X> and <Y> must be replaced by the character \'" + AnnotationFilter.SPACE_RPLACE+ "\'");
		String filteredOutArg = addArg("filteredOut", CmdArgumentsHandler.noValue, "A path to a file that contains the filtered RNAs. If not specified, no file is created");
		String keepFilteredArg = addArg("keepFiltered", "0", "keepFiltered:<X> keeps X members of the filtered group (note that X must be an integer)");
		String superContextCardinalityArg = addArg("maxSuperCardinality", ""+Env.maxSuperContextCardinalityTrain, "For optimizing the cardinality of represnetative contexts");
		String logArg = addArg("log", CmdArgumentsHandler.noValue, "Path to a log file. If not specified, output is printed to screen");
		String keepIterationWeightsArg = argumentHandler.addEnumArg("keepIterationWeights", "Saving intermediate weights after each iteration", 0, "false", "true");
		String fMeasureBetaArg = argumentHandler.addArg("fBeta", "1", "The beta parameter for the f-measure calculation (has to be non-negative)");
		String formatArg = argumentHandler.addEnumArg("format", "Input file format", 1, "rnaStrand", "oneLine");


		argumentHandler.processArgs(args);

		String log = argumentHandler.stringValue(logArg);

		if (log != null && !log.equals(CmdArgumentsHandler.noValue)) {
			PrintStream logStream = new PrintStream(new File(log));
			System.setOut(logStream);
			System.setErr(logStream);
		}

		startTime = System.currentTimeMillis();


		weightsOutputFile 		= argumentHandler.stringValue(trainedModelArg);
		verboseMode 			= argumentHandler.boolValue(verboseArg);
		echoMode 				= argumentHandler.boolValue(echoArg);
		shuffleMode				= argumentHandler.boolValue(shuffleArg);
		//		saveEvery				= Integer.parseInt(argValues.get(saveEveryArg));
		int prefixLength = -1;
		if (argumentHandler.stringValue(prefixArg) != null){
			prefixLength = argumentHandler.intValue(prefixArg);
		}
		int maxLength = -1;
		if (argumentHandler.stringValue(maxLengthArg) != null){
			maxLength = argumentHandler.intValue(maxLengthArg);
		}
		debugMode				= argumentHandler.boolValue(debugArg);
		requiredImproveFactor	= argumentHandler.floatValue(requiredImproveFactorArg)/100;
		maxNoImporvments		= argumentHandler.intValue(maxNoImporvmentsArg);
		keepIterationWeights 	= argumentHandler.boolValue(keepIterationWeightsArg);

		String format = argumentHandler.stringValue(formatArg);
		RNAListReader lr;
		if ("oneLine".equals(format)){
			lr = new OneLineReader();
		}
		else{
			lr = new RNAStrandReader();
		}
		rnas = lr.readList(new File(argumentHandler.stringValue(dataInputFileArg)));


		String filterAnnotation = argumentHandler.stringValue(rnaFilterArg);
		if (filterAnnotation != null && filterAnnotation != CmdArgumentsHandler.noValue){
			String[] annotation = filterAnnotation.split("=");
			RNA_Filter filter = new AnnotationFilter(annotation[0], annotation[1]);
			int keep = argumentHandler.intValue(keepFilteredArg);
			rnas = filter.filter(rnas, keep, true);
			String filteredOut = argumentHandler.stringValue(filteredOutArg);
			if (!filteredOut.equals(CmdArgumentsHandler.noValue)){
				RNAWriter filteredWriter = new RNAWriter(new File(filteredOut));
				filteredWriter.writeAll(filter.getLastFiltered());
			}
		}

		if (prefixLength > 0) rnas = rnas.subList(0, prefixLength);
		if (maxLength > 0) RNA.filter(rnas, maxLength);
		String annotationFilter = argumentHandler.stringValue(annotationFilterArg);
		if (annotationFilter != null){
			RNA.filter(rnas, annotationFilter);
		}


		String iterNum = argumentHandler.stringValue(itersArg);
		ITERS = Integer.parseInt(iterNum); 

		String averageStr = argumentHandler.stringValue(averageEveryArg);
		if (averageStr == null) averageEvery = -1;
		else averageEvery = (int) (Float.parseFloat(averageStr)*rnas.size());

		contextFold.common.Env.DEBUG_MODE = debugMode;
		contextFold.common.Env.VERBOSE = verboseMode;
		contextFold.common.Env.ECHO = echoMode;

		String model = argumentHandler.stringValue(modelArg);

		try{
			featureManager = FeatureManager.make(model);
		}
		catch (Exception e){
			try {
				featureManager = Utils.load(FeatureManager.class, model);
				featureManager.getFeatureWeights().uncompress();
				featureManager.getFeatureWeights().average();
			} catch (ClassNotFoundException e1) {
				System.err.println("Cannot create scoring model: " + model);
				e1.printStackTrace();
			}
		}

		int maxSuperContextCardinality = argumentHandler.intValue(superContextCardinalityArg);
		featureManager.makeRepresentativeContexts(maxSuperContextCardinality );
		foldEvaluator = new FoldEvaluatorImpl(featureManager);


		// initialize the fold-evaluator
		String LOSS = argumentHandler.stringValue(lossArg);
		if (LOSS.equals("hamming")) {
			lossCalculator = new HammingLossCalculator();
		} else if (LOSS.equals("fMeasure")) {
			float beta = argumentHandler.floatValue(fMeasureBetaArg);
			lossCalculator = new FMeasureLossCalculator(beta);
		} else {
			throw new RuntimeException("bad loss type: "+LOSS);
		}

		System.out.println("Training dataset: " + argumentHandler.stringValue(dataInputFileArg));
		System.out.println("Scoring model: " + argumentHandler.stringValue(modelArg));
		System.out.println("Output model file: " + weightsOutputFile);


	}


	private static String addArg(String arg, String defaultVal, String man) {
		return argumentHandler.addArg(arg, defaultVal, man);
	}

}

