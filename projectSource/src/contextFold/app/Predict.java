/*
 * This file is a part of the contextFold software package for RNA  
 * secondary structure prediction.
 *
 * Copyright (C) 2010, 2011 Shay Zakov and Yoav Goldberg.
 *
 * contextFold is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * contextFold is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:
 * Shay Zakov:		zakovs@cs.bgu.ac.il
 * Yoav Goldberg:	yoavg@cs.bgu.ac.il
 */


package contextFold.app;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;


import contextFold.common.CmdArgumentsHandler;
import contextFold.common.Env;
import contextFold.common.Utils;
import contextFold.features.FeatureManager;
import contextFold.rna.OneLineReader;
import contextFold.rna.RNA;
import contextFold.rna.RNAListReader;
import contextFold.rna.RNAStrandReader;
import contextFold.rna.RNAWriter;
import contextFold.rnaFolding.Folder;
import contextFold.scoring.StructreElementsScorer;


/**
 * Produces secondary structure predictions for RNA sequences.<br>
 * <br>
 * Usage (from the 'ContextFold' directory):<br>
 * &nbsp &nbsp "java -cp bin contextFold.app.Predict {@literal <arg1 name>:X1 <arg2 name>:X2} ...", where
 * Xi is the value to assign for the i-th argument. <br>
 * <br>
 * Usage examples: <br>
 * &nbsp &nbsp java -cp bin contextFold.app.Predict in:C:/RNAdata/sequences.txt<br>
 * &nbsp &nbsp java -cp bin contextFold.app.Predict in:AAGGCCUUGGGGAAGGCCUU<br>
 * &nbsp &nbsp java -cp bin contextFold.app.Predict in:C:/RNAdata/sequences.txt
 * model:trained/StMedCoHigh.model out:predictions/sequences.pred<br>
 * <br>
 * Required argument: <br>
 * &nbsp &nbsp in - Either a single RNA string, or a path to a file that contains the input RNA strings.<br>
 * <br>
 * Main optional arguments (use the 'man' argument for the complete list): <br>
 * &nbsp &nbsp man - Prints the usage manual.<br>
 * &nbsp &nbsp out - Path to the prediction output file (if not specified, a 
 * file with the same name as the input file and an the added '.pred' extension
 * will be generated in the same directory as the input file).<br> 
 * &nbsp &nbsp model - Specifies a scoring model (Default: trained/StHighCoHigh.model). <br>
 * &nbsp &nbsp validate	- Validates prediction quality at the end of the run (Allowed values: yes, no (default)). <br>
 * &nbsp &nbsp echo - Prints progress notifications to screen (Allowed values: yes (default), no, splits, octCounts). <br>
 * &nbsp &nbsp maxLength - Maximum length of predicted sequences (Default: 2147483647, longer sequences are ignored). <br>
 * &nbsp &nbsp minLength - Minimum length of predicted sequences (Default: 0, shorter sequences are ignored). <br>
 * 
 */
public class Predict {

	private static boolean verbose = Env.VERBOSE;
	private static final Folder folder = new Folder();


	/**
	 * 
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		String progName = Predict.class.getSimpleName();
		String description = "Predicting the foldings of a library of RNA sequences.";
		CmdArgumentsHandler argHandler = new CmdArgumentsHandler(progName, description, true);

		String inputArg = argHandler.addRequiredArg("in", 
		"Either a single RNA string, or a path to a file that contains a library of input RNA strings");
		String outputArg = argHandler.addArg("out", "Path to the prediction " +
				"output file. If not specified, then in the case where the input " +
				"is a single RNA string the output will be printed to screen, and " +
				"in the case of a file path the output will be generated in a file " +
				"by the same name with the additional extention '.pred'. For printing" +
				"the output to screen in the later case, set the value to 'screen'");
		String modelArg = argHandler.addArg("model", "trained/StHighCoHigh.model",
		"Scoring model");
		String weightPropFiterArg = argHandler.addArg("weightPropFilter", "0.01",
		"Dictates the treshold proportion for feature filtering. For a feature i such wi/wmax is smaller than this proportion, where wi is the absulte feature weight and wmax is the maximum absulte weight of a featue in the model, wi is set to 0"); 
		String minLengthArg = argHandler.addArg("minLength", "0", "Minimum length of predicted sequences");
		String maxLengthArg = argHandler.addArg("maxLength", ""+Integer.MAX_VALUE, "Maximum length of predicted sequences");
		String sparseArg = argHandler.addEnumArg("sparse", "Sparsification type.", 0, new String[]{"STEP-OCT", "OCT", "STEP", "none"});
		String echoArg = argHandler.addEnumArg("echo", "Prints progress notifications to screen", 0, new String[]{"yes", "no", "splits", "octCounts"});
		String validateArg = argHandler.addEnumArg("validate", "Validates prediction quality", 1, new String[]{"yes", "no"});
		String superContextCardinalityArg = argHandler.addArg("maxSuperCardinality", ""+Env.maxSuperContextCardinalityTrain, "For optimizing the cardinality of represnetative contexts");
		String formatArg = argHandler.addEnumArg("format", "Input file format", 1, "rnaStrand", "oneLine");
		String logArg = argHandler.addArg("log", "Path to a log file. If not specified, output is printed to screen");


		argHandler.processArgs(args);
		

		if (argHandler.hasValue(logArg)) {
			String log = argHandler.stringValue(logArg);
			PrintStream logStream = new PrintStream(new File(log));
			System.setOut(logStream);
			System.setErr(logStream);
		}


		FeatureManager featureManager = null;
		String scoringModelName = argHandler.stringValue(modelArg);
		try {
			featureManager = Utils.load(FeatureManager.class, scoringModelName);

		} catch (ClassNotFoundException e) {
			System.err.println("Cannot create scoring model: " + scoringModelName);
			e.printStackTrace();
		}

		featureManager.getFeatureWeights().uncompress();
		featureManager.getFeatureWeights().average();
		int maxSuperContextCardinality = argHandler.intValue(superContextCardinalityArg);
		featureManager.makeRepresentativeContexts(maxSuperContextCardinality);

		String input = argHandler.stringValue(inputArg);
		int maxLength = argHandler.intValue(maxLengthArg);
		int minLength = argHandler.intValue(minLengthArg);
		String output = argHandler.stringValue(outputArg);
		float weightFilter = argHandler.floatValue(weightPropFiterArg);
		String echo = argHandler.stringValue(echoArg);

		String sparse = argHandler.stringValue(sparseArg);

		folder.setFeatureManager(featureManager);
		folder.resetCounts();
		folder.sparseByOcts = true;
		folder.sparseBySteps = true;
		if (sparse.equals("none") || sparse.equals("STEP")) folder.sparseByOcts = false;
		if (sparse.equals("none") || sparse.equals("OCT")) folder.sparseBySteps = false;

		if (weightFilter > 0){
			featureManager.filterWeights(weightFilter);
		}

		List<RNA> rnas = null;
		try{
			if (RNA.isStandardRNAString(input)){
				rnas = new ArrayList<RNA>(1);
				rnas.add(new RNA(input));
				if ("screen".equals(output)){
					output = CmdArgumentsHandler.noValue;
				}

			}
			else {
				String format = argHandler.stringValue(formatArg);
				RNAListReader lr;
				if ("oneLine".equals(format)){
					lr = new OneLineReader();
				}
				else{
					lr = new RNAStrandReader();
				}
				rnas = lr.readList(new File(input));
				List<RNA> toRemove = new ArrayList<RNA>();
				for (RNA rna: rnas){
					if (rna.length<minLength || rna.length>maxLength) toRemove.add(rna);
				}
				rnas.removeAll(toRemove);

				
				if (CmdArgumentsHandler.noValue.equals(output)) {
					output = input+".pred";
				}
				else if ("screen".equals(output)){
					output = CmdArgumentsHandler.noValue;
				}
				
				if ("yes".equals(echo)) {
					System.out.println("Input: " + input);
					System.out.println("Scoring model: " + scoringModelName +
							", non-zero weight parameters: " +
							featureManager.getFeatureWeights().countNonZeroEntries() +
							", OCT-sparsification: "+folder.sparseByOcts + 
							", STEP-sparsification: "+folder.sparseBySteps);

					System.out.println("\n" + RNA.RNAListStatistics(rnas));
				}

			}
		} catch(Exception e){
			System.err.println("Could not interpret the 'in' parameter as a " +
					"FASTA file or as an RNA sequence"); 
			e.printStackTrace();
			System.exit(1);
		}


		predictAll(featureManager, output, rnas, echo);

		if (argHandler.stringValue(validateArg).equals("yes") && 
				!(CmdArgumentsHandler.noValue.equals(output))){
			String[] validationArgs = {
					"path:"+output
			};
			ValidatePrediction.main(validationArgs);
		}
	}

	public static void predictAll(FeatureManager featureManager,
			String outputPath, List<RNA> rnas) throws IOException {
		predictAll(featureManager, outputPath, rnas, "yes");
	}

	public static void predictAll(FeatureManager featureManager,
			String output, List<RNA> rnas, String echoStr) throws IOException {

		boolean sendToFile = ((output!=null) &&
				!(CmdArgumentsHandler.noValue.equals(output)));
		RNAWriter rfw = null;

		if (sendToFile){
			rfw = new RNAWriter(new File(output));
		}
		else rfw = new RNAWriter(System.out);

		int step = 5;
		int stepCounter = 1;
		int counter = 0;
		int nextPrint = (int) (stepCounter*step*rnas.size()/100.0);

		boolean echo = "yes".equals(echoStr);
		boolean printSplits = "splits".equals(echoStr);

		float timeFactor = (float) (1d/60000);
		long startTime = System.currentTimeMillis();

		for (RNA rna : rnas) {
			++counter;
			if (echo && sendToFile){
				if (verbose){
					if (counter%10 == 0) System.out.println();
					System.out.print(counter + "/" + rnas.size()+"(" + rna.length+")"+", ");
				}
				else {
					if (counter >= nextPrint){
						System.out.print(stepCounter*step +"%, ");
						++stepCounter;
						nextPrint = (int) (stepCounter*step*rnas.size()/100.0);
					}
				}
			}

			StructreElementsScorer G = featureManager.getElementScorer(rna);
			List<int[]> kbestFolds = new ArrayList<int[]>();
			folder.computeDPTables(G, kbestFolds, 1);
			rna.addFold(kbestFolds.get(0));
//			if (sendToFile) 
			rfw.write(rna);
		}

		if (echo && sendToFile){
			System.out.println();

			System.out.println("time (min): " + (System.currentTimeMillis() - startTime)*timeFactor +
					",\nconcatenations: " + folder.concatanations + ", naiv splits: " + folder.naivSplits + 
					", sparsification factor: " + ((float) folder.concatanations) / folder.naivSplits);
		}
		else if (printSplits){
			System.out.print(folder.concatanations);
		}
		else if ("octCounts".equals(echoStr)){
			System.out.println("\nlength\tsequence count\tOCT count\tprop\tsplits count\tprop");
			for (int i=1; i<folder.octPerLength.length; ++i){
				long seqNum = folder.seqPerLength[i];
				long octNum = folder.octPerLength[i];
				long splitNum = folder.splitsPerLength[i];
				System.out.println(i + "\t" + seqNum + "\t" + octNum + "\t" + ((float) octNum)/seqNum +
						"\t" + splitNum + "\t" + ((float) splitNum)/seqNum);
			}
		}

		if (sendToFile) rfw.close();
	}
}
