import json
import subprocess as sp
#from email import header
import zipfile as zf


exampleFile="/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/biologyproject/example.fa"


def merge_files(files, output):
    with open(output, 'w') as outfile:
        for name in files:
            with open(name) as infile:
                outfile.write(infile.read())
"""
OPERATION WITH CONFIG FILE
"""
CONFIG_FILE_NAME="../config.json"
def writeConfig(key,value):
    data=readConfig()
    data.update({key:value})
    print(data)
    with open(CONFIG_FILE_NAME,mode="w") as f:
        f.write(json.dumps(data,indent=4))

#return dictionary that contains data from config.json as key:value
def readConfig():
    print("Read config file")
    with open(CONFIG_FILE_NAME) as data_file:
        data = json.load(data_file)
    return data

def convertFastaToSeq(src):
    data=[]
    f=open(src,'r')
    for line in f:
        data.append(line)
    f.close()
    #print(data[1:])
    res=""
    for i in range (1, len(data)):
        res=res+data[i]
    g=open("inputSequence.seq","w")
    g.write(res)

#convertFastaToSeq("/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/biologyproject/inn.fa")

#print(readConfig()["example"])

"""
RUN PROGRAMMES
"""
def copyDir(src,dst="./"):
    print("copying from ",src)
    proc = sp.Popen(["cp","-R", src, dst], stdout=sp.PIPE, stderr=sp.PIPE)
    (out, err) = proc.communicate()
    if (len(err) > 3):
        print("Something wrong during copy: ",src)
        return False
    print(src, " copied")
    return True

def deleteDir(src):
    print("delating ",src)
    proc = sp.Popen(["rm","-R", src], stdout=sp.PIPE, stderr=sp.PIPE)
    (out, err) = proc.communicate()
    if (len(err) > 3):
        print("Something wrong during deleting: ",src)
        return False
    print(src, " deleted")
    return True

def readFromFile(path):
    with open(path,'r') as f:
        data=f.read()
    print(path, " was read")
    return data

def header(name):
    """
    print()
    print("*" * 50)
    print("*" * 50)
    c = 40 - len(name)
    if(c%2 == 0):
        print("*" * int(c/2)," "*3, name, " "*3, "*" * int(c/2))
    else:
        print("*" * int((c+1) / 2), " " * 3, name, " " * 2, "*" * int((c+1) / 2))
    print("*" * 50)
    print("*" * 50)
    print()
    """

    text="\n"
    text+="*" * 50
    text += "\n"
    text += "*" * 50
    text += "\n"
    c = 40 - len(name)
    if (c % 2 == 0):
        text+= ("*" * int(c / 2))
        text += " " * 5
        text+= name
        text +=" " * 5
        text += ("*" * int(c / 2))
    else:
        text += ("*" * int((c+1) / 2))
        text += " " * 5
        text += name
        text += " " * 4
        text += ("*" * int((c+1) / 2))
    text += "\n"
    text+= ("*" * 50)
    text += "\n"
    text+=("*" * 50)
    text+="\n"

    return text

hotKnots="/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/Programes/HotKnots_v2.0"
def runHotKnots(HotKnotsDirectory, inputFile):
    print("HotKnots directory should contain: bin")
    convertFastaToSeq(inputFile)

    if (HotKnotsDirectory[-1] != "/"):
        src = HotKnotsDirectory + "/bin/"
    else:
        src = HotKnotsDirectory + "bin/"

    if (copyDir(src) == False):
        return;


    proc = sp.Popen(["/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/biologyproject/projectSource/Logic/runHotKnots.sh","inputSequence"], stdout=sp.PIPE, stderr=sp.PIPE)
    (out, err) = proc.communicate()
    if(len(err) > 3 ):
        print("Something wrong with HotKnots")

    with open("HotKnotsRes", mode="w") as f:
        f.write(str(out).replace("\\n","\n")[2:-2])
    f.close();
    deleteDir("bin")
    return "HotKnotsRes"

#runHotKnots(hotKnots,exampleFile)

def runAfold(afoldDirectory, inputFile):
    print("Afold directory should contain: Afold and RNAdata.bin")
###  path correction  ###
    if(afoldDirectory[-1] != "/"):
        src=afoldDirectory+"/Afold"
        src2=afoldDirectory+"/RNAdata.bin"
    else:
        src = afoldDirectory + "Afold"
        src2 = afoldDirectory + "RNAdata.bin"
###  copy Afold to current directory otherwise it not running! ###
    if(copyDir(src)==False):
        return;
    if (copyDir(src2) == False):
        return;
### run Afold ###
    proc = sp.Popen(["./Afold", inputFile], stdout=sp.PIPE, stderr=sp.PIPE)
    (out, err) = proc.communicate()
    if (len(err) > 3):
        print("Something wrong with running Afold")
        print(err)
        return
    print(out)
    print("afold completed")
    deleteDir(("Afold"))
    deleteDir(("RNAdata.bin"))
    return "Afold.res"


"""
afoldDirectory="/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/biologyproject/Afold"
header("AFOLD")
print(runAfold(afoldDirectory))
"""

#path - path to ContextFold
def runContextFold(path,inputFile):
    print("The path to ContextFold should contain directories: bin,doc,src and trained")
    if(path[-1]!="/"):
        path+="/"

    copyDir(path+"bin");
    copyDir(path + "doc");
    copyDir(path + "src");
    copyDir(path + "trained");

    text="/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/biologyproject/projectSource/Logic/runContextFold.sh "
    text+= inputFile

    #run special bashScript because Python cannt run 'java'
    proc = sp.Popen((text),shell=True, stdout=sp.PIPE, stderr=sp.PIPE)
    (out, err) = proc.communicate()
    if (len(err) > 3):
        print("Something wrong with running contextFold")
        print(err)
        return
    print("ContextFold completed")
    deleteDir("bin");
    deleteDir("doc");
    deleteDir("src");
    deleteDir("trained");
    return "contextFold.res"


#contextFoldPath="/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/Programes/ContextFold"
#print(runContextFold(contextFoldPath,"/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/biologyproject/example.fa"))

def runContraFold(path,inputFile):
    print("the path to ContraFold should contain file src/contrafold")
    if (path[-1] != "/"):
        path += "/"
    text=path+"src/contrafold"
    proc = sp.Popen([text, "predict",inputFile], stdout=sp.PIPE, stderr=sp.PIPE)
    (out, err) = proc.communicate()
    if (len(err) > 3):
        print("Something wrong with running contrafold")
        print(err)
        return
    #print((str(out).replace("\\n","\n"))[2:-2])  #[2:-2] because 'out' contains b' at the begining and ' at the end
    print("ContraFold completed")

    with open("ContraFold", mode="w") as f:
        f.write(str(out).replace("\\n","\n")[2:-2])
    f.close()
    return "ContraFold"

mxScarnaPath="/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/Programes/mxscarna_ver1.9"
def runMxScarna(path,inputFile):
    copyDir(inputFile,"input.fasta")
    print("the path to MxScarna should contain file program/mxscarna")
    if (path[-1] != "/"):
        path += "/"
    text=path+"program/mxscarna"
    proc = sp.Popen([text, "input.fasta"], stdout=sp.PIPE, stderr=sp.PIPE)
    (out, err) = proc.communicate()
    #print((str(out).replace("\\n","\n"))[2:-2])  #[2:-2] because 'out' contains b' at the begining and ' at the end
    print("MxScarna completed")

    with open("mxScarnaRes", mode="w") as f:
        f.write(str(out).replace("\\n","\n")[2:-2])
    f.close()
    return "mxScarnaRes"
#runMxScarna(mxScarnaPath,exampleFile)

maxExpectPath = "/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/Programes/RNAstructure/exe"
def runMaxExpect(path, inputFile):
    print("The directory(from path) should contains file: MaxExpect")
    if (path[-1] != "/"):
        path += "/"

    text = "/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/biologyproject/projectSource/Logic/runMaxExpect.sh "
    text += path + " "
    text += inputFile

    # run special bashScript because Python cannt run 'java'
    proc = sp.Popen((text), shell=True, stdout=sp.PIPE, stderr=sp.PIPE)
    (out, err) = proc.communicate()
    if (len(err) > 3):
        print("Something wrong with running MaxExpect")
        print(err)
        return
    print("MaxExpect completed")
    return "outputMaxExpect"

#runMaxExpect(maxExpectPath,exampleFile)

#runContraFold("/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/Programes/contrafold/", exampleFile)

def runFold(path, inputFile):
    print("The directory(from path) should contains file: Fold")
    if (path[-1] != "/"):
        path += "/"

    text = "/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/biologyproject/projectSource/Logic/runFold.sh "
    text += path + " "
    text += inputFile

    # run special bashScript because Python cannt run 'java'
    proc = sp.Popen((text), shell=True, stdout=sp.PIPE, stderr=sp.PIPE)
    (out, err) = proc.communicate()
    if (len(err) > 3):
        print("Something wrong with running Fold")
        print(err)
        return
    print("Fold completed")
    return "outputFold"

petFoldPath="/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/Programes/PETfold"
def runPetFold(path, inputFile):
    print("The directory(from path) should contains file: bin")
    if (path[-1] != "/"):
        path += "/"

    text = "/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/biologyproject/projectSource/Logic/runPetFold.sh "
    text += path + " " + "PETfold "
    text += inputFile

    # run special bashScript because Python cannt run 'java'
    proc = sp.Popen((text), shell=True, stdout=sp.PIPE, stderr=sp.PIPE)
    (out, err) = proc.communicate()
    if (len(err) > 3):
        print("Something wrong with running PetFold")
        print(err)
        return
    print("PetFold completed")
    with open("outputPetFold", mode="w") as f:
        f.write(str(out).replace("\\n","\n")[2:-2])
    f.close()
    return "outputPetFold"

#runPetFold(petFoldPath,exampleFile)

petFoldPathPre2="/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/Programes/PETfoldPre2"
def runPetFoldPre2(path, inputFile):
    print("The directory(from path) should contains file: bin")
    if (path[-1] != "/"):
        path += "/"

    text = "/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/biologyproject/projectSource/Logic/runPetFold.sh "
    text += path + " " + "PETfold "
    text += inputFile

    # run special bashScript because Python cannt run 'java'
    proc = sp.Popen((text), shell=True, stdout=sp.PIPE, stderr=sp.PIPE)
    (out, err) = proc.communicate()
    if (len(err) > 3):
        print("Something wrong with running PetFoldPre2")
        print(err)
        return
    print("PetFoldPre2 completed")
    with open("outputPetFoldPre2", mode="w") as f:
        f.write(str(out).replace("\\n","\n")[2:-2])
    f.close()
    return "outputPetFoldPre2"

#runPetFoldPre2(petFoldPathPre2,exampleFile)

ppFoldPath="/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/Programes/PPfold/PPfold3.1.1.jar"
def runPpFold(path,inputFile):
    print("ppFold should be path to .jar file")
    text = "/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/biologyproject/projectSource/Logic/runPPfold.sh "
    text = text + path + " " + inputFile

    # run special bashScript because Python cannt run 'java'
    proc = sp.Popen((text), shell=True, stdout=sp.PIPE, stderr=sp.PIPE)
    (out, err) = proc.communicate()
    if (len(err) > 3):
        print("Something wrong with running ppFold")
        print(err)
        return
    print("ppFold completed")
    merge_files(("example.ct","example.lseq", "example.newick", "example.seq", "example.st"), "outputPpFold")
    deleteDir("example.ct")
    deleteDir("example.lseq")
    deleteDir("example.newick")
    deleteDir("example.seq")
    deleteDir("example.st")
    return "outputPpFold"

#runPpFold(ppFoldPath,exampleFile)

probKnotPath="/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/Programes/RNAstructure/exe"
def runProbKnot(path, inputFile):
    print("The directory(from path) should contains file: ProbKnot")
    if (path[-1] != "/"):
        path += "/"

    text = "/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/biologyproject/projectSource/Logic/runProbKnot.sh "
    text += path + " "
    text += inputFile

    # run special bashScript because Python cannt run 'java'
    proc = sp.Popen((text), shell=True, stdout=sp.PIPE, stderr=sp.PIPE)
    (out, err) = proc.communicate()
    if (len(err) > 3):
        print("Something wrong with running ProbKnot")
        print(err)
        return
    print("ProbKnot completed")
    return "outputProbKnot"
#runProbKnot(probKnotPath,exampleFile)

pathRNASlo="/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/Programes/RNASLOptSourceCode"
def runRNASlo(path,inputFile):
    convertFastaToSeq(inputFile)
    print("The directory(from path) should contains file: RNASLOpt")
    if (path[-1] != "/"):
        path += "/"

    text = "/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/biologyproject/projectSource/Logic/runRNASLOpt.sh "
    text += path + " inputSequence.seq"

    # run special bashScript because Python cannt run 'java'
    proc = sp.Popen((text), shell=True, stdout=sp.PIPE, stderr=sp.PIPE)
    (out, err) = proc.communicate()
    if (len(err) > 3):
        print("Something wrong with running ProbKnot")
        print(err)
        return

    print("RNASlo completed")
    copyDir(path+"outputRNASLOpt.txt")

    return "outputRNASLOpt.txt"
#runRNASlo(pathRNASlo,exampleFile)


def runAllProgrammes(inputFile=exampleFile, afoldPath="/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/biologyproject/Afold", contextAfoldPath="/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/Programes/ContextFold", contraFoldPath="/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/Programes/contrafold/", foldPath="/home/michalsienko/studia/INFORMATYKA/ROK4/BioInformatyka/Programes/RNAstructure/exe"):
    output=""
    output+=header("Afold")
    output+=runAfold(afoldPath,inputFile)

    output+="\n\n"
    output+=header("ContextAfold")
    output+=runContextFold(contextAfoldPath,inputFile)

    output += "\n\n"
    output+=header("ContraFold")
    output+=runContraFold(contraFoldPath,inputFile)

    output += "\n\n"
    output+=header("Fold")
    output+=runFold(foldPath,inputFile)

    with open("GENERAL_OUTPUT", mode="w") as f:
        f.write(output)

    with zf.ZipFile('GENERAL_OUTPUT.zip', 'w') as myzip:
        myzip.write('GENERAL_OUTPUT')

#runAllProgrammes()
