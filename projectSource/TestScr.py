# Run this script to test basic stability

# Test Config

from GUI.Utils import Config

config = Config.load_config()
if not config:
    print("Test failed; Couldnt laod config file")
    exit()
app_names = Config.get_external_apps_names()
if not app_names:
    print("Test failed; Couldnt find app names")
    exit()
for name in app_names:
    if name not in config:
        print("Test failed; "+str(name)+" not found in config")
        exit()

print("Config test passed")