import json

from Logic.runningProgrammes import runAfold, runContextFold, runContraFold, runFold, runHotKnots, runRNASlo, runProbKnot, runPpFold, runPetFoldPre2, runPetFold, runMaxExpect, runMxScarna

"""
OPERATION WITH CONFIG FILE
"""


class Config:
    config_file_name = "config.json"

    @staticmethod
    def save_config(config_object):
        with open(Config.config_file_name, mode="w") as f:
            f.write(json.dumps(config_object, indent=4))

    @staticmethod
    def load_config():
        with open(Config.config_file_name) as data_file:
            data = json.load(data_file)
        return data

    @staticmethod
    def get_external_apps_names():
        return [
            "Afold",
            "ContextAfold",
            "ContraFold",
            "Fold",
            "HotKnots",
            "RNASLopt",
            "probKnot",
            "ppFold",
            "petFoldPre2",
            "petFold",
            "maxExpect",
            "mxScarna",
            "Program12",
            "Program13",
        ]

    @staticmethod
    def get_external_apps_execution_functions():
        return {
            "Afold": runAfold,
            "ContextAfold": runContextFold,
            "ContraFold": runContraFold,
            "Fold": runFold,
            "HotKnots": runHotKnots,
            "RNASLopt": runRNASlo,
            "probKnot": runProbKnot,
            "ppFold": runPpFold,
            "petFoldPre2": runPetFoldPre2,
            "petFold": runPetFold,
            "maxExpect": runMaxExpect,
            "mxScarna": runMxScarna,
        }

    @staticmethod
    def merge_files(files, output):
        with open(output, 'w') as outfile:
            for name in files:
                with open(name) as infile:
                    outfile.write(infile.read())
