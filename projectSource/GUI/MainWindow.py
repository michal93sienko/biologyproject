import os
import sys
import subprocess

from PyQt5.QtCore import QBasicTimer
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QCheckBox
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtWidgets import QLabel
from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QProgressBar
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtWidgets import QTextEdit

from GUI.Utils import Config


def run_gui():
    app = QApplication(sys.argv)
    w = MainWindow()
    sys.exit(app.exec_())


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        ####################
        self.apps_spacing = 40
        self.is_processing = False
        self.execute_script_process = None
        self.processed_apps = 0
        ####################
        self.resize(540, 600)
        self.move(300, 300)
        self.setWindowTitle('Manager2DStructureRNA')

        self.run_button = QPushButton('Run', self)
        self.run_button.clicked.connect(self.__run_apps)
        self.run_button.move(350, 515)

        # self.run_button = QPushButton('Test', self)
        # self.run_button.clicked.connect(self.__test_apps)
        # self.run_button.move(240, 515)

        self.progress_bar = QProgressBar(self)
        self.progress_bar.setGeometry(180, 550, 380, 10)



        self.timer = QBasicTimer()

        self.log_box = QTextEdit(self)
        self.log_box.setFixedSize(350, 500)
        self.log_box.setReadOnly(True)
        self.log_box.setFontPointSize(10)
        self.log_box.move(180, 10)

        self.app_labels = {}
        self.app_path_line = {}
        self.app_get_path_buttons = {}
        self.app_checkboxes = {}

        self.config = Config.load_config()
        self.log_box.setText(str(self.config))

        self.apps_names = Config.get_external_apps_names()
        for i in range(len(self.apps_names)):
            app = self.apps_names[i]
            if app not in self.config:
                self.config[app] = ";0"
            app_config_data = self.config[app].split(";")
            label = QLabel(self)
            label.setText("<b>"+app+"</b>")
            label.move(30+15, i*self.apps_spacing)
            self.app_labels[app] = label

            path_line = QLineEdit(self)
            path_line.setFixedSize(100, 12)
            path_line.move(30, i*self.apps_spacing+25)
            path_line.setText(app_config_data[0])
            self.app_path_line[app] = path_line

            get_path_button = QPushButton(self)
            get_path_button.setFixedSize(24, 15)
            get_path_button.setText("...")
            get_path_button.move(30 + 105, i*self.apps_spacing+23)
            get_path_button.clicked.connect(self.make_path_button_callback(path_line))
            self.app_get_path_buttons[app] = get_path_button

            checkbox = QCheckBox(self)
            checkbox.move(30, i*self.apps_spacing)
            checkbox.setChecked(True if app_config_data[1] == "1" else False)
            self.app_checkboxes[app] = checkbox

        self.data_label = QLabel(self)
        self.data_label.setText("<b> Data file: </b>")
        self.data_label.move(100, 565)
        self.data_path_line = QLineEdit(self)
        self.data_path_line.setFixedSize(250, 15)
        self.data_path_line.move(160, 572)
        self.data_path_line.setText("" if "DataFilePath" not in self.config else self.config["DataFilePath"])
        self.data_get_path_button = QPushButton(self)
        self.data_get_path_button.setFixedSize(24, 15)
        self.data_get_path_button.setText("...")
        self.data_get_path_button.move(415, 572)
        self.data_get_path_button.clicked.connect(self.make_path_button_callback(self.data_path_line))

        self.show()

    def closeEvent(self, event):
        super().closeEvent()
        QApplication.quit()

    def __run_apps(self):
        if not self.is_processing:
            for app in self.apps_names:
                self.config[app] = str(self.app_path_line[app].text())+";"+("1" if self.app_checkboxes[app].isChecked() else "0")
                self.config["DataFilePath"] = str(self.data_path_line.text())
            Config.save_config(self.config)
            self.is_processing = True
            self.processed_apps = 0
            self.progress_bar.setValue(0)
            self.log_box.clear()
            self.log_box.append("python -u "+os.path.join("..","Execution","ExecutionScript.py"))
            #self.log_box.append(str(os.path.isfile(os.path.join("Execution","ExecutionScript.py"))))
            self.execute_script_process = subprocess.Popen("python -u "+os.path.join("Execution","ExecutionScript.py"), shell=True, stdout=subprocess.PIPE)
            self.timer.start(1000, self)

    def __test_apps(self):
        return

    def make_path_button_callback(self, line_edit):
        def path_button_callback():
            dir = QFileDialog.getExistingDirectory(None, "Select Directory")
            if dir:
                line_edit.setText(dir)
            return
        return path_button_callback

    def timerEvent(self, e):
        finished = True
        for line in iter(self.execute_script_process.stdout.readline, b''):
            line = str(line, sys.stdout.encoding).rstrip(os.linesep)
            if line == "APP_PROCESSING_FINISHEDsdsdmdfdkflsdkdsl":
                self.processed_apps += 1
                new_progress_bar_value = self.processed_apps*100./len([name for name in self.apps_names if self.config[name].split(";")[1] == "1"])
                self.progress_bar.setValue(new_progress_bar_value)
            else:
                self.log_box.append(line)
            finished = False
            break
        if finished:
            self.timer.stop()
            self.is_processing = False
            self.log_box.append("Processing finished!")
